; (function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.nav = {
    settings: {
      autoinit: true,
      main: '.default-nav',
      toggle: '[data-nav="toggle"]',
      overlay: '.default-overlay',
      openedClass: 'opened',
      menuFocusClass: 'do-focus'
    },

    init: function () {
      this.handler();
    },

    handler: function () {
      var that = this;

      // Toggle
      $(document).on({
        click: function (e) {
          that.toggle();
          e.preventDefault();
        }
      }, this.settings.toggle);

      // Menu
      $(document).on({
        click: function (e) {
          that.toggle();
          main.scrollTop.run();
          e.preventDefault();
        },

        focus: function () {
          $(this).parent().addClass(that.settings.menuFocusClass);
        },

        blur: function () {
          $(this).parent().removeClass(that.settings.menuFocusClass);
        }
      }, this.settings.main + ' .menu a');

      // Keys
      $(document).on({
        keydown: function (e) {
          switch (e.which) {
            case 27: // Esc
              that.toggle(true);
              break;

            case 77: // M
              that.toggle();
              break;

            case 38: // Up
            case 40: // Down
              that.menuAccess(e);
              break;
          }
        }
      });

      // Avoid close
      $(document).on({
        click: function (e) {
          e.stopPropagation();
        }
      }, this.settings.main);

      // Avoid shortcut
      $(document).on({
        keydown: function (e) {
          if (e.which === 77) { // M
            e.stopPropagation();
          }
        }
      }, 'input, textarea');
    },

    toggle: function (close) {
      var that = this,
        $main = $(this.settings.main),
        $overlay = $main.closest($(this.settings.overlay)),
        speed = 150;

      if ($main.hasClass(this.settings.openedClass)) {
        $main.removeClass(that.settings.openedClass);
        $overlay.fadeOut(speed, function () {
          $main.find('.menu li').removeClass(that.settings.menuFocusClass);
        });
      } else if (!close && !$('.modal-backdrop, .overlay').is(':visible')) {
        $overlay.fadeIn(speed, function () {
          $main
            .addClass(that.settings.openedClass)
            .find('.top button')
            .focus(); // Prepare for navigation with Tab key
        });
      }
    },

    menuAccess: function (event) {
      var key = event.which,
        $main = $(this.settings.main),
        $menu = $main.find('.menu'),
        $focused,
        $focusedPrev,
        $focusedPrevLast,
        $focusedNext,
        $focusedNextFirst;

      if ($main.hasClass(this.settings.openedClass)) {
        $focused = $menu.find('.' + this.settings.menuFocusClass);

        event.preventDefault();

        if (key === 38) {
          $focusedPrev = $focused.prev();
          $focusedPrevLast = $menu.find('li:last-child').find('a');

          if ($focused.length) {
            if ($focusedPrev.length) {
              $focusedPrev.find('a').focus();
            } else {
              $focusedPrevLast.focus();
            }
          } else {
            $focusedPrevLast.focus();
          }
        } else {
          $focusedNext = $focused.next();
          $focusedNextFirst = $menu.find('li:first-child').find('a');

          if ($focused.length) {
            if ($focusedNext.length) {
              $focusedNext.find('a').focus();
            } else {
              $focusedNextFirst.focus();
            }
          } else {
            $focusedNextFirst.focus();
          }
        }
      }
    }
  };
} (jQuery, this, this.document));
