;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.scrollTop = {
    settings: {
      autoinit: true,
      main: '[data-scroll-top]'
    },

    init: function () {
      this.handler();
    },

    handler: function () {
      var that = this;

      $(document).on({
        click: function (e) {
          that.run();
          e.preventDefault();
        }
      }, this.settings.main);
    },

    run: function () {
      $('html, body').animate({
        scrollTop: 0
      }, 0);
    }
  };
}(jQuery, this, this.document));
