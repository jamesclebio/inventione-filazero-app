;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.inputRange = {
    settings: {
      autoinit: false,
      main: '[inv-input-range]'
    },

    init: function () {},

    builder: function (element) {
      var $main = $(element),
        $input = $main.find('[type="range"]'),
        $output = $main.find('output');

      $input
        .rangeslider('destroy')
        .rangeslider({
          polyfill: false
        });

      this.output($input, $output);
      this.handler($input, $output);
    },

    handler: function ($input, $output) {
      var that = this;

      $input.on({
        input: function () {
          that.output($input, $output);
        },

        change: function () {
          that.output($input, $output);
        }
      });
    },

    output: function ($input, $output) {
      $output.val($input.val());
    },

    update: function () {
      $(this.settings.main + ' input[type="range"]').rangeslider('update', true).change();
    }
  };
}(jQuery, this, this.document));
