;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.marquee = {
    settings: {
      autoinit: false
    },

    init: function (element) {
      var $main = $(element);

      this.builder($main);
    },

    sigmoid: function (childRelativeWidth) {
      // Max value to be added to the duration base time
      var maxValue = 10000;
      // How fast the duration will be decreased
      var steepness = 10;
      // Define the range on which the duration will be decreased
      // For example:
      //   for a parent width of 200px, the duration base
      //   time will start to be decreased when the child width
      //   exceeds 200px. The decrement will reaches its maximum
      //   value when the child width is around 300px.  
      var xValueMidPoint = 1.5;

      return maxValue / (1 + Math.pow(Math.E, -steepness * (childRelativeWidth - xValueMidPoint)));
    },

    builder: function ($main) {
      // Default duration time
      var durationBaseTime = 25000;

      var $parentWidth = $main.parent().width();
      var $mainWidth = $main.width();

      if ($mainWidth > $parentWidth) {
        $main.marquee({
          startVisible: true,
          delayBeforeStart: 2000,
          duration: 25000 - this.sigmoid($mainWidth / $parentWidth),
          duplicated: true,
          gap: 100
        });
      }
    },

    handler: function () {},

    destroy: function (element) {
      $(element).marquee('destroy');
    },
  };
}(jQuery, this, this.document));
