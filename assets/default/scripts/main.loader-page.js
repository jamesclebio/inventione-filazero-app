;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.loaderPage = {
    settings: {
      autoinit: false,
      main: '.loader-page',
      template: '<div class="loader-page"></div>',
      speed: 100
    },

    init: function () {},

    builder: function () {
      var $main = $(this.settings.main);

      return ($main.length) ? $main : $(this.settings.template).appendTo($('body'));
    },

    handler: function () {},

    show: function () {
      this.builder().fadeIn(this.settings.speed);
    },

    hide: function () {
      this.builder().fadeOut(this.settings.speed);
    }
  };
}(jQuery, this, this.document));
