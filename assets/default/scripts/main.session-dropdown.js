;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.sessionDropdown = {
    settings: {
      autoinit: true,
      main: '.session-dropdown',
      trigger: '.default-header .block-item-session',
      speed: 100,
      delay: 500
    },

    init: function () {
      this.handler();
    },

    handler: function () {
      var that = this;

      // Dropdown
      $(document).on({
        mouseenter: function() {
          $(this).clearQueue();
        },

        mouseleave: function() {
          $(this)
            .delay(that.settings.delay)
            .fadeOut(that.settings.speed);
        }
      }, this.settings.main);

      // Trigger
      $(document).on({
        click: function (e) {
          var $dropdown = $(that.settings.main);

          if ($dropdown.is(':visible')) {
            that.heightAdjust();

            $dropdown
              .clearQueue()
              .fadeOut(that.settings.speed);
          } else {
            $dropdown.fadeIn(that.settings.speed);
          }

          e.preventDefault();
        },

        mouseenter: function () {
          var $dropdown = $(that.settings.main);

          if ($dropdown.length) {
            $dropdown.clearQueue();

            if (!$dropdown.is(':visible')) {
              that.heightAdjust();
              $dropdown.fadeIn(that.settings.speed);
            }
          }
        },

        mouseleave: function () {
          var $dropdown = $(that.settings.main);

          if ($dropdown.is(':visible')) {
            $dropdown
              .delay(that.settings.delay)
              .fadeOut(that.settings.speed);
          }
        }
      }, this.settings.trigger);
    },

    heightAdjust: function () {
      $(this.settings.main).css('max-height', $(window).height() - 100);
    }
  };
}(jQuery, this, this.document));
