; (function () {
  'use strict';

  angular
    .module('ResponseCatcher')
    .factory('ResponseCatcherService', ResponseCatcherService);

  ResponseCatcherService.$inject = [
    'NotificationsService',
    '$state',
    '$q',
    '$filter'
  ];

  function ResponseCatcherService(NotificationsService, $state, $q, $filter) {
    var service = {
      catchError: catchError,
      catchSuccess: catchSuccess
    };
    return service;

    function catchSuccess(success, showSuccessNotification, showErrorNotification, showInfoNotification, showWarningNotification) {
      var deferred = $q.defer();

      showSuccessNotification = typeof showSuccessNotification === 'undefined' ? true : showSuccessNotification;
      showErrorNotification = typeof showErrorNotification === 'undefined' ? true : showErrorNotification;
      showInfoNotification = typeof showInfoNotification === 'undefined' ? true : showInfoNotification;
      showWarningNotification = typeof showWarningNotification === 'undefined' ? true : showWarningNotification;

      var response = {
        successes: [],
        errors: [],
        infos: [],
        warnings: []
      }
      var message = {
        success: "",
        error: "",
        info: "",
        warning: ""
      };

      if (success.messages) {
        for (var i = 0; i < success.messages.length; i++) {
          switch ((success.messages[i].type).toUpperCase()) {
            case 'SUCCESS':
              response.successes.push(success.messages[i]);
              break;
            case 'ERROR':
              response.errors.push(success.messages[i]);
              break;
            case 'INFO':
              response.infos.push(success.messages[i]);
              break;
            case 'WARNING':
              response.warnings.push(success.messages[i]);
              break;
          }
        }
      }

      for (var i = 0; i < response.successes.length; i++) {
        message.success += $filter('translate')('global.message.success.' + response.successes[i].code || 'default', response.successes[i].parameters);
        if (i !== response.successes.length - 1) {
          message.success += "<br><br>";
        }
      }
      for (var i = 0; i < response.errors.length; i++) {
        message.error += $filter('translate')('global.message.error.' + response.errors[i].code || 'default', response.errors[i].parameters);
        if (i !== response.errors.length - 1) {
          message.error += "<br><br>";
        }
      }
      for (var i = 0; i < response.infos.length; i++) {
        message.info += $filter('translate')('global.message.info.' + response.infos[i].code || 'default', response.infos[i].parameters);
        if (i !== response.infos.length - 1) {
          message.info += "<br><br>";
        }
      }
      for (var i = 0; i < response.warnings.length; i++) {
        message.warning += $filter('translate')('global.message.warning.' + response.warnings[i].code || 'default', response.warnings[i].parameters);
        if (i !== response.warnings.length - 1) {
          message.warning += "<br><br>";
        }
      }

      if (showSuccessNotification && message.success) {
        NotificationsService.success(message.success);
      }
      if (showErrorNotification && message.error) {
        NotificationsService.error(message.error);
      }
      if (showInfoNotification && message.info) {
        NotificationsService.info(message.info);
      }
      if (showWarningNotification && message.warning) {
        NotificationsService.warning(message.warning);
      }

      if (response.successes.length) {
        deferred.resolve(response);
      } else {
        deferred.reject(response);
      }

      return deferred.promise;
    };

    function catchError(error, redirect) {
      if (!redirect) {
        switch (error.status) {
          case 400:
            NotificationsService.error(error.error_description);
            break;
          case 401:
            NotificationsService.error('Você precisa estar autenticado para executar essa ação.');
            break;
          case 403:
            NotificationsService.error('Você não tem permissões para executar essa ação.');
            break;
          case 404:
            NotificationsService.error('O recurso que você está tentando acessar não existe.');
            break;
          case 0:
          case 500:
          default:
            NotificationsService.error('Sistema indisponível. Tente novamente dentro de instantes.');
            break;
        }
      } else {
        switch (error.status) {
          case 401:
            $state.go('login');
            break;
          case 403:
            $state.go('app.access-denied');
            break;
          case 404:
            $state.go('app.not-found');
            break;
          case 0:
          case 500:
          default:
            $state.go('app.internal-error');
            break;
        }
      }
    };
  }
})();
