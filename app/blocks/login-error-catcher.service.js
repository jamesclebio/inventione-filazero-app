;(function () {
  'use strict';

  angular
    .module('ResponseCatcher')
    .factory('LoginErrorCatcherService', LoginErrorCatcherService);

  LoginErrorCatcherService.$inject = [
    'NotificationsService',
    '$filter'
  ];

  function LoginErrorCatcherService(NotificationsService, $filter) {
    var service = {
      catchError: catchError
    };
    return service;

    function catchError(response) {
      NotificationsService.error($filter('translate')('global.message.error.' + response.error));
    }
  }
})();
