﻿; (function() {
  'use strict';

  angular
    .module('filazero-app')
    .controller('HeaderController', HeaderController);

  HeaderController.$inject = [
    '$scope',
    '$rootScope',
    'AuthService',
    'LocationsManagerService',
    'ProviderService',
    'ProvidersService',
    '$q',
    '$state',
    '$stateParams',
    'ResponseCatcherService'
  ];

  function HeaderController($scope, $rootScope, AuthService, LocationsManagerService, ProviderService, ProvidersService, $q, $state, $stateParams, ResponseCatcherService) {
    var vm = this;

    vm.logOut = logOut;
    vm.isCurrentProvider = isCurrentProvider;
    vm.hasCurrentProvider = hasCurrentProvider;
    vm.hasPermission = hasPermission;
    vm.openLocationSelector = openLocationSelector;
    vm.openSearch = openSearch;

    init();

    function init() {
      vm.currentProvider = ProviderService.getCurrentProvider();
      getUserInfo();
      getProviders();
    }

    $scope.$on('appProviderChanged', function() {
      vm.currentProvider = ProviderService.getCurrentProvider();
    });

    $scope.$on('appLocationChanged', function() {
      vm.currentLocation = LocationsManagerService.getCurrentLocation();
    });

    $scope.$on('providerListChanged', function() {
      getProviders();
    });

    function createPermissionString(provider) {
      var permissions = '';

      for (var i = 0; i < provider.roles.length; i++) {
        if (permissions.length > 0) {
          permissions = permissions + ', ';
        }
        switch (provider.roles[i]) {
          case 'owner':
            permissions = permissions + "Dono";
            break;
          case 'admin':
            permissions = permissions + "Administrador";
            break;
          case 'manager':
            permissions = permissions + "Gerente";
            break;
          case 'attendance':
            permissions = permissions + "Atendente";
            break;
        }
      }

      return permissions;
    }

    function getUserInfo() {
      if (AuthService.isAuth()) {
        vm.profileInfo = AuthService.getUserInfo();
      }
    }

    function getProviders() {
      ProvidersService.getProviders().then(
        function(response) {
          vm.providers = response.providers;
        },
        function(error) {
          ResponseCatcherService.catchError(error, true);
        });
    }

    function logOut() {
      AuthService.logOut();
      $state.go('login');
    }

    function hasCurrentProvider() {
      return !!vm.currentProvider;
    }

    function isCurrentProvider(provider) {
      return !!vm.currentProvider && vm.currentProvider.id === provider.id;
    }

    function hasPermission() {
      var provider = ProviderService.getCurrentProvider();
      if (provider) {
        return createPermissionString(provider);
      }
    }

    function openLocationSelector() {
      $rootScope.$broadcast('toggleLocationSelector');
    }

    function openSearch() {
      $rootScope.$broadcast('toggleSearchOverlay', {
        show: true
      });
    }


  }
})();
