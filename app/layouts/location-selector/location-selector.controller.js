; (function() {
  'use strict';

  angular
    .module('filazero-app')
    .controller('LocationSelectorController', LocationSelectorController);

  LocationSelectorController.$inject = [
    '$rootScope',
    '$scope',
    'ProviderService',
    'LocationsManagerService',
    '$q',
    'ResponseCatcherService'
  ];

  function LocationSelectorController($rootScope, $scope, ProviderService, LocationsManagerService, $q, ResponseCatcherService) {
    var vm = this;

    vm.toggleSelector = false;

    var currentLocation;

    vm.changeAppLocation = changeAppLocation;
    vm.isCurrentLocation = isCurrentLocation;

    init();

    function init() {
      vm.currentProvider = ProviderService.getCurrentProvider();
      if (vm.currentProvider) {
        getLocations(vm.currentProvider.id);
      }
    }

    $scope.$on('toggleLocationSelector', function() {
      vm.toggleSelector = !vm.toggleSelector;
    });

    $scope.$on('locationsListChanged', function() {
      getLocations(vm.currentProvider.id);
    });

    $scope.$on('appLocationChanged', function() {
      currentLocation = LocationsManagerService.getCurrentLocation();
      vm.toggleSelector = false;
    });

    $scope.$on('appProviderChanged', function() {
      vm.currentProvider = ProviderService.getCurrentProvider();
      if (vm.currentProvider) {
        getLocations(vm.currentProvider.id);
      }
    });

    function getLocations(providerId) {
      var deferred = $q.defer();

      LocationsManagerService.getLocations(providerId).then(
        function(response) {
          vm.locations = response;
          changeAppLocation(vm.locations[0]);
          deferred.resolve();
        },
        function(error) {
          ResponseCatcherService.catchError(error, true);
          deferred.reject();
        });

      return deferred.promise;
    };

    function changeAppLocation(location) {
      LocationsManagerService.setCurrentLocation(location);
      $rootScope.$broadcast('appLocationChanged');
    };

    function isCurrentLocation(location) {
      return currentLocation.id === location.id;
    };
  }
})();
