; (function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('SearchController', SearchController);

  SearchController.$inject = [
    '$scope',
    'SearchService',
    '$uibModal',
    'ResponseCatcherService'
  ];

  function SearchController($scope, SearchService, $uibModal, ResponseCatcherService) {
    var vm = this;
    var lastSearchedString = '';

    vm.order = 'client';

    vm.search = search;
    vm.openTicketDetails = openTicketDetails;
    vm.setOrder = setOrder;

    $scope.$on('toggleSearchOverlay', function () {
      lastSearchedString = '';
      vm.searchString = undefined;
      vm.searchResult = undefined;
      vm.initState = true;
      vm.focus = !vm.focus;
    });

    $scope.$watch('vm.searchResult', function (newValue, oldValue) {
      if (newValue) {
        vm.hasTickets = vm.searchResult.length > 0;
      }
    });

    function search(string) {
      if (string !== lastSearchedString && string.length > 0) {
        vm.initState = false;
        lastSearchedString = string;

        SearchService.search(string).then(
          function (response) {
            vm.searchResult = response;
          },
          function (error) {
            ResponseCatcherService.catchError(error, true);
          });
      }
    }

    function openTicketDetails(ticketId) {
      $uibModal.open({
        templateUrl: 'modules/attendance/sessions/ticket-details-modal.html',
        controller: 'TicketDetailsModalController',
        windowClass: 'stick-up',
        controllerAs: 'vm',
        resolve: {
          resolve: function () {
            return {
              ticketId: ticketId
            };
          }
        }
      });
    }

    function setOrder(order) {
      if (order === vm.order) {
        vm.order = '-' + order;
      } else {
        vm.order = order;
      }
    }
  }
})();
