; (function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('NavController', NavController);

  NavController.$inject = [
    '$scope',
    'ProviderService',
    'LocationsManagerService',
    '$rootScope',
    '$state',
    'localStorageService'
  ];

  function NavController($scope, ProviderService, LocationsManagerService, $rootScope, $state, localStorageService) {
    $scope.$on('appProviderChanged', function () {
      $scope.currentProvider = ProviderService.getCurrentProvider();
    });

    $scope.$on('appLocationChanged', function () {
      $scope.currentLocation = LocationsManagerService.getCurrentLocation();
    });

    $scope.hasCurrentProvider = function () {
      return !!$scope.currentProvider;
    };

    $scope.openSearch = function () {
      $rootScope.$broadcast('toggleSearchOverlay', {
        show: true
      });
    }

    $scope.goToAttendanceRoute = function () {
      var defaultAttendanceRoute = localStorageService.get('default-attendance-route');
      if (!defaultAttendanceRoute || defaultAttendanceRoute === 'old') {
        $state.go('app.provider.attendance', { providerSlug: $scope.currentProvider.slug });
      } else {
        $state.go('app.provider.new-attendance', { providerSlug: $scope.currentProvider.slug });
      }
    }

    $scope.goToBasicAttendance = function () {
      var url = $state.href('basic-attendance', { providerSlug: $scope.currentProvider.slug, locationId: $scope.currentLocation.id });

      window.open(url, '_blank', 'width=570, height=640, top=0, left=0, resizable=0, scrollbars=1, location=0, status=0');
    };

    $scope.currentProvider = ProviderService.getCurrentProvider();
  };
})();
