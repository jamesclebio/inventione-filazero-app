(function () {
  'use strict';

  angular
    .module('filazero-app')
    .factory('NotificationsService', NotificationsService);

  NotificationsService.$inject = [];

  function NotificationsService() {
    var notificationOptions = {
      style: 'flip',
      message: '',
      position: 'top-right',
      timeout: 5000,
      type: 'success'
    };

    return {
      error: error,
      success: success,
      warning: warning,
      info: info
    };

    function error(message, elementId) {
      notificationOptions.type = 'error';
      notificationOptions.message = message;
      show(notificationOptions, elementId);
    }

    function success(message, elementId) {
      notificationOptions.type = 'success';
      notificationOptions.message = message;
      show(notificationOptions, elementId);
    }

    function warning(message, elementId) {
      notificationOptions.type = 'warning';
      notificationOptions.message = message;
      show(notificationOptions, elementId);
    }

    function info(message, elementId) {
      notificationOptions.type = 'info';
      notificationOptions.message = message;
      show(notificationOptions, elementId);
    }

    function show(options, elementId) {
      $(elementId || 'body').pgNotification(options).show();
    }
  }
})();
