;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .factory('PanelService', PanelService);

  PanelService.$inject = [
    '$resource',
    '$q',
    'ngAuthSettings',
    'ProviderService'
  ];

  function PanelService($resource, $q, ngAuthSettings, ProviderService) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var resourcePanel = $resource(serviceBase + 'api/providers/:providerId/locations/:locationId/panels/:panelId/customer-services', {providerId: '@providerId', panelId: '@panelId'});

    return {
      getPanelData: getPanelData
    };

    function getPanelData(locationId, panelId) {
      var deferred = $q.defer();
      var providerId = ProviderService.getCurrentProvider().id;

      resourcePanel.get({providerId: providerId, locationId: locationId, panelId: panelId},
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }
  }
})();
