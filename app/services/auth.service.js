﻿;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .factory('AuthService', AuthService);

  AuthService.$inject = [
    '$rootScope',
    '$resource',
    '$http',
    '$q',
    'localStorageService',
    'ngAuthSettings'
  ];

  function AuthService($rootScope, $resource, $http, $q, localStorageService, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var resourceSocialLogin = $resource(serviceBase + 'api/account/registerorupdateexternallogin');
    var resourceRegistration = $resource(serviceBase + 'api/account/register');
    var resourceProfile = $resource(serviceBase + 'api/me/profile');

    return {
      login: login,
      registerOrUpdateExternal: registerOrUpdateExternal,
      logOut: logOut,
      saveRegistration: saveRegistration,
      updateCurrentUser: updateCurrentUser,
      getToken: getToken,
      getUserInfo: getUserInfo,
      getPermissions: getPermissions,
      userHasPermission: userHasPermission,
      isAuth: isAuth
    };

    //Login
    function login(loginData) {
      logOut();

      loginData.userName = loginData.userName.toLowerCase();

      var data = {
        grant_type: "password",
        client_id: ngAuthSettings.clientId,
        userName: loginData.userName,
        password: loginData.password
      };

      return token(data);
    }

    function token(data) {
      var deferred = $q.defer();

      $http.post(
        serviceBase + 'token',
        $.param(data),
        {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
        .success(function (response) {
          afterLogin(response).then(
            function (response) {
              deferred.resolve(response)
            },
            function (error) {
              deferred.reject(error);
            }
          );
        })
        .error(function (error) {
          logOut();
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function registerOrUpdateExternal(registerExternalData) {

      var deferred = $q.defer();

      resourceSocialLogin.save({}, registerExternalData,
        function (response) {
          afterLogin(response).then(
            function (response) {
              deferred.resolve(response)
            },
            function (error) {
              deferred.reject(error);
            }
          );
        },
        function (error) {
          logOut();
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function afterLogin(response) {
      var deferred = $q.defer();

      setToken({
        token: response.access_token
      });

      $rootScope.$broadcast('$$userAuthenticated');

      updateCurrentUser().then(
        function () {
          deferred.resolve(response);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function logOut() {
      removeToken();
      removePermissions();
      removeUserInfo();

      $rootScope.$broadcast('$$logout');
    }

    // Registration
    function saveRegistration(registration) {
      logOut();

      var deferred = $q.defer();

      resourceRegistration.save({}, registration,
        function (response) {
          deferred.resolve(response);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function updateCurrentUser(providerId) {
      var deferred = $q.defer();

      resourceProfile.get({pid: providerId},
        function (response) {
          setUserInfo(response.user);

          var resources = _.keys(response.permissions);
          var actions = _.reduceRight(_.values(response.permissions), function (a, b) {
            return a.concat(b);
          }, []);
          var permissions = _.union(resources, actions);

          setPermissions(permissions);

          deferred.resolve();
        },
        function (error) {
          deferred.reject(error);
        }
      );

      return deferred.promise;
    }

    // Permissions
    function getPermissions() {
      return localStorageService.get('userPermissions');
    }

    function setPermissions(permissions) {
      localStorageService.set('userPermissions', permissions);
    }

    function removePermissions() {
      localStorageService.remove('userPermissions');
    }

    // Token
    function getToken() {
      return localStorageService.get('token');
    }

    function setToken(tokenData) {
      localStorageService.set('token', tokenData);
    }

    function removeToken() {
      localStorageService.remove('token');
    }

    // User info
    function getUserInfo() {
      return localStorageService.get('user');
    }

    function setUserInfo(user) {
      localStorageService.set('user', user);
    }

    function removeUserInfo() {
      localStorageService.remove('user');
    }

    function isAuth() {
      return localStorageService.get('token') !== null;
    }

    function userHasPermission(permission) {
      return _.intersection(permission, getPermissions()).length > 0;
    }
  }
})();
