;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .factory('OverbookingService', OverbookingService);

  OverbookingService.$inject = [
    '$resource',
    '$q',
    'ngAuthSettings'];

  function OverbookingService($resource, $q, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var overbookResource = $resource(serviceBase + 'api/overbooking/:overbookId', {overbookId: '@overbookId'}, {});

    return {
      overbookUser: overbookUser
    };

    function overbookUser(overbook) {
      var deferred = $q.defer();

      overbookResource.save(overbook,
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }
  }
})();
