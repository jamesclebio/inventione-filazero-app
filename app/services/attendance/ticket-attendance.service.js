; (function() {
  'use strict';

  angular
    .module('filazero-app')
    .factory('TicketsAttendanceService', TicketsAttendanceService);

  TicketsAttendanceService.$inject = ['ProviderService', '$resource', '$q', 'ngAuthSettings'];

  function TicketsAttendanceService(ProviderService, $resource, $q, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var ticketResource = $resource(
      serviceBase + 'api/providers/:providerId/tickets/:ticketId',
      { providerId: '@providerId', ticketId: '@ticketId' }
    );
    var ticketsActionsResource = $resource(
      serviceBase + 'api/providers/:providerId/tickets/:ticketId/:action',
      { providerId: '@providerId', ticketId: '@ticketId', action: '@action' }
    );
    var ticketsCallResource = $resource(
      serviceBase + 'api/providers/:providerId/tickets/:ticketId/call/:resourceId',
      { providerId: '@providerId', ticketId: '@ticketId', resourceId: '@resourceId' }
    );

    var ticketPriorityResource = $resource(
      serviceBase + 'api/providers/:providerId/tickets/:ticketId/priority',
      { providerId: '@providerId', ticketId: '@ticketId' }
    );

    return {
      actionOnTicket: function(ticketId, action) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        ticketsActionsResource.save({ providerId: providerId, ticketId: ticketId, action: action },
          function(data) {
            deferred.resolve(data);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      callTicket: function(ticketId, resourceId) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        ticketsCallResource.save({ providerId: providerId, ticketId: ticketId, resourceId: resourceId },
          function(data) {
            deferred.resolve(data);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      getTicket: function(ticketId) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        ticketResource.get({ providerId: providerId, ticketId: ticketId },
          function(data) {
            deferred.resolve(data);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      changePreference: function(ticketId, data){
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        ticketPriorityResource.save({ providerId: providerId, ticketId: ticketId }, data,
          function(data) {
            deferred.resolve(data);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      }
    };
  }
})();
