﻿; (function() {
  'use strict';

  angular
    .module('filazero-app')
    .factory('ServicesAttendanceService', ServicesAttendanceService);

  ServicesAttendanceService.$inject = ['ProviderService', '$resource', '$q', 'ngAuthSettings'];

  function ServicesAttendanceService(ProviderService, $resource, $q, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var resourceServices = $resource(
      serviceBase + 'api/providers/:providerId/locations/:locationId/services',
      { providerId: '@providerId', locationId: '@locationId' }
    );

    return {
      getServices: function(locationId) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        resourceServices.query({ providerId: providerId, locationId: locationId },
          function(data) {
            deferred.resolve(data);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      }
    }
  }
} ());
