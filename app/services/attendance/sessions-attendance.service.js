﻿; (function() {
  'use strict';

  angular
    .module('filazero-app')
    .factory('SessionsAttendanceService', SessionsAttendanceService);

  SessionsAttendanceService.$inject = ['ProviderService', '$resource', '$q', 'ngAuthSettings'];

  function SessionsAttendanceService(ProviderService, $resource, $q, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var basicSessionResource = $resource(
      serviceBase + 'api/providers/:providerId/sessions/:sessionId/tickets-in-attendance',
      { providerId: '@providerId', sessionId: '@sessionId' }
    );
    var callNextResource = $resource(
      serviceBase + 'api/providers/:providerId/session/:sessionId/callnext',
      { providerId: '@providerId', sessionId: '@sessionId' }
    );
    var callNextPriorityResource = $resource(
      serviceBase + 'api/providers/:providerId/sessions/:sessionId/callnextpriority',
      { providerId: '@providerId', sessionId: '@sessionId' }
    );
    var closeSessionResource = $resource(
      serviceBase + 'api/providers/:providerId/sessions/:sessionId/close',
      { providerId: '@providerId', sessionId: '@sessionId' }
    );
    var countTicketsResource = $resource(
      serviceBase + 'api/providers/:providerId/sessions/:sessionId/tickets-count',
      { providerId: '@providerId', sessionId: '@sessionId' }
    );
    var sessionDataResource = $resource(
      serviceBase + 'api/providers/:providerId/sessions/:sessionId',
      { providerId: '@providerId', sessionId: '@sessionId' }
    );
    var sessionsResource = $resource(
      serviceBase + 'api/providers/:providerId/locations/:locationId/sessions/',
      { providerId: '@providerId', locationId: '@locationId' }
    );
    var sessionsResourceV2 = $resource(
      serviceBase + 'api/providers/:providerId/locations/:locationId/sessions/v2',
      { providerId: '@providerId', locationId: '@locationId' }
    );
    var sessionResource = $resource(
      serviceBase + 'api/providers/:providerId/sessions/:sessionId/tickets-session-attendance',
      { providerId: '@providerId', sessionId: '@sessionId' }
    );
    var ticketsActionsResource = $resource(
      serviceBase + 'api/providers/:providerId/tickets/:ticketId/:action',
      { providerId: '@providerId', ticketId: '@ticketId', action: '@action' }
    );

    return {

      actionOnTicket: function(ticketId, action, data) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        ticketsActionsResource.save({ providerId: providerId, ticketId: ticketId, action: action }, data,
          function(data) {
            deferred.resolve(data);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      callNext: function(callNextObject) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        callNextResource.save({ providerId: providerId, sessionId: callNextObject.sessionId }, { resourceId: callNextObject.resourceId, callSpecial: callNextObject.callSpecial },
          function(data) {
            deferred.resolve(data);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      callNextPriorityService: function(callNextObject) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        callNextPriorityResource.save({ providerId: providerId, sessionId: callNextObject.sessionId }, { resourceId: callNextObject.resourceId },
          function(data) {
            deferred.resolve(data);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      closeSession: function(sessionId) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        closeSessionResource.save({ providerId: providerId, sessionId: sessionId },
          function(data) {
            deferred.resolve(data);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      countTicket: function(sessionId) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        countTicketsResource.get({ providerId: providerId, sessionId: sessionId },
          function(data) {
            deferred.resolve(data);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      getBasicSession: function(sessionId) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        basicSessionResource.get({ providerId: providerId, sessionId: sessionId },
          function(data) {
            deferred.resolve(data);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      getSession: function(sessionId) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        sessionResource.query({ providerId: providerId, sessionId: sessionId },
          function(data) {
            deferred.resolve(data);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      getSessionData: function(sessionId) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        sessionDataResource.get({ providerId: providerId, sessionId: sessionId },
          function(data) {
            deferred.resolve(data);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      getSessions: function(locationId, date) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        sessionsResource.query({ providerId: providerId, locationId: locationId, date: date },
          function(data) {
            deferred.resolve(data);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      getSessionsV2: function(locationId, date) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        sessionsResourceV2.query({ providerId: providerId, locationId: locationId, date: date },
          function(data) {
            deferred.resolve(data);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      getResumedTicket: function(ticketId) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        ticketsActionsResource.get({ providerId: providerId, ticketId: ticketId, action: 'ticket-resumed' },
          function(data) {
            deferred.resolve(data);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      }
    };
  }
} ());
