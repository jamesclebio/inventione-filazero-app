﻿;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .factory('ScheduleAttendanceService', ScheduleAttendanceService);

  ScheduleAttendanceService.$inject = ['ProviderService', '$resource', '$q', 'ngAuthSettings'];

  function ScheduleAttendanceService(ProviderService, $resource, $q, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var resourceSessionDaysAvailability = $resource(
      serviceBase + 'api/providers/:providerId/services/:serviceId/available-session-days',
      {providerId: '@providerId', serviceId: '@serviceId'}
    );
    var resourceSessionsAvailability = $resource(
      serviceBase + 'api/providers/:providerId/services/:serviceId/sessions-resources-by-service',
      {providerId: '@providerId', serviceId: '@serviceId'}
    );
    var resourceSession = $resource(
      serviceBase + 'api/providers/:providerId/sessions/:sessionId/schedule-listing',
      {providerId: '@providerId', serviceId: '@sessionId'}
    );
    var resourceCustomers = $resource(
      serviceBase + 'api/providers/:providerId/clients',
      {providerId: '@providerId', searchString: '@searchString'}
    );
    var resourceTicket = $resource(
      serviceBase + 'api/providers/:providerId/sessions/:sessionId/tickets',
      {providerId: '@providerId', sessionId: '@sessionId'}
    );
    var actionsResource = $resource(
      serviceBase + 'api/providers/:providerId/tickets/:ticketId/:action',
      {providerId: '@providerId', ticketId: '@ticketId', action: '@action'}
    );

    return {
      getDatesAvailability: getDatesAvailability,
      getSessionsAvailability: getSessionsAvailability,
      getSession: getSession,
      getCustomers: getCustomers,
      emitTicket: emitTicket,
      ticketAction: ticketAction
    };

    function getDatesAvailability(serviceId) {
      var deferred = $q.defer();
      var providerId = ProviderService.getCurrentProvider().id;

      resourceSessionDaysAvailability.query({providerId: providerId, serviceId: serviceId},
        function (calendarData) {
          deferred.resolve(calendarData);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function getSessionsAvailability(serviceId, date) {
      var deferred = $q.defer();
      var providerId = ProviderService.getCurrentProvider().id;

      resourceSessionsAvailability.get({providerId: providerId, serviceId: serviceId, date: date},
        function (sessions) {
          deferred.resolve(sessions);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function getSession(sessionId, limit, offset) {
      var deferred = $q.defer();
      var providerId = ProviderService.getCurrentProvider().id;

      resourceSession.get({providerId: providerId, sessionId: sessionId, limit: limit, offset: offset},
        function (session) {
          deferred.resolve(session);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function getCustomers(providerId, searchString) {
      var deferred = $q.defer();
      var providerId = ProviderService.getCurrentProvider().id;

      resourceCustomers.query({providerId: providerId, searchString: searchString},
        function (customers) {
          deferred.resolve(customers);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function emitTicket(emitTicketObject) {
      var deferred = $q.defer();
      var providerId = ProviderService.getCurrentProvider().id;

      if (emitTicketObject.customer.email) {
        emitTicketObject.customer.email = emitTicketObject.customer.email.toLowerCase();
      }

      resourceTicket.save({
          providerId: providerId,
          sessionId: emitTicketObject.sessionId,
          customer: emitTicketObject.customer,
          attendanceEstimatedTime: emitTicketObject.attendanceEstimatedTime,
          special: emitTicketObject.isSpecial,
          autoCheckIn: emitTicketObject.autoCheckIn,
          serviceId: emitTicketObject.serviceId
        },
        function (ticket) {
          deferred.resolve(ticket);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function ticketAction(ticketId, actionName) {
      var deferred = $q.defer();
      var providerId = ProviderService.getCurrentProvider().id;

      actionsResource.save({providerId: providerId, ticketId: ticketId, action: actionName},
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }
  }
})();
