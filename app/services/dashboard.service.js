﻿;(function(){
  'use strict';

  angular
    .module('filazero-app')
    .factory('DashboardService', DashboardService);

  DashboardService.$inject = ['$resource', '$q', 'ngAuthSettings', 'ProviderService'];

  function DashboardService($resource, $q, ngAuthSettings, ProviderService) {
    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var resourceTicketsPerDay = $resource(serviceBase + 'api/providers/:providerId/locations/:locationId/chart/ticketsperday',
      {locationId: '@locationId', providerId: '@providerId'});
    var resourceTicketsPerService = $resource(serviceBase + 'api/providers/:providerId/locations/:locationId/ticketsperservice',
      {locationId: '@locationId', providerId: '@providerId'});
    var resourceServicesAverageTimePerMonth = $resource(serviceBase + 'api/locations/:locationId/services/:serviceId/servicesaveragetimepermonth', {
      locationId: '@locationId',
      serviceId: '@serviceId'
    });

    return {
      getServicesAverageTimePerMonthChartData: getServicesAverageTimePerMonthChartData,
      getTicketsPerDayChartData: getTicketsPerDayChartData,
      getTicketsPerServiceChartData: getTicketsPerServiceChartData
    };

    function getServicesAverageTimePerMonthChartData(locationId, serviceId) {
      var deferred = $q.defer();

      resourceServicesAverageTimePerMonth.query({locationId: locationId, serviceId: serviceId},
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function getTicketsPerDayChartData(locationId) {
      var deferred = $q.defer();
      var pid = ProviderService.getCurrentProvider().id;

      resourceTicketsPerDay.query({locationId: locationId, providerId: pid},
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function getTicketsPerServiceChartData(locationId) {
      var deferred = $q.defer();
      var pid = ProviderService.getCurrentProvider().id;

      resourceTicketsPerService.query({locationId: locationId, providerId: pid},
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

  }

})();
