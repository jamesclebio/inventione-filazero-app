(function () {
  'use strict';

  angular
    .module('filazero-app')
    .factory('ViaCepService', ViaCepService);

  ViaCepService.$inject = ['$resource', '$q'];

  function ViaCepService($resource, $q) {
    var resourceAddress = $resource('https://viacep.com.br/ws/:zipCode/json/', {zipCode: '@zipCode'});

    return {
      getAddress: getAddress
    };

    function getAddress(zipCode) {
      var deferred = $q.defer();

      resourceAddress.get({zipCode: zipCode},
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }
  }
})();
