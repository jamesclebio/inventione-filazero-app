; (function () {
  'use strict';

  angular
    .module('filazero-app')
    .factory('ProvidersService', ProvidersService);

  ProvidersService.$inject = ['$resource', '$q', 'ngAuthSettings', '$rootScope'];

  function ProvidersService($resource, $q, ngAuthSettings, $rootScope) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var resourceCategories = $resource(serviceBase + 'api/providers/categories', {}, { 'query': { method: 'GET', isArray: true } });
    var resourceProviders = $resource(serviceBase + 'api/me/providers', {}, { 'update': { method: 'PUT' } });
    var resourceProvider = $resource(serviceBase + 'api/providers/:providerId', { providerId: '@providerId' }, { 'update': { method: 'PUT' } });
    var resourceSlug = $resource(serviceBase + 'api/verify/slug/:slug', { providerId: '@providerId', slug: '@slug' });

    return {
      getProviders: getProviders,
      addProvider: addProvider,
      getProvider: getProvider,
      updateProvider: updateProvider,
      deleteProvider: deleteProvider,
      checkAvailability: checkAvailability,
      getCategories: getCategories
    };

    function getProviders() {
      var deferred = $q.defer();

      resourceProviders.get(
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function getProvider(providerId) {

      var deferred = $q.defer();

      resourceProvider.get({ providerId: providerId },
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function addProvider(provider) {
      var deferred = $q.defer();

      resourceProvider.save(provider,
        function (data) {
          $rootScope.$broadcast('providerListChanged');
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function updateProvider(object) {
      var deferred = $q.defer();

      resourceProvider.update({ providerId: object.provider.id }, object.provider,
        function (data) {
          $rootScope.$broadcast('providerListChanged');
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function deleteProvider(object) {
      var deferred = $q.defer();

      resourceProvider.delete({ providerId: object.provider.id },
        function (data) {
          $rootScope.$broadcast('providerListChanged');
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function checkAvailability(slug, providerId) {
      var deferred = $q.defer();

      resourceSlug.get({ slug: slug, providerId: providerId },
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function getCategories() {
      var deferred = $q.defer();

      resourceCategories.query(
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

  }
})();
