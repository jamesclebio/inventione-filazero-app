; (function () {
  'use strict';

  angular
    .module('filazero-app')
    .factory('ServiceConfigService', ServiceConfigService);

  ServiceConfigService.$inject = [
    '$resource',
    '$q',
    'ngAuthSettings',
    'ProviderService'
  ];

  function ServiceConfigService($resource, $q, ngAuthSettings, ProviderService) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var resourceServiceConfig = $resource(serviceBase + 'api/providers/:providerId/locations/:locationId/serviceConfigs/:serviceConfigId', { providerId: '@providerId', locationId: '@locationId', serviceConfigId: '@serviceConfigId' }, { update: { method: 'PUT' } });
    var resourceServiceConfigV2 = $resource(serviceBase + 'api/providers/:providerId/locations/:locationId/serviceConfigs/:serviceConfigId/v2', { providerId: '@providerId', locationId: '@locationId', serviceConfigId: '@serviceConfigId' }, { update: { method: 'PUT' } });
    var resourceServiceConfigService = $resource(serviceBase + 'api/providers/:providerId/locations/:locationId/serviceConfigs/:serviceConfigId/services/:serviceId', { providerId: '@providerId', locationId: '@locationId', serviceConfigId: '@serviceConfigId', serviceId: '@serviceId' }, { update: { method: 'PUT' } });
    var resourceServiceConfigAttendants = $resource(serviceBase + 'api/providers/:providerId/locations/:locationId/serviceConfigs/:serviceConfigId/attendants/:attendantId', { providerId: '@providerId', locationId: '@locationId', serviceConfigId: '@serviceConfigId', attendantId: '@attendantId' }, { update: { method: 'PUT' } });

    var storedServiceConfig = undefined;

    return {
      getServiceConfigs: getServiceConfigs,
      getServiceConfigsV2: getServiceConfigsV2,
      getServiceConfig: getServiceConfig,
      addServiceConfig: addServiceConfig,
      updateServiceConfig: updateServiceConfig,
      deleteServiceConfig: deleteServiceConfig,
      addServiceOnServiceConfig: addServiceOnServiceConfig,
      removeServiceOnServiceConfig: removeServiceOnServiceConfig,
      addAttendantOnServiceConfig: addAttendantOnServiceConfig,
      removeAttendantOnServiceConfig: removeAttendantOnServiceConfig,
      setCreatedServiceConfig: setCreatedServiceConfig,
      getCreatedServiceConfig: getCreatedServiceConfig
    };

    function getServiceConfigs(locationId) {
      var deferred = $q.defer();
      var providerId = ProviderService.getCurrentProvider().id;

      resourceServiceConfig.query({ providerId: providerId, locationId: locationId }, {},
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function getServiceConfigsV2(locationId) {
      var deferred = $q.defer();
      var providerId = ProviderService.getCurrentProvider().id;

      resourceServiceConfigV2.query({ providerId: providerId, locationId: locationId }, {},
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function getServiceConfig(locationId, serviceConfigId) {
      var deferred = $q.defer();
      var providerId = ProviderService.getCurrentProvider().id;

      resourceServiceConfig.get({ providerId: providerId, locationId: locationId, serviceConfigId: serviceConfigId }, {},
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function addServiceConfig(locationId, serviceConfig, providerId) {
      var deferred = $q.defer();
      var pId = providerId ? providerId : ProviderService.getCurrentProvider().id;

      resourceServiceConfig.save({ providerId: pId, locationId: locationId }, serviceConfig,
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function updateServiceConfig(locationId, serviceConfig) {
      var deferred = $q.defer();
      var providerId = ProviderService.getCurrentProvider().id;

      resourceServiceConfig.update({ providerId: providerId, locationId: locationId, serviceConfigId: serviceConfig.id }, serviceConfig,
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function deleteServiceConfig(locationId, serviceConfigId) {
      var deferred = $q.defer();
      var providerId = ProviderService.getCurrentProvider().id;

      resourceServiceConfig.delete({ providerId: providerId, locationId: locationId, serviceConfigId: serviceConfigId }, {},
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function addServiceOnServiceConfig(locationId, serviceConfigId, serviceId, providerId) {
      var deferred = $q.defer();
      var pId = providerId ? providerId : ProviderService.getCurrentProvider().id;

      resourceServiceConfigService.save({ providerId: pId, locationId: locationId, serviceConfigId: serviceConfigId }, { serviceId: serviceId },
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function removeServiceOnServiceConfig(locationId, serviceConfigId, serviceId) {
      var deferred = $q.defer();
      var providerId = ProviderService.getCurrentProvider().id;

      resourceServiceConfigService.delete({ providerId: providerId, locationId: locationId, serviceConfigId: serviceConfigId, serviceId: serviceId }, {},
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function addAttendantOnServiceConfig(locationId, serviceConfigId, attendantId, providerId) {
      var deferred = $q.defer();
      var pId = providerId ? providerId : ProviderService.getCurrentProvider().id;

      resourceServiceConfigAttendants.save({ providerId: pId, locationId: locationId, serviceConfigId: serviceConfigId }, { attendantId: attendantId },
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function removeAttendantOnServiceConfig(locationId, serviceConfigId, attendantId) {
      var deferred = $q.defer();
      var providerId = ProviderService.getCurrentProvider().id;

      resourceServiceConfigAttendants.delete({ providerId: providerId, locationId: locationId, serviceConfigId: serviceConfigId, attendantId: attendantId }, {},
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function setCreatedServiceConfig(serviceConfig) {
      storedServiceConfig = serviceConfig;
    }

    function getCreatedServiceConfig() {
      var serviceConfig = angular.copy(storedServiceConfig);
      storedServiceConfig = undefined;
      return serviceConfig;
    }

  }

})();
