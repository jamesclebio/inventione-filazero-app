;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .factory('TerminalsService', TerminalsService);

  TerminalsService.$inject = [
    '$resource',
    '$q',
    'ngAuthSettings',
    'ProviderService'
  ];

  function TerminalsService($resource, $q, ngAuthSettings, ProviderService) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var resourceTerminals = $resource(serviceBase + 'api/providers/:providerId/locations/:locationId/terminals/:terminalId', {
      providerId: '@providerId',
      locationId: '@locationId',
      terminalId: '@terminalId'
    }, {update: {method: 'PUT', isArray: false}});

    return {
      getTerminals: getTerminals,
      getTerminal: getTerminal,
      addTerminal: addTerminal,
      updateTerminal: updateTerminal,
      deleteTerminal: deleteTerminal
    };

    function getTerminals(locationId) {
      var deferred = $q.defer();
      var providerId = ProviderService.getCurrentProvider().id;

      resourceTerminals.query({providerId: providerId, locationId: locationId},
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function getTerminal(locationId, terminalId) {
      var deferred = $q.defer();
      var providerId = ProviderService.getCurrentProvider().id;

      resourceTerminals.get({providerId: providerId, locationId: locationId, terminalId: terminalId},
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function addTerminal(saveObject) {
      var deferred = $q.defer();
      var providerId = ProviderService.getCurrentProvider().id;

      resourceTerminals.save({providerId: providerId, locationId: saveObject.locationId}, saveObject.terminal,
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function updateTerminal(editObject) {
      var deferred = $q.defer();
      var providerId = ProviderService.getCurrentProvider().id;

      resourceTerminals.update({
          providerId: providerId,
          locationId: editObject.locationId,
          terminalId: editObject.terminal.id
        }, editObject.terminal,
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function deleteTerminal(terminal) {
      var deferred = $q.defer();
      var providerId = ProviderService.getCurrentProvider().id;

      resourceTerminals.delete({
          providerId: providerId,
          locationId: terminal.locationId,
          terminalId: terminal.id
        },
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }
  }
}());
