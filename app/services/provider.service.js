﻿;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .factory('ProviderService', ProviderService);

  ProviderService.$inject = ['$resource', '$q', 'ngAuthSettings', '$rootScope', 'AuthService', 'ResponseCatcherService'];

  function ProviderService($resource, $q, ngAuthSettings, $rootScope, AuthService, ResponseCatcherService) {
    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var resourceProviders = $resource(serviceBase + 'api/providers/slug/:slug', {slug: 'slug'});

    var currentProvider;

    return {
      getProviderBySlug: getProviderBySlug,
      verifyProvider: verifyProvider,
      getCurrentProvider: getCurrentProvider,
      hasCurrentProvider: hasCurrentProvider,
      isCurrentProvider: isCurrentProvider,
      removeCurrentProvider: removeCurrentProvider
    };

    function getProviderBySlug(slug) {
      var deferred = $q.defer();

      resourceProviders.get({slug: slug},
        function (provider) {
          deferred.resolve(provider);
        },
        function (error) {
          ResponseCatcherService.catchError(error, true);
        });

      return deferred.promise;
    }

    function setCurrentProvider(provider) {
      currentProvider = provider;
      $rootScope.$broadcast('appProviderChanged');
    }

    function getCurrentProvider() {
      return currentProvider;
    }

    function hasCurrentProvider(slug) {
      return !!currentProvider;
    }

    function isCurrentProvider(slug) {
      if (hasCurrentProvider(slug)) {
        return (slug === currentProvider.slug);
      }
    }

    function removeCurrentProvider() {
      if (currentProvider) {
        currentProvider = undefined;
        $rootScope.$broadcast('appProviderChanged');
      }
    }

    function verifyProvider(slug) {
      var deferred = $q.defer();

      if (!isCurrentProvider(slug)) {
        getProviderBySlug(slug).then(
          function (provider) {
            provider.slug = slug;
            setCurrentProvider(provider);

            AuthService.updateCurrentUser(provider.id).then(
              function () {
                deferred.resolve();
              });
          },
          function () {
            deferred.reject(error);
          }
        );
      } else {
        deferred.resolve();
      }

      return deferred.promise;
    }
  }

})();
