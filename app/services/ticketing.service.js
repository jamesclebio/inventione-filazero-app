;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .factory('TicketingService', TicketingService);

  TicketingService.$inject = [
    '$resource',
    '$q',
    'ngAuthSettings',
    'ProviderService'
  ];

  function TicketingService($resource, $q, ngAuthSettings, ProviderService) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var terminalResource = $resource(serviceBase + 'api/providers/:providerId/locations/:locationId/terminals/:terminalId/services/:serviceId/booking', { providerId: '@providerId', locationId: '@locationId', serviceId: '@serviceId', terminalId: '@terminalId' }, { update: { method: 'PUT', isArray: false }});

    return {
      emitTerminalTicket: emitTerminalTicket
    };

    function emitTerminalTicket(locationId, terminalId, serviceId, isSpecial){
      var deferred = $q.defer();
      var providerId = ProviderService.getCurrentProvider().id;

      terminalResource.save({ providerId: providerId, locationId: locationId, serviceId: serviceId, terminalId: terminalId }, isSpecial,
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }
  }
})();
