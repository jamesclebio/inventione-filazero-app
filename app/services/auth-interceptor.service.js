﻿;
(function () {
  'use strict';

  angular
    .module('filazero-app')
    .factory('authInterceptorService', AuthInterceptorService);

  AuthInterceptorService.$inject = [
    '$q',
    '$injector',
    '$location'
  ];


  function AuthInterceptorService($q, $injector, $location) {

    return{
      request: request,
      responseError: responseError
    };

    function request(config) {
      config.headers = config.headers || {};

      if (!(/.*\.html/).test(config.url) && !(/.*\.json/).test(config.url)) {
        var AuthService = $injector.get('AuthService');
        var tokenData = AuthService.getToken();
        if (AuthService.isAuth()) {
          config.headers.Authorization = 'Bearer ' + tokenData.token;
        }
      }
      return config;
    }

    function responseError(rejection) {
      if (rejection.status === 401 && $location.path() !== '/login') {
        var AuthService = $injector.get('AuthService');
        var redirectTo = encodeURIComponent($location.url());

        AuthService.logOut();
        $location.url('/login').search({'redirectTo': redirectTo});
      }
      return $q.reject(rejection);
    }

  }
})();
