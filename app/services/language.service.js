﻿;(function(){
  'use strict';

  angular
    .module('filazero-app')
    .factory('LanguageService', LanguageService);

  LanguageService.$inject = ['$http', '$translate', 'LANGUAGES'];

  function LanguageService($http, $translate, LANGUAGES) {
    return {
      getBy: function (language) {
        if (language == undefined) {
          language = $translate.storage().get('NG_TRANSLATE_LANG_KEY');
        }

        var promise = $http.get('i18n/' + language + '.json').then(
          function (response) {
            return LANGUAGES;
          }
        );
        return promise;
      }
    };
  }

})();
