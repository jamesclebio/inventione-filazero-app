﻿;(function(){
  'use strict';

  angular
    .module('filazero-app')
    .factory('AccountService', AccountService);

  AccountService.$inject = ['$q', 'ngAuthSettings', '$resource'];


  function AccountService($q, ngAuthSettings, $resource) {
    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var resourceUserPassword = $resource(serviceBase + 'api/me/password', {}, {update: {method: 'PUT'}});
    var resourceUser = $resource(serviceBase + 'api/me', {}, {update: {method: 'PUT'}});

    return {
      changePassword: changePassword,
      userInfo: userInfo,
      updateUser: updateUser
    };

    function changePassword(passwordObject) {
      var deferred = $q.defer();

      resourceUserPassword.update({}, passwordObject,
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function userInfo() {
      var deferred = $q.defer();

      resourceUser.get({},
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function updateUser(user) {
      var deferred = $q.defer();

      resourceUser.update({}, user,
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

  }

})();
