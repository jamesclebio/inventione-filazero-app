﻿;(function(){
  angular
    .module('filazero-app')
    .factory('UserProviderService', UserProviderService);

  UserProviderService.$inject = ['$resource', '$q', 'ngAuthSettings'];


  function UserProviderService($resource, $q, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var userProvider = $resource(serviceBase + 'api/providers/:providerId/users/:userId',
      {providerId: '@providerId', userId: '@userId'});

    var usersProvider = $resource(serviceBase + 'api/providers/:providerId/users/search',
      {providerId: '@providerId'});

    var userRoleProvider = $resource(serviceBase + 'api/providers/:providerId/users/:userId/roles/:roleId',
      {providerId: '@providerId', userId: '@userId', roleId: '@roleId'});

    return {
      getUsers: getUsers,
      removeUser: removeUser,
      addRole: addRole,
      deleteRole: deleteRole
    };

    function getUsers(providerId) {

      var deferred = $q.defer();

      usersProvider.query({'providerId': providerId},
        function success(data) {
          deferred.resolve(data);
        },
        function error(data) {
          deferred.reject(data);
        });

      return deferred.promise;
    }

    function removeUser(providerId, userId) {

      var deferred = $q.defer();

      userProvider.delete({'providerId': providerId, 'userId': userId},
        function success(data) {
          deferred.resolve(data);
        },
        function error(data) {
          deferred.reject(data);
        });

      return deferred.promise;
    }

    function addRole(providerId, userId, roleId) {

      var deferred = $q.defer();

      userRoleProvider.save({'providerId': providerId, 'userId': userId, 'roleId': roleId},
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function deleteRole(providerId, userId, roleId) {

      var deferred = $q.defer();

      userRoleProvider.delete({'providerId': providerId, 'userId': userId, 'roleId': roleId},
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

  }

})();
