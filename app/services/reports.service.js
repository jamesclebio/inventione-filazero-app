; (function () {

  angular
    .module('filazero-app')
    .factory('ReportsService', ReportsService);

  ReportsService.$inject = ['ProviderService', '$resource', '$q', 'ngAuthSettings'];

  function ReportsService(ProviderService, $resource, $q, ngAuthSettings) {
    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var resourceAttendanceReport = $resource(
      serviceBase + 'api/providers/:providerId/locations/:locationId/reports/attendance',
      { providerId: '@providerId', locationId: '@locationId' }
    );

    return {
      getAttendanceReport: function (locationId, startDate, endDate) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        resourceAttendanceReport.query({ providerId: providerId, locationId: locationId, startDate: startDate, endDate: endDate },
          function (attendanceReport) {
            deferred.resolve(attendanceReport);
          },
          function (error) {
            deferred.reject(error);
          });

        return deferred.promise;
      }
    }
  };
})();
