; (function () {
  'use strict';

  angular
    .module('filazero-app')
    .factory('SearchService', SearchService);

  SearchService.$inject = [
    '$resource',
    '$q',
    'ngAuthSettings',
    'ProviderService'
  ];

  function SearchService($resource, $q, ngAuthSettings, ProviderService) {
    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var resourceSearch = $resource(serviceBase + 'api/providers/:providerId/search', { providerId: '@providerId' });

    return {
      search: search
    };

    function search(string) {
      var deferred = $q.defer();
      var providerId = ProviderService.getCurrentProvider().id;

      resourceSearch.query({ providerId: providerId, q: string },
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }
  }
})();
