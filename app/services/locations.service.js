﻿; (function() {
  'use strict';

  angular
    .module('filazero-app')
    .factory('LocationsAdminService', LocationsAdminService);

  LocationsAdminService.$inject = [
    '$resource',
    '$q',
    'ngAuthSettings',
    'ProviderService'
  ];

  function LocationsAdminService($resource, $q, ngAuthSettings, ProviderService) {
    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var resourceLocation = $resource(
      serviceBase + 'api/providers/:providerId/locations/:locationId',
      { providerId: '@providerId', locationId: '@locationId' }, { 'update': { method: 'PUT' } }
    );

    return {
      getLocations: getLocations,
      getLocation: getLocation,
      addLocation: addLocation,
      updateLocation: updateLocation,
      deleteLocation: deleteLocation
    };

    function getLocations() {
      var deferred = $q.defer();
      var providerId = ProviderService.getCurrentProvider().id;

      resourceLocation.query({ providerId: providerId },
        function(data) {
          deferred.resolve(data);
        },
        function(error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function getLocation(locationId) {
      var deferred = $q.defer();
      var providerId = ProviderService.getCurrentProvider().id;

      resourceLocation.get({ providerId: providerId, locationId: locationId },
        function(data) {
          deferred.resolve(data);
        },
        function(error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function addLocation(object) {
      var deferred = $q.defer();
      var providerId = ProviderService.getCurrentProvider().id;

      resourceLocation.save({ providerId: providerId }, object,
        function(data) {
          deferred.resolve(data);
        },
        function(error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function updateLocation(editLocationObject) {
      var deferred = $q.defer();
      var providerId = ProviderService.getCurrentProvider().id;

      resourceLocation.update({ providerId: providerId, locationId: editLocationObject.id }, editLocationObject,
        function(data) {
          deferred.resolve(data);
        },
        function(error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function deleteLocation(object) {
      var deferred = $q.defer();
      var providerId = ProviderService.getCurrentProvider().id;

      resourceLocation.delete({ providerId: providerId, locationId: object.locationId },
        function(data) {
          deferred.resolve(data);
        },
        function(error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

  }
})();
