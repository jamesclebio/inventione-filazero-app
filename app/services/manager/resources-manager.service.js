﻿; (function() {
  'use strict';

  angular
    .module('filazero-app')
    .factory('ResourcesManagerService', ['ProviderService', '$resource', '$q', 'ngAuthSettings',
      function(ProviderService, $resource, $q, ngAuthSettings) {

        var serviceBase = ngAuthSettings.apiServiceBaseUri;

        var resourceResources = $resource(
          serviceBase + 'api/providers/:providerId/locations/:locationId/resources/:resourceId',
          { locationId: '@locationId', resourceId: '@resourceId' },
          { 'update': { method: 'PUT' } }
        );
        var resourceUsers = $resource(
          serviceBase + 'api/providers/:providerId/users/search',
          { providerId: '@providerId' }
        );
        var resourceResourcesByService = $resource(
          serviceBase + 'api/providers/:providerId/locations/:locationId/resources',
          { providerId: '@providerId',
            locationId: '@locationId' }
        );

        return {
          getResources: function(locationId) {
            var deferred = $q.defer();
            var providerId = ProviderService.getCurrentProvider().id;

            resourceResources.query({ providerId: providerId, locationId: locationId },
              function(resources) {
                deferred.resolve(resources);
              },
              function(error) {
                deferred.reject(error);
              });

            return deferred.promise;
          },

          getResource: function(locationId, resourceId) {
            var deferred = $q.defer();
            var providerId = ProviderService.getCurrentProvider().id;

            resourceResources.get({ providerId: providerId, locationId: locationId, resourceId: resourceId },
              function(resource) {
                deferred.resolve(resource);
              },
              function(error) {
                deferred.reject(error);
              });

            return deferred.promise;
          },

          getUsers: function(providerId) {
            var deferred = $q.defer();
            var providerId = ProviderService.getCurrentProvider().id;

            resourceUsers.query({ providerId: providerId },
              function(users) {
                deferred.resolve(users);
              },
              function(error) {
                deferred.reject(error);
              });

            return deferred.promise;
          },

          addResource: function(addResourceObject, providerId) {
            var deferred = $q.defer();
            var pid = providerId ? providerId : ProviderService.getCurrentProvider().id;

            resourceResources.save({ providerId: pid, locationId: addResourceObject.locationId }, addResourceObject.resource,
              function(resources) {
                deferred.resolve(resources);
              },
              function(error) {
                deferred.reject(error);
              });

            return deferred.promise;
          },

          editResource: function(editResourceObject) {
            var deferred = $q.defer();
            var providerId = ProviderService.getCurrentProvider().id;

            resourceResources.update({ providerId: providerId, locationId: editResourceObject.locationId, resourceId: editResourceObject.resource.id }, editResourceObject.resource,
              function(resources) {
                deferred.resolve(resources);
              },
              function(error) {
                deferred.reject(error);
              });

            return deferred.promise;
          },

          removeResource: function(removeResourceObject) {
            var deferred = $q.defer();
            var providerId = ProviderService.getCurrentProvider().id;

            resourceResources.delete({ providerId: providerId, locationId: removeResourceObject.locationId, resourceId: removeResourceObject.resource.id },
              function(resources) {
                deferred.resolve(resources);
              },
              function(error) {
                deferred.reject(error);
              });

            return deferred.promise;
          },

          getResourcesByService: function(locationId, serviceId) {
            var deferred = $q.defer();
            var providerId = ProviderService.getCurrentProvider().id;

            resourceResourcesByService.query(
              { providerId: providerId,
                locationId: locationId,
                serviceId: serviceId },
              function(resources) {
                deferred.resolve(resources);
              },
              function(error) {
                deferred.reject(error);
              });

            return deferred.promise;
          }
        }
      }
    ]);
})();
