﻿; (function() {
  angular
    .module('filazero-app')
    .factory('SessionGeneratorManagerService', SessionGeneratorManagerService);

  SessionGeneratorManagerService.$inject = ['ProviderService', '$resource', '$q', 'ngAuthSettings'];

  function SessionGeneratorManagerService(ProviderService, $resource, $q, ngAuthSettings) {
    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var resourceSessionGenerator = $resource(
      serviceBase + 'api/providers/:providerId/locations/:locationId/serviceconfigs/:serviceConfigId/sessions/generate',
      { providerId: '@providerId', locationId: '@locationId', serviceConfigId: '@serviceConfigId' }
    );
    var resourceSessionModelsByProvider = $resource(
      serviceBase + 'api/providers/:providerId/sessionmodels/:sessionModelId',
      { providerId: '@providerId', sessionModelId: '@sessionModelId' }
    );
    var resourceSessionModels = $resource(
      serviceBase + 'api/providers/:providerId/sessionmodels/:sessionModelId',
      { providerId: '@providerId', sessionModelId: '@sessionModelId' }
    );

    return {
      getAvailability: function(locationId) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        resourceSessionGenerator.get({ providerId: providerId, locationId: locationId },
          function(data) {
            deferred.resolve(data);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      getSessionModels: function() {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        resourceSessionModelsByProvider.query({ providerId: providerId },
          function(sessionModels) {
            deferred.resolve(sessionModels);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      addSessionModel: function(sessionModel, providerId) {
        var deferred = $q.defer();
        var pid = providerId ? providerId : ProviderService.getCurrentProvider().id;

        resourceSessionModelsByProvider.save({ providerId: pid }, sessionModel,
          function(data) {
            deferred.resolve(data);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      removeSessionModel: function(sessionModelId) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        resourceSessionModels.delete({ providerId: providerId, sessionModelId: sessionModelId },
          function(data) {
            deferred.resolve(data);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      generate: function(locationId, serviceConfigId, sessionGenerateObject, providerId) {
        var deferred = $q.defer();
        var pid = providerId ? providerId : ProviderService.getCurrentProvider().id;

        resourceSessionGenerator.save({ providerId: pid, locationId: locationId, serviceConfigId: serviceConfigId }, sessionGenerateObject,
          function(data) {
            deferred.resolve(data);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      }
    }
  }
})();
