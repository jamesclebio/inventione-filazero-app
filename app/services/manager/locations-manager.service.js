﻿;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .factory('LocationsManagerService', LocationsManagerService);

  LocationsManagerService.$inject = ['$resource', '$q', 'ngAuthSettings'];

  function LocationsManagerService($resource, $q, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var resourceLocations = $resource(serviceBase + 'api/providers/:providerId/dropdownlocations', {providerId: '@providerId'});

    var currentLocation;

    return {
      getLocations: function (providerId) {
        var deferred = $q.defer();

        resourceLocations.query({providerId: providerId},
          function (locations) {
            deferred.resolve(locations);
          },
          function (error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      setCurrentLocation: function (location) {
        currentLocation = location;
      },

      getCurrentLocation: function () {
        return currentLocation;
      }
    }
  }
})();
