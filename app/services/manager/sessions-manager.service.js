﻿; (function () {
  angular
    .module('filazero-app')
    .factory('SessionsManagerService', SessionsManagerService);

  SessionsManagerService.$inject = [
    'ProviderService',
    '$resource',
    '$q',
    'ngAuthSettings'
  ];

  function SessionsManagerService(ProviderService, $resource, $q, ngAuthSettings) {
    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var resourceSessions = $resource(
      serviceBase + 'api/providers/:providerId/locations/:locationId/sessions',
      { providerId: '@providerId', locationId: '@locationId' }
    );
    var resourceSession = $resource(
      serviceBase + 'api/providers/:providerId/sessions/:sessionId',
      { providerId: '@providerId', serviceId: '@serviceId', sessionId: '@sessionId' },
      { 'update': { method: 'PUT' } }
    );
    var resourceBulkEditSessions = $resource(
      serviceBase + 'api/providers/:providerId/sessions/bulk/edit',
      { providerId: '@providerId' }
    );
    var resourceBulkRemoveSessions = $resource(
      serviceBase + 'api/providers/:providerId/sessions/bulk/remove',
      { providerId: '@providerId' }
    );

    return {
      getSessions: function (locationId, filters) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        var getSessionsParams = {
          providerId: providerId,
          locationId: locationId,
          limit: filters.limit,
          offset: filters.offset,
          serviceId: filters.serviceId,
          startDate: filters.startDate,
          endDate: filters.endDate,
          resourceId: filters.resourceId,
          roomId: filters.roomId,
          serviceConfigs: filters.serviceConfigs
        };

        resourceSessions.get(getSessionsParams,
          function (data) {
            deferred.resolve(data);
          },
          function (error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      editSession: function (object) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        resourceSession.update({ providerId: providerId, sessionId: object.session.id }, object.session,
          function (data) {
            deferred.resolve(data);
          },
          function (error) {
            deferred.reject(error);
          }
        );

        return deferred.promise;
      },

      bulkEditSessions: function (params) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        resourceBulkEditSessions.save({ providerId: providerId }, params,
          function (data) {
            deferred.resolve(data);
          },
          function (error) {
            deferred.reject(error);
          }
        );

        return deferred.promise;
      },

      removeSession: function (sessionId) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        resourceSession.delete({ providerId: providerId, sessionId: sessionId },
          function (data) {
            deferred.resolve(data);
          },
          function (error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      bulkRemoveSessions: function (params) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        resourceBulkRemoveSessions.save({ providerId: providerId }, params,
          function (data) {
            deferred.resolve(data);
          },
          function (error) {
            deferred.reject(error);
          }
        );

        return deferred.promise;
      }
    };
  }
})();
