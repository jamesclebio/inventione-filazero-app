﻿; (function() {
  'use strict';

  angular
    .module('filazero-app')
    .factory('ServicesManagerService', ServicesManagerService);

  ServicesManagerService.$inject = ['$resource', '$q', 'ngAuthSettings', 'ProviderService'];

  function ServicesManagerService($resource, $q, ngAuthSettings, ProviderService) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var resourceServices = $resource(
      serviceBase + 'api/services'
    );
    var resourceServicesInstance = $resource(
      serviceBase + 'api/providers/:providerId/locations/:locationId/services/:serviceId',
      { providerId: '@providerId', locationId: '@locationId', serviceId: '@serviceId' },
      { 'update': { method: 'PUT' } }
    );
    var resourceServicesWithRoomsInstance = $resource(
      serviceBase + 'api/providers/:providerId/locations/:locationId/services/with-rooms',
      { providerId: '@providerId', locationId: '@locationId'},
      { 'update': { method: 'PUT' } }
    );
    var resourceServiceResource = $resource(
      serviceBase + 'api/providers/:providerId/locations/:locationId/services/:serviceId/serviceresource/:serviceResourceId',
      { providerId: '@providerId', locationId: '@locationId', serviceId: '@serviceId', serviceResourceId: '@serviceResourceId' },
      { 'update': { method: 'PUT' } }
    );
    var resourceResourcesRooms = $resource(
      serviceBase + 'api/providers/:providerId/locations/:locationId/resourcesrooms',
      { providerId: '@providerId', locationId: '@locationId' },
      { 'update': { method: 'PUT' } }
    );

    return {
      getServices: function(locationId) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        resourceServicesWithRoomsInstance.query({ providerId: providerId, locationId: locationId },
          function(data) {
            deferred.resolve(data);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      getService: function(locationId, serviceId) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        resourceServicesInstance.get({ providerId: providerId, locationId: locationId, serviceId: serviceId },
          function(data) {
            deferred.resolve(data);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;

      },

      searchServices: function(queryString) {
        var deferred = $q.defer();

        resourceServices.query({ q: queryString },
          function(data) {
            deferred.resolve(data);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      getResourcesRooms: function(locationId) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        resourceResourcesRooms.get({ providerId: providerId, locationId: locationId },
          function(data) {
            deferred.resolve(data);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;

      },

      addService: function(addServiceObject, providerId) {
        var deferred = $q.defer();
        var pid = providerId ? providerId : ProviderService.getCurrentProvider().id;

        resourceServicesInstance.save({ providerId: pid, locationId: addServiceObject.locationId }, addServiceObject.service,
          function(data) {
            deferred.resolve(data);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      addServiceResource: function(locationId, serviceId, serviceResource) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        resourceServiceResource.save({ providerId: providerId, locationId: locationId, serviceId: serviceId }, serviceResource,
          function(data) {
            deferred.resolve(data);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      editService: function(editServiceObject) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        resourceServicesInstance.update({
          providerId: providerId,
          locationId: editServiceObject.locationId,
          serviceId: editServiceObject.service.id
        }, editServiceObject.service,
          function(data) {
            deferred.resolve(data);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      editServiceResource: function(locationId, serviceId, serviceResource) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        resourceServiceResource.update({
          providerId: providerId,
          locationId: locationId,
          serviceId: serviceId,
          serviceResourceId: serviceResource.id
        }, serviceResource,
          function(data) {
            deferred.resolve(data);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      removeService: function(locationId, serviceId) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        resourceServicesInstance.delete({
          providerId: providerId,
          locationId: locationId,
          serviceId: serviceId
        },
          function(data) {
            deferred.resolve(data);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      removeServiceResource: function(locationId, serviceId, serviceResourceId) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        resourceServiceResource.delete({
          providerId: providerId,
          locationId: locationId,
          serviceId: serviceId,
          serviceResourceId: serviceResourceId
        },
          function(data) {
            deferred.resolve(data);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      }
    };
  }
})();
