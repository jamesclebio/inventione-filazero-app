﻿; (function() {
  angular
    .module('filazero-app')
    .factory('RoomsManagerService', RoomsManagerService);

  RoomsManagerService.$inject = ['ProviderService', '$resource', '$q', 'ngAuthSettings'];

  function RoomsManagerService(ProviderService, $resource, $q, ngAuthSettings) {
    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var resourceRooms = $resource(
      serviceBase + 'api/providers/:providerId/locations/:locationId/rooms/:roomId',
      { providerId: '@providerId', locationId: '@locationId', roomId: '@roomId' },
      { 'update': { method: 'PUT' } }
    );

    return {
      getRooms: function(locationId) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        resourceRooms.query({ providerId: providerId, locationId: locationId },
          function(data) {
            deferred.resolve(data);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      getRoom: function(locationId, roomId) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        resourceRooms.get({ providerId: providerId, locationId: locationId, roomId: roomId },
          function(data) {
            deferred.resolve(data);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      addRoom: function(addRoomObject, providerId) {
        var deferred = $q.defer();
        var pid = providerId ? providerId : ProviderService.getCurrentProvider().id;

        resourceRooms.save({ providerId: pid, locationId: addRoomObject.locationId }, addRoomObject.room,
          function(data) {
            deferred.resolve(data);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      updateRoom: function(editRoomObject) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        resourceRooms.update({ providerId: providerId, locationId: editRoomObject.locationId, roomId: editRoomObject.room.id }, editRoomObject.room,
          function(data) {
            deferred.resolve(data);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      removeRoom: function(removeRoomObject) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        resourceRooms.delete({ providerId: providerId, locationId: removeRoomObject.locationId, roomId: removeRoomObject.room.id },
          function(data) {
            deferred.resolve(data);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      }
    }
  };
})();
