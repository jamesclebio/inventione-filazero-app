﻿; (function() {

  angular
    .module('filazero-app')
    .factory('PanelsManagerService', PanelsManagerService);

  PanelsManagerService.$inject = ['ProviderService', '$resource', '$q', 'ngAuthSettings'];

  function PanelsManagerService(ProviderService, $resource, $q, ngAuthSettings) {
    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var resourcePanels = $resource(
      serviceBase + 'api/providers/:providerId/locations/:locationId/panels/:panelId',
      { providerId: '@providerId', locationId: '@locationId', panelId: '@panelId' },
      { 'update': { method: 'PUT' } }
    );

    return {
      getPanels: function(locationId) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        resourcePanels.query({ providerId: providerId, locationId: locationId },
          function(panels) {
            deferred.resolve(panels);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      getPanel: function(locationId, panelId) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        resourcePanels.get({ providerId: providerId, locationId: locationId, panelId: panelId },
          function(panel) {
            deferred.resolve(panel);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      addPanel: function(addPanelObject) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        resourcePanels.save({ providerId: providerId, locationId: addPanelObject.locationId }, addPanelObject.panel,
          function(success) {
            deferred.resolve(success);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      editPanel: function(editPanelObject) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        resourcePanels.update({ providerId: providerId, locationId: editPanelObject.locationId, panelId: editPanelObject.panel.id }, editPanelObject.panel,
          function(success) {
            deferred.resolve(success);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      },

      removePanel: function(locationId, panelId) {
        var deferred = $q.defer();
        var providerId = ProviderService.getCurrentProvider().id;

        resourcePanels.delete({ providerId: providerId, locationId: locationId, panelId: panelId },
          function(success) {
            deferred.resolve(success);
          },
          function(error) {
            deferred.reject(error);
          });

        return deferred.promise;
      }
    }
  };
})();
