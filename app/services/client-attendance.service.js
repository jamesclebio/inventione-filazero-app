;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .factory('ClientsAttendanceService', ClientsAttendanceService);

  ClientsAttendanceService.$inject = [
    '$resource',
    '$q',
    'ngAuthSettings',
    'ProviderService'
  ];

  function ClientsAttendanceService($resource, $q, ngAuthSettings, ProviderService) {
    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var resourceClients = $resource(
      serviceBase + 'api/providers/:providerId/clients/:clientId',
      {providerId: '@providerId', clientId: '@clientId'},
      {'update': {method: 'PUT'}}
    );

    return {
      getClients: getClients,
      getClient: getClient,
      addClient: addClient,
      updateClient: updateClient,
      deleteClient: deleteClient
    };

    function getClients(searchString) {
      var deferred = $q.defer();
      var providerId = ProviderService.getCurrentProvider().id;

      resourceClients.query({providerId: providerId, searchString: searchString},
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function getClient(clientId) {
      var deferred = $q.defer();
      var providerId = ProviderService.getCurrentProvider().id;

      resourceClients.get({providerId: providerId, clientId: clientId},
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function addClient(addClientObject) {
      var deferred = $q.defer();
      var providerId = ProviderService.getCurrentProvider().id;

      addClientObject.client.email = addClientObject.client.email.toLowerCase();

      resourceClients.save({providerId: providerId}, addClientObject.client,
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function updateClient(client) {
      var deferred = $q.defer();
      var pid = ProviderService.getCurrentProvider().id;

      client.email = client.email.toLowerCase();

      resourceClients.update({providerId: pid, clientId: client.id}, client,
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function deleteClient(clientId) {
      var deferred = $q.defer();
      var providerId = ProviderService.getCurrentProvider().id;

      resourceClients.delete({providerId: providerId, clientId: clientId},
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }
  }
})();
