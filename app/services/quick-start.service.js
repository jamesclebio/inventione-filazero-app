﻿;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .factory('QuickStartService', QuickStartService);

  QuickStartService.$inject = [
    '$q',
    'ResponseCatcherService',
    'RoomsManagerService',
    'ResourcesManagerService',
    'SessionGeneratorManagerService',
    'ServicesManagerService',
    'ServiceConfigService'
  ];

  function QuickStartService($q, ResponseCatcherService, RoomsService, ResourcesService, SessionsService, ServicesManagerService, ServiceConfigService) {

    return {
      configureFirstSpecialty: configureFirstSpecialty
    };

    function configureFirstSpecialty(providerId, locationId, user, specialty) {
      var deferred = $q.defer();

      var defaultRoom = {
        room: {
          name: 'Sala 1'
        },
        locationId: locationId
      };

      var defaultResource = {
        resource: {
          name: user.name,
          staffs: [user.email]
        },
        locationId: locationId
      };

      $q.all([
        RoomsService.addRoom(defaultRoom, providerId),
        ResourcesService.addResource(defaultResource, providerId),
        createSessionModels(specialty.sessionModels, providerId)
      ]).then(
        function (response) {
          if (response[0].responseData) {
            var roomId = response[0].responseData.id;
            var resourceId = response[1].responseData.id;
            var sessionModelIds = response[2];
          }

          createService(providerId, locationId, specialty, resourceId, roomId).then(
            function (response) {
              specialty.id = response.responseData.id;
              defaultResource.resource.id = resourceId;
              createServiceConfig(providerId, locationId, defaultResource.resource, specialty, roomId).then(
                function(serviceConfig){
                  generateSessions(providerId, locationId, serviceConfig.id, sessionModelIds, specialty.daysOfWeek).then(
                    function (response) {
                      deferred.resolve(response);
                    },
                    function (error) {
                      deferred.reject(error);
                    }
                  )
                }
              );
            },
            function (error) {
              deferred.reject(error);
            }
          );
        }
      );

      return deferred.promise;
    }

    function createService(providerId, locationId, specialty, resourceId, roomId) {
      var deferred = $q.defer();

      var service = {
        locationId: locationId,
        service: {
          name: specialty.name,
          averageTime: specialty.averageTime,
          restrictedSchedule: specialty.allowsSelfService,
          description: specialty.description,
          tags: [],
          update: true,
          resourcesRooms: [
            {
              resource: {id: resourceId},
              room: {id: roomId}
            }
          ]
        }
      };

      ServicesManagerService.addService(service, providerId).then(
        function (result) {
          ResponseCatcherService.catchSuccess(result).then(
            function () {
              deferred.resolve(result);
            },
            function (error) {
              deferred.reject(error);
            }
          )
        },
        function (error) {
          deferred.reject(error);
        }
      );

      return deferred.promise;
    }

    function createServiceConfig(providerId, locationId, resource, specialty, roomId){
      var deferred = $q.defer();

      var serviceConfig = {
        "name": resource.name + ' - ' + specialty.name,
        "roomId": roomId,
        "selfServiceAvailable": true
      };
      ServiceConfigService.addServiceConfig(locationId, serviceConfig, providerId).then(
        function(response){
          ResponseCatcherService.catchSuccess(response, false).then(
            function(){
              var serviceConfig = response.responseData;
              var promises = [
                ServiceConfigService.addServiceOnServiceConfig(locationId, serviceConfig.id, specialty.id, providerId),
                ServiceConfigService.addAttendantOnServiceConfig(locationId, serviceConfig.id, resource.id, providerId)
              ];

              $q.all(promises).then(
                function(response){
                  promises = [
                    ResponseCatcherService.catchSuccess(response[0], false),
                    ResponseCatcherService.catchSuccess(response[1], false)
                  ];
                  $q.all(promises).then(
                    function(){
                      deferred.resolve(serviceConfig);
                    },
                    function(error){
                      deferred.error(error);
                    }
                  );
                },
                function(error){
                  deferred.reject(error);
                }
              )
            },
            function(error){
              deferred.reject(error);
            }
          )
        },
        function(error){
          deferred.error(error);
        }
      );

      return deferred.promise;
    }

    function generateSessions(providerId, locationId, serviceConfigId, sessionModelIds, daysOfWeek) {
      var deferred = $q.defer();

      var startDate = new Date(new Date().setHours(0, 0, 0, 0));
      var endDate = new Date(new Date().setMonth(startDate.getMonth() + 1));

      var session = {
        weekdays: daysOfWeek,
        endDate: endDate,
        sessionModelIds: sessionModelIds,
        startDate: startDate
      };

      SessionsService.generate(locationId, serviceConfigId, session, providerId).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response, false).then(
            function(){
              deferred.resolve(response)
            },
            function(error){
              deferred.reject(error)
            }
          );
        },
        function (error) {
          deferred.reject(error);
        }
      );

      return deferred.promise;
    }

    function createSessionModels(models, providerId) {
      var deferredModels = $q.defer();

      var sessionModels = angular.copy(models);

      var sessionModelPromises = [];
      angular.forEach(sessionModels, function (value, key) {
        var modelIndex = key + 1;
        value.description = 'Modelo de sessão ' + modelIndex;
        value.start = value.start.getHours() + ':' + value.start.getMinutes();
        value.duration = value.duration.slice(0, 2) + ':' + value.duration.slice(2, 4);
        sessionModelPromises.push(SessionsService.addSessionModel(value, providerId));
      });

      $q.all(sessionModelPromises).then(
        function (results) {
          var sessionModelsAdded = _.map(results,
            function (obj) {
              return obj.responseData.id;
            });
          deferredModels.resolve(sessionModelsAdded);
        });

      return deferredModels.promise;
    }

  }
})();
