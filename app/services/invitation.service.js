﻿;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .factory('InvitationService', InvitationService);

  InvitationService.$inject = ['$q', 'ngAuthSettings', '$resource', 'ProviderService'];

  function InvitationService($q, ngAuthSettings, $resource, ProviderService) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var resourceInvitations = $resource(serviceBase + 'api/providers/:providerId/invitations', {providerId: '@providerId'});
    var resourceInvitation = $resource(serviceBase + 'api/invitations/:token/:action', {token: '@token', action: '@action'});

    return {

      getProviderInvitations: getProviderInvitations,
      getUserInvitations: getUserInvitations,
      getInvitationDetails: getInvitationDetails,
      invite: invite,
      acceptInvitation: acceptInvitation,
      refuseInvitation: refuseInvitation,
      resendInvitation: resendInvitation,
      cancelInvitation: cancelInvitation,
      registerAndAccept: registerAndAccept
    };

    function getProviderInvitations() {
      var deferred = $q.defer();
      var providerId = ProviderService.getCurrentProvider().id;

      resourceInvitations.query({providerId: providerId},
        function (response) {
          deferred.resolve(response);
        },
        function (error) {
          deferred.reject(error);
        }
      );

      return deferred.promise;
    }

    function getUserInvitations(){
      var deferred = $q.defer();

      resourceInvitation.get({},
        function (response) {
          deferred.resolve(response);
        },
        function (error) {
          deferred.reject(error);
        }
      );

      return deferred.promise;
    }

    function getInvitationDetails(token) {
      var deferred = $q.defer();

      resourceInvitation.get({token: token},
        function (response) {
          deferred.resolve(response);
        },
        function (error) {
          deferred.reject(error);
        }
      );

      return deferred.promise;
    }

    function invite(invite) {
      var deferred = $q.defer();
      var providerId = ProviderService.getCurrentProvider().id;

      invite.email = invite.email.toLowerCase();

      resourceInvitations.save({providerId: providerId}, invite,
        function (response) {
          deferred.resolve(response);
        },
        function (error) {
          deferred.reject(error);
        }
      );

      return deferred.promise;
    }

    function acceptInvitation(token) {
      var deferred = $q.defer();

      resourceInvitation.save({token: token, action: 'accept'},
        function (response) {
          deferred.resolve(response);
        },
        function (error) {
          deferred.reject(error);
        }
      );

      return deferred.promise;
    }

    function refuseInvitation(token) {
      var deferred = $q.defer();

      resourceInvitation.save({token: token, action: 'refuse'},
        function (response) {
          deferred.resolve(response);
        },
        function (error) {
          deferred.reject(error);
        }
      );

      return deferred.promise;
    }

    function resendInvitation(token) {
      var deferred = $q.defer();

      resourceInvitation.save({token: token, action: 'resend'},
        function (response) {
          deferred.resolve(response);
        },
        function (error) {
          deferred.reject(error);
        }
      );

      return deferred.promise;
    }

    function cancelInvitation(token) {
      var deferred = $q.defer();

      resourceInvitation.save({token: token, action: 'cancel'},
        function (response) {
          deferred.resolve(response);
        },
        function (error) {
          deferred.reject(error);
        }
      );

      return deferred.promise;
    }

    function registerAndAccept(obj) {
      var deferred = $q.defer();

      var registerData = _.propertyOf(obj)('data');
      registerData.token = obj.token;
      registerData.action = 'register-and-accept';

      resourceInvitation.save(registerData,
        function (response) {
          deferred.resolve(response);
        },
        function (error) {
          deferred.reject(error);
        }
      );

      return deferred.promise;
    }

  }

})();
