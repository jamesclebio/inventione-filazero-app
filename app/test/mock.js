﻿angular
  .module('filazero-app-mock', ['filazero-app', 'ngMockE2E'])
  .run(function ($httpBackend) {
    var attendanceReport = [
      {
        "service": {
          "id": 1,
          "name": "Fisioterapia"
        },
        "tickets": {
          "total": 958,
          "attended": 743,
          "canceled": 73,
          "notAttended": 42
        },
        "feedback": {
          "total": 361,
          "score": 4.2
        }
      },
      {
        "service": {
          "id": 2,
          "name": "Otorrinolaringologia"
        },
        "tickets": {
          "total": 2357,
          "attended": 1266,
          "canceled": 731,
          "notAttended": 147
        },
        "feedback": {
          "total": 1124,
          "score": 2.5
        }
      },
      {
        "service": {
          "id": 3,
          "name": "Alergologia"
        },
        "tickets": {
          "total": 3775,
          "attended": 1346,
          "canceled": 1432,
          "notAttended": 165
        },
        "feedback": {
          "total": 1831,
          "score": 4.1
        }
      }
    ];

    $httpBackend.whenGET(/api\/providers\/\d+\/locations\/\d+\/reports\/attendance/).respond(attendanceReport);

    $httpBackend.whenGET(/api/).passThrough();
    $httpBackend.whenPOST(/api/).passThrough();
    $httpBackend.whenPUT(/api/).passThrough();
    $httpBackend.whenDELETE(/api/).passThrough();
    $httpBackend.whenGET(/token/).passThrough();
    $httpBackend.whenPOST(/token/).passThrough();

    $httpBackend.whenGET(/.*\.html/).passThrough();
    $httpBackend.whenGET(/.*\.json/).passThrough();
    $httpBackend.whenGET(/.*\.jpg/).passThrough();
  });
