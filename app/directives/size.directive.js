(function(){
  'use strict';

  angular
    .module('filazero-app')
    .directive('ngHeight', SizeDirective);

  function SizeDirective(){
    return {
      restrict: 'A',
      scope: {
        height: '=ngHeight'
      },
      link: link
    };

    function link(scope, element, attrs){
      scope.$watch(function() {
        return element[0].clientWidth;
      }, function(value){
        scope.height = element[0].clientHeight;
      });

      scope.$watch(function() {
        return element[0].clientHeight
      }, function(value){
        scope.height = element[0].clientHeight;
      });
    }

  }

})();
