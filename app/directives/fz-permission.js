;
(function () {
  'use strict';

  angular.module("filazero-app")
    .directive('permission', ['AuthService', permission]);

  function permission(AuthService) {
    return {
      restrict: 'A',
      scope: {
        permission: '='
      },

      link: function (scope, elem) {
        scope.$watchCollection(AuthService.getPermissions, function () {
          if (AuthService.userHasPermission(scope.permission)) {
            elem.show();
          } else {
            elem.hide();
          }
        });
      }
    }
  }
}());
