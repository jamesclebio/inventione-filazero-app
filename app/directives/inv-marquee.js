;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .directive('invMarquee', invMarquee);

  function invMarquee() {
    return {
      restrict: 'A',
      scope: {invMarqueeValue: '='},
      template: '{{invMarqueeValue}}',
      link: function (scope, element) {
        scope.$watch('invMarqueeValue', function(newValue){
          main.marquee.destroy(element);
          element[0].innerText = newValue;
          main.marquee.init(element);
        });
      }
    };
  }
}());
