;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .directive('invInputRange', invInputRange);

  function invInputRange() {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        scope.$watch(attrs.rangeValue,function(newvalue){
          main.inputRange.builder(element);
        });

        scope.$watch(attrs.rangeDisabled,function(newvalue){
          main.inputRange.builder(element);
        })
      }
    }
  }
}());
