;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider
      .state('app.access-denied', {
        url: '/403',
        data: {
          requireAuthentication: false
        },
        templateUrl: 'modules/core/errors/403.html'
      })
      .state('app.not-found', {
        url: '/404',
        data: {
          requireAuthentication: false
        },
        templateUrl: 'modules/core/errors/404.html'
      })
      .state('app.internal-error', {
        url: '/500',
        data: {
          requireAuthentication: false
        },
        templateUrl: 'modules/core/errors/500.html'
      })
      .state('app.error', {
        url: '/error',
        data: {
          requireAuthentication: false
        },
        templateUrl: 'modules/core/errors/error.html'
      });
  }
}());
