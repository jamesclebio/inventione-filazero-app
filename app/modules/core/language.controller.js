﻿;(function(){
  'use strict';

  /** Controlador utilizado para modificar a linguagem da aplicação **/
  angular
    .module('filazero-app')
    .controller('LanguageController', LanguageController);

  LanguageController.$inject = ['$scope', '$translate', 'LanguageService', 'tmhDynamicLocale'];

  function LanguageController($scope, $translate, LanguageService, tmhDynamicLocale) {
    $scope.changeLanguage = function (languageKey) {
      $translate.use(languageKey);

      tmhDynamicLocale.set(languageKey);

      LanguageService.getBy(languageKey).then(function (languages) {
        $scope.languages = languages;
      });
    };

    LanguageService.getBy().then(function (languages) {
      $scope.languages = languages;
    });
  }
})();
