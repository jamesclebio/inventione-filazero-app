; (function () {
  'use strict';

  angular.module('filazero-app')
    .config(['$stateProvider', '$urlRouterProvider',

      function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider
          .otherwise('/app/account/providers');

        $stateProvider
          .state('app', {
            abstract: true,
            url: '/app',
            templateUrl: 'layouts/base.html',
            resolve: {
              deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                  'smartsupp',
                  'select',
                  'services/search.service.js',
                  'services/attendance/ticket-attendance.service.js',
                  'services/manager/locations-manager.service.js'
                ], {
                    insertBefore: '#lazyload_placeholder'
                  }).then(function () {
                    return $ocLazyLoad.load([
                      'layouts/location-selector/location-selector.controller.js',
                      'layouts/header/header.controller.js',
                      'layouts/nav/nav.controller.js',
                      'layouts/search/search.controller.js',
                      'modules/attendance/sessions/ticket-details-modal.controller.js'
                    ]);
                  });
              }]
            }
          })
          .state('app.provider', {
            abstract: true,
            url: '/p/:providerSlug',
            template: '<ui-view></ui-view>',
            resolve: {
              deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                  'select'
                ], {
                    insertBefore: '#lazyload_placeholder'
                  }).then(function () {
                    return $ocLazyLoad.load([
                    ]);
                  });
              }],
              currentProvider: ['$stateParams', 'ProviderService', function ($stateParams, ProviderService) {
                return ProviderService.verifyProvider($stateParams.providerSlug).then(
                  function () {
                    return ProviderService.getCurrentProvider();
                  }
                );
              }]
            }
          });
      }
    ]);
})();
