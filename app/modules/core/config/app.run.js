; (function () {
  'use strict';

  angular
    .module('filazero-app')
    .run(run);

  run.$inject = [
    '$rootScope',
    '$state',
    'AuthService',
    '$location',
    '$anchorScroll',
    'tmhDynamicLocale'];

  function run($rootScope, $state, AuthService, $location, $anchorScroll, tmhDynamicLocale) {
    $anchorScroll.yOffset = 70;
    tmhDynamicLocale.set('pt-br');

    // Provider
    $rootScope.$on('$stateChangeStart', function (event, toState) {
      if (toState.data.requireAuthentication && !AuthService.isAuth()) {
        var redirectTo = encodeURIComponent($location.url());
        $state.transitionTo('login', { 'redirectTo': redirectTo});
        event.preventDefault();
      }
    });
  }
})();
