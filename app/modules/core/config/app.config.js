; (function() {
  'use strict';

  angular
    .module('filazero-app')
    .config(config);

  config.$inject = [
    '$locationProvider',
    '$translateProvider',
    'tmhDynamicLocaleProvider',
    'PusherServiceProvider',
    '$httpProvider',
    'laddaProvider',
    'ngAuthSettings'
  ];

  function config($locationProvider, $translateProvider, tmhDynamicLocaleProvider, PusherServiceProvider, $httpProvider, laddaProvider, ngAuthSettings) {
    // Initialize angular-translate
    $translateProvider.useStaticFilesLoader({
      prefix: 'i18n/',
      suffix: '.json'
    });

    $translateProvider.preferredLanguage('pt-br');
    $translateProvider.useCookieStorage();

    tmhDynamicLocaleProvider.localeLocationPattern('assets/plugins/angular-i18n/angular-locale_{{locale}}.js');
    tmhDynamicLocaleProvider.useCookieStorage('NG_TRANSLATE_LANG_KEY');

    // Pusher configuration
    PusherServiceProvider.setToken(ngAuthSettings.pusherId).setOptions({});

    // Interceptor
    $httpProvider.interceptors.push('authInterceptorService');

    // Ladda
    laddaProvider.setOption({
      style: 'expand-left',
      spinnerSize: 30
    });
  }
})();
