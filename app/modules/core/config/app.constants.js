;(function () {
  'use strict';

  // Service base
  window.serviceBase = (function () {
    var hosts = {
      production: {
        regex: /app\.filazero\.net/,
        set: {
          clientId: 'filazeroApp',
          apiServiceBaseUri: 'https://api.filazero.net/',
          urlOrigin: 'https://app.filazero.net/#/',
          site: 'https://filazero.net/#/',
          pusherId: 'caed352c9c960ed6a5a1'
        }
      },

      release: {
        regex: /fzapphmg\.azurewebsites\.net/,
        set: {
          clientId: 'filazeroApp-hmg',
          apiServiceBaseUri: 'https://fzapihmg.azurewebsites.net/',
          urlOrigin: 'https://fzapphmg.azurewebsites.net/#/',
          site: 'https://fzsitehmg.azurewebsites.net/#/',
          pusherId: '0041ab94e110de594bdb'
        }
      },

      development: {
        regex: /fzappdev\.azurewebsites\.net/,
        set: {
          clientId: 'filazeroApp-dev',
          apiServiceBaseUri: 'https://fzapidev.azurewebsites.net/',
          urlOrigin: 'https://fzappdev.azurewebsites.net/#/',
          site: 'https://fzsitedev.azurewebsites.net/#/',
          pusherId: '0041ab94e110de594bdb'
        }
      },

      local: {
        regex: /localhost/,
        port: '3000',
        set: {
          clientId: 'filazeroApp-local',
          apiServiceBaseUri: 'https://localhost:44300/',
          urlOrigin: 'http://localhost:3000/#/',
          site: 'https://fzsitedev.azurewebsites.net/#/',
          pusherId: '0041ab94e110de594bdb'
        }
      },

      localRemote: {
        regex: /localhost/,
        port: '37374',
        set: {
          clientId: 'filazeroApp-localRemote',
          apiServiceBaseUri: 'https://fzapidev.azurewebsites.net/',
          urlOrigin: 'http://localhost:37374/#/',
          site: 'https://fzsitedev.azurewebsites.net/#/',
          pusherId: '0041ab94e110de594bdb'
        }
      }
    };

    for (var host in hosts) {
      if (hosts[host].regex.test(window.location.hostname)) {

        if (!hosts[host].port) {
          return hosts[host].set;
        }

        if (hosts[host].port === window.location.port) {
          return hosts[host].set;
        }
      }
    }
  })();

  angular
    .module('filazero-app')
    .constant('ngAuthSettings', window.serviceBase)
    .constant('LANGUAGES', {
      'en-us': 'English',
      'pt-br': 'Portuguese',
      'es-es': 'Spanish'
    });
}());
