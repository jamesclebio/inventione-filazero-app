;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider.state('app.provider.edit-service-config', {
      url: '/serviceConfig/:serviceConfigId',
      data: {
        requireAuthentication: true,
        serviceConfig: {}
      },
      templateUrl: 'modules/service-config/service-config-manager.html',
      controller: 'ServiceConfigManagerController',
      controllerAs: 'vm',
      resolve: {
        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            'services/manager/services-manager.service.js',
            'services/manager/resources-manager.service.js',
            'services/manager/locations-manager.service.js',
            'services/manager/rooms-manager.service.js',
            'services/service-config.service.js'
          ], {
            insertBefore: '#lazyload_placeholder'
          }).then(function () {
            return $ocLazyLoad.load([
              'modules/service-config/service-config-manager.controller.js'
            ]);
          });
        }]
      }
    });
  }
})();
