;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('ServiceConfigRemoveModalController', ServiceConfigRemoveModalController);

  ServiceConfigRemoveModalController.$inject = [
    'resolve',
    '$uibModalInstance'
  ];

  function ServiceConfigRemoveModalController(resolve, $uibModalInstance) {
    var vm = this;

    vm.isLoading = false;

    vm.cancel = cancel;

    init();

    function init(){
      vm.serviceConfig = resolve;
    }

    function cancel(){
      $uibModalInstance.dismiss();
    }

  }
})();
