; (function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('ServiceConfigManagerController', ServiceConfigController);

  ServiceConfigController.$inject = ['$scope', '$q', '$stateParams', 'ResponseCatcherService', 'ServicesManagerService', 'RoomsManagerService', 'LocationsManagerService', 'ResourcesManagerService', 'ServiceConfigService', 'ProviderService'];

  function ServiceConfigController($scope, $q, $stateParams, ResponseCatcherService, ServicesManagerService, RoomsManagerService, LocationsManagerService, ResourcesManagerService, ServiceConfigService, ProviderService) {
    var vm = this;
    var location = '';

    vm.serviceConfig = {
      'room': {},
      'servicesProvising': [],
      'attendants': [],
      'selfServiceAvailable': false
    };
    vm.provider = ProviderService.getCurrentProvider();

    vm.isSelfServiceAvailable = isSelfServiceAvailable;
    vm.addServiceProvision = addServiceProvision;
    vm.removeServiceProvision = removeServiceProvision;
    vm.isServiceProvisionSelected = isServiceProvisionSelected;
    vm.addAttendant = addAttendant;
    vm.removeAttendant = removeAttendant;
    vm.isAttendantSelected = isAttendantSelected;
    vm.save = save;
    vm.changeConfirmationTimeUnit = changeConfirmationTimeUnit;

    init();

    function init() {
      location = LocationsManagerService.getCurrentLocation();
      if (location) {
        vm.isLoading = true;

        vm.timeUnits = [
          {
            'id': 1,
            'name': 'Dias'
          },
          {
            'id': 2,
            'name': 'Horas'
          },
          {
            'id': 3,
            'name': 'Minutos'
          }
        ];

        var promises = [
          RoomsManagerService.getRooms(location.id),
          ServicesManagerService.getServices(location.id),
          ResourcesManagerService.getResources(location.id)
        ];

        vm.serviceConfig = ServiceConfigService.getCreatedServiceConfig();

        if (!vm.serviceConfig) {
          promises.push(ServiceConfigService.getServiceConfig(location.id, $stateParams.serviceConfigId));
        }

        $q.all(promises).then(
          function (response) {
            vm.rooms = response[0];
            vm.serviceProvisions = response[1];
            vm.attendants = response[2];
            if (!vm.serviceConfig) {
              vm.serviceConfig = response[3];
            }
            initServiceConfig();
          },
          function (error) {
            ResponseCatcherService.catchError(error, true);
          }
        ).finally(function () {
          vm.isLoading = false;
        });
      }
    }

    function addServiceProvision() {
      if (vm.selectedServiceProvision) {
        vm.isLoadingServicesProvisions = true;
        ServiceConfigService.addServiceOnServiceConfig(location.id, vm.serviceConfig.id, vm.selectedServiceProvision.id).then(
          function (response) {
            ResponseCatcherService.catchSuccess(response);
            vm.serviceConfig.serviceProvisions.push(vm.selectedServiceProvision);
            vm.selectedServiceProvision = undefined;
          }
        ).finally(function () {
          vm.isLoadingServicesProvisions = false;
        });
      }
    }

    function removeServiceProvision(serviceProvision) {
      vm.isLoadingServicesProvisions = true;
      ServiceConfigService.removeServiceOnServiceConfig(location.id, vm.serviceConfig.id, serviceProvision.id).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response).then(
            function () {
              for (var i = 0; i < vm.serviceConfig.serviceProvisions.length; i++) {
                if (vm.serviceConfig.serviceProvisions[i].id === serviceProvision.id) {
                  vm.serviceConfig.serviceProvisions.splice(i, 1);
                  break;
                }
              }
            }
          );
        }
      ).finally(function () {
        vm.isLoadingServicesProvisions = false;
      })
    }

    function isServiceProvisionSelected(serviceProvision) {
      return vm.serviceConfig.serviceProvisions.indexOf(serviceProvision) !== -1;
    }

    function addAttendant() {
      if (vm.selectedAttendant) {
        vm.isLoadingAttendants = true;
        ServiceConfigService.addAttendantOnServiceConfig(location.id, vm.serviceConfig.id, vm.selectedAttendant.id).then(
          function (response) {
            ResponseCatcherService.catchSuccess(response);
            vm.serviceConfig.attendants.push(vm.selectedAttendant);
            vm.selectedAttendant = undefined;
          }
        ).finally(function () {
          vm.isLoadingAttendants = false;
        });
      }
    }

    function removeAttendant(attendant) {
      vm.isLoadingAttendants = true;
      ServiceConfigService.removeAttendantOnServiceConfig(location.id, vm.serviceConfig.id, attendant.id).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response).then(
            function () {
              for (var i = 0; i < vm.serviceConfig.attendants.length; i++) {
                if (vm.serviceConfig.attendants[i].id === attendant.id) {
                  vm.serviceConfig.attendants.splice(i, 1);
                  break;
                }
              }
            }
          )
        }
      ).finally(function () {
        vm.isLoadingAttendants = false;
      })
    }

    function isAttendantSelected(attendant) {
      return vm.serviceConfig.attendants.indexOf(attendant) !== -1;
    }

    function save() {
      if (checkServiceConfig(vm.serviceConfig)) {
        var serviceConfigObject = {
          'name': vm.serviceConfig.name,
          'roomId': vm.serviceConfig.room.id,
          'serviceProvisions': _.pluck(vm.serviceConfig.serviceProvisions, 'id'),
          'attendants': _.pluck(vm.serviceConfig.attendants, 'id'),
          'selfServiceAvailable': vm.serviceConfig.selfServiceAvailable === 'true',
          'selfServiceTimeLimit': getTimeInMinutes(vm.time, vm.timeUnit),
          'confirmationTimeLimit': getTimeInMinutes(vm.confirmationTime, vm.confirmationTimeUnit)
        };
        vm.isLoadingSave = true;
        serviceConfigObject.id = vm.serviceConfig.id;
        updateServiceConfig(serviceConfigObject);
      }
    }

    function getTimeInMinutes(time, timeUnit) {
      if (timeUnit.id === 1) {
        return time * 1440;
      }
      if (timeUnit.id === 2) {
        return time * 60;
      }
      if (timeUnit.id === 3) {
        return time;
      }
    }

    function isSelfServiceAvailable() {
      if (vm.serviceConfig) {
        return vm.serviceConfig.selfServiceAvailable === 'true';
      }
    }

    function initServiceConfig() {
      vm.serviceConfig.selfServiceAvailable = vm.serviceConfig.selfServiceAvailable.toString();

      vm.serviceConfig.room = _.find(vm.rooms, function (obj) {
        return obj.id === vm.serviceConfig.roomId;
      });

      vm.time = checkTime(vm.serviceConfig.selfServiceTimeLimit);
      vm.timeUnit = checkTimeUnit(vm.serviceConfig.selfServiceTimeLimit);

      vm.confirmationTime = checkTime(vm.serviceConfig.confirmationTimeLimit);
      vm.confirmationTimeUnit = checkTimeUnit(vm.serviceConfig.confirmationTimeLimit);
      changeConfirmationTimeUnit();

      if (!vm.serviceConfig.serviceProvisions) {
        vm.serviceConfig.serviceProvisions = [];
      } else {
        for (var i = 0; i < vm.serviceConfig.serviceProvisions.length; i++) {
          vm.serviceConfig.serviceProvisions[i] = _.find(vm.serviceProvisions, function (obj) {
            return obj.id === vm.serviceConfig.serviceProvisions[i];
          });
        }
      }
      if (!vm.serviceConfig.attendants) {
        vm.serviceConfig.attendants = [];
      } else {
        for (i = 0; i < vm.serviceConfig.attendants.length; i++) {
          vm.serviceConfig.attendants[i] = _.find(vm.attendants, function (obj) {
            return obj.id === vm.serviceConfig.attendants[i];
          });
        }
      }
    }

    function checkTime(timeLimit) {
      if (!timeLimit || (timeLimit % 1440) === 0) {
        return timeLimit / 1440 || 0;
      } else {
        if ((timeLimit % 60) === 0) {
          return timeLimit / 60;
        } else {
          return timeLimit;
        }
      }
    }

    function checkTimeUnit(timeLimit){
      if (!timeLimit || (timeLimit % 1440) === 0) {
        return { 'id': 1, 'name': 'Dias' };
      } else {
        if ((timeLimit % 60) === 0) {
          return { 'id': 2, 'name': 'Horas' };
        } else {
          return { 'id': 3, 'name': 'Minutos' };
        }
      }
    }

    function checkServiceConfig(serviceConfig) {
      return serviceConfig.name &&
        serviceConfig.name.length >= 3 && serviceConfig.room.id;
    }

    function updateServiceConfig(serviceConfigObject) {
      ServiceConfigService.updateServiceConfig(location.id, serviceConfigObject).then(
        function (response) {
          afterSave(response);
        },
        function (error) {
          ResponseCatcherService.catchError(error, true);
        }
      ).finally(function () {
        vm.isLoadingSave = false;
      })
    }


    function afterSave(response) {
      ResponseCatcherService.catchSuccess(response).then(
        function () {
          var idList = [];
          idList.push(response.responseData.id);
        }
      );
    }

    function changeConfirmationTimeUnit(){
      switch (vm.confirmationTimeUnit.id){
        case 1:
          vm.confirmationMax = 3;
          break;
        case 2:
          vm.confirmationMax = 72;
          break;
        case 3:
          vm.confirmationMax = 4320;
          break;
      }
    }

    $scope.$on('appLocationChanged', function () {
      init();
    });
  }
})();
