;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('ServiceConfigAddModalController', ServiceConfigAddModalController);

  ServiceConfigAddModalController.$inject = ['$uibModalInstance', 'ResponseCatcherService', '$state', 'LocationsManagerService', 'RoomsManagerService', 'ServiceConfigService', 'ProviderService'];

  function ServiceConfigAddModalController($uibModalInstance, ResponseCatcherService, $state, LocationsManagerService, RoomsManagerService, ServiceConfigService, ProviderService) {
    var vm = this;
    var location;
    var provider;

    vm.cancel = cancel;
    vm.save = save;

    init();

    function init(){
      vm.isLoading = true;
      vm.serviceConfig = {};

      provider = ProviderService.getCurrentProvider();
      location = LocationsManagerService.getCurrentLocation();
      RoomsManagerService.getRooms(location.id).then(
        function(rooms){
          vm.rooms = rooms;
        }
      ).finally(function(){
        vm.isLoading = false;
      });
    }

    function cancel(){
      $uibModalInstance.dismiss('cancel');
    }

    function save(form){
      form.$setSubmitted();
      if(form.$valid && vm.serviceConfig.room){
        vm.isLoading = true;
        var serviceConfiguration = {
          name: vm.serviceConfig.name,
          roomId: vm.serviceConfig.room.id
        };
        ServiceConfigService.addServiceConfig(location.id, serviceConfiguration).then(
          function(response){
            ResponseCatcherService.catchSuccess(response);
            serviceConfiguration = response.responseData;
            $state.go('app.provider.edit-service-config', {serviceConfigId: serviceConfiguration.id});
            ServiceConfigService.setCreatedServiceConfig(serviceConfiguration);
            $uibModalInstance.close();
          },
          function(error){
            ResponseCatcherService.catchError(error, true);
          }
        ).finally(function(){
          vm.isLoading = false;
        })
      }
    }
  }
})();
