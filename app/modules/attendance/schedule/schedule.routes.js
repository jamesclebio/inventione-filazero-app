; (function () {
	'use strict';

	angular
		.module('filazero-app')
		.config(['$stateProvider', scheduleRoutes]);

	scheduleRoutes.$inject = ['$stateProvider'];

	function scheduleRoutes($stateProvider) {
		$stateProvider.state('app.provider.scheduledirect', {
			url: '/schedule/services/:serviceId/sessions/:sessionId',
			templateUrl: 'modules/attendance/schedule/schedule.html',
			controller: 'ScheduleController',
			data: {
        requireAuthentication: true
      },
		});

		$stateProvider.state('app.provider.schedule', {
			url: '/schedule',
			templateUrl: 'modules/attendance/schedule/schedule.html',
			controller: 'ScheduleController',
			data: {
        requireAuthentication: true
      },
      resolve: {
        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            'services/attendance/schedule-attendance.service.js',
            'services/attendance/sessions-attendance.service.js',
            'services/attendance/services-attendance.service.js',
            'services/manager/locations-manager.service.js',
            'services/attendance/ticket-attendance.service.js',
            'services/overbooking.service.js'
          ], {
            insertBefore: '#lazyload_placeholder'
          }).then(function () {
            return $ocLazyLoad.load([
              'modules/attendance/schedule/schedule.controller.js',
              'modules/attendance/sessions/ticket-details-modal.controller.js'
            ]);
          });
        }]
      }
		});
	};

})();
