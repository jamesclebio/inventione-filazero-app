﻿;
(function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('ScheduleController', ScheduleController);

  ScheduleController.$inject = [
    '$scope',
    'LocationsManagerService',
    'ScheduleAttendanceService',
    'ServicesAttendanceService',
    '$uibModal',
    '$q',
    'OverbookingService',
    'currentProvider',
    'ResponseCatcherService'
  ];

  function ScheduleController($scope, LocationsManagerService, ScheduleAttendanceService, ServicesAttendanceService, $uibModal, $q, OverbookingService, currentProvider, ResponseCatcherService) {
    var location, provider;
    var datesAvailability;

    $scope.isLoadingServices = true;
    $scope.isLoadingDatesAvailability = false;
    $scope.isLoaded = false;
    $scope.isNewCustomer = false;
    $scope.currentPage = 1;
    $scope.itensLimit = 10;
    $scope.provider = currentProvider;
    $scope.order = 'estimatedTimeTicket';

    $scope.autoCheckIn = false;
    $scope.hasServices = true;
    $scope.selected = {};
    $scope.selected.special = "false";
    $scope.todayDate = new Date();

    // Initialization
    var init = function () {
      location = LocationsManagerService.getCurrentLocation();
      if (location) {
        getServices(location.id).then(
          function () {
            $scope.isLoaded = true;
          },
          function () {
            $scope.isLoaded = true;
          });
      }
    };

    init();
    // /Initialization

    // Listeners
    $scope.$on('appLocationChanged', function () {
      init();
    });

    $scope.$watch('services', function (newValue, oldValue) {
      if (newValue) {
        setSelectedService(newValue[0]);
      }
    });

    $scope.$watch('selected.service', function (newValue, oldValue) {
      if (newValue) {
        datesAvailability = null;
        getDatesAvailability($scope.selected.service.id);
      }
    });

    $scope.$watch('selected.date', function (newValue, oldValue) {
      if (newValue && $scope.selected.service) {
        getSessionsAvailability($scope.selected.service.id);
        $scope.selected.session = null;
      }
    });

    $scope.$watch('selected.session', function (newValue, oldValue) {
      if (newValue && $scope.selected.service) {
        $scope.currentPage = 1;
        $scope.getSession(newValue.id);
        $scope.autoCheckIn = $scope.enableAutoCheckIn();
      }
    });

    $scope.$watch('availability', function (newValue, oldValue) {
      if (newValue) {
        $scope.hasResources = $scope.availability.resources.length > 0;
        $scope.hasSessions = $scope.availability.sessions.length > 0;
      }
    });

    $scope.$watch('session', function (newValue, oldValue) {
      if (newValue) {
        $scope.hasTickets = $scope.session.tickets.length > 0;
      }
    });

    $scope.$watch('itensLimit', function (newValue, oldValue) {
      if (newValue && newValue !== oldValue) {
        $scope.currentPage = 1;
        $scope.getSession($scope.selected.session.id);
      }
    });
    // /Listeners

    // Services
    function getServices(locationId) {
      var deferred = $q.defer();

      $scope.isLoadingServices = true;
      ServicesAttendanceService.getServices(locationId).then(
        function (response) {
          $scope.services = response;
          $scope.hasServices = $scope.services.length > 0;
          deferred.resolve();
        },
        function (error) {
          ResponseCatcherService.catchError(error, true);
          deferred.reject();
        }).finally(function () {
          $scope.isLoadingServices = false;
        });

      return deferred.promise;
    }

    function setSelectedService(service) {
      $scope.selected.service = service;
    }

    // /Services

    // Dates Availability
    function getDatesAvailability(serviceId) {
      $scope.isLoadingDatesAvailability = true;
      ScheduleAttendanceService.getDatesAvailability(serviceId).then(
        function (response) {
          datesAvailability = response;
          for (var i = 0; i < datesAvailability.length; i++) {
            datesAvailability[i].date = new Date(datesAvailability[i].date);
          }
          $scope.selected.date = new Date();

        },
        function (error) {
          ResponseCatcherService.catchError(error, true);
        }
      ).finally(function () {
        $scope.isLoadingDatesAvailability = false;
      });
    }

    $scope.hasDatesAvailability = function () {
      return !!datesAvailability;
    };

    $scope.hasDatesAvailable = function () {
      if (datesAvailability) {
        return datesAvailability.length > 0;
      }
    };

    $scope.goNextDate = function () {
      for (var i = 0; i < datesAvailability.length; i++) {
        if (datesAvailability[i].hasSlotLeft) {
          $scope.selected.date = datesAvailability[i].date;
          break;
        }
      }
    };
    // /Dates Availability

    // Sessions
    function getSessionsAvailability(serviceId) {
      $scope.availability = {
        resources: [],
        sessions: []
      };
      $scope.isLoadingSessionsAvailability = true;
      ScheduleAttendanceService.getSessionsAvailability(serviceId, $scope.selected.date).then(
        function (response) {
          $scope.availability = response;
          $scope.availability.resources.unshift({ "id": 0, "name": "Sem preferência" });
          $scope.selected.resource = $scope.availability.resources[0];
        },
        function (error) {
          ResponseCatcherService.catchError(error, true);
        }
      ).finally(function () {
        $scope.isLoadingSessionsAvailability = false;
      });
    }

    $scope.getSession = function (sessionId) {
      $scope.isLoadingSession = true;
      var offset = ($scope.currentPage - 1) * $scope.itensLimit;
      ScheduleAttendanceService.getSession(sessionId, $scope.itensLimit, offset).then(
        function (response) {
          $scope.session = response;
          $scope.isLoadingSession = false;
        },
        function (error) {
          ResponseCatcherService.catchError(error, true);
        }
      );
    };

    $scope.findService = function (serviceId) {
      return _.find($scope.session.services, function (service) {
        return service.id === serviceId;
      })
    };

    $scope.setOrder = function (order) {
      if (order === $scope.order) {
        $scope.order = '-' + order;
      } else {
        $scope.order = order;
      }
    };

    $scope.sessionHasSlotsLeft = function () {
      if ($scope.session) {
        return $scope.session.slotsLeft !== 0;
      } else {
        return false;
      }
    };

    $scope.hasSelectedSession = function () {
      return $scope.selected.session ? true : false;
    };

    $scope.showSession = function (session) {
      if ($scope.hasSelectedResource()) {
        if ($scope.selected.resource.id === 0) {
          return true;
        }
        return $scope.isSessionOfSelectedResource(session);
      } else {
        return true;
      }
    };

    $scope.isSessionOfSelectedResource = function (session) {
      if ($scope.hasSelectedResource()) {
        return session.resourceId === $scope.selected.resource.id;
      }
      return false;
    };

    $scope.hasSelectedResource = function () {
      return $scope.selected.resource.id !== 0;
    };

    $scope.hasSessionForResource = function (resource) {
      if (resource.id === 0) {
        return true;
      }
      for (var i = 0; i < $scope.availability.sessions.length; i++) {
        if ($scope.availability.sessions[i].resourceId === resource.id) {
          return true;
        }
      }
      return false;
    };

    $scope.resourceNameById = function (resourceId) {
      var resources = [];
      for (var i = 0; i < $scope.availability.resources.length; i++) {
        if (resourceId === $scope.availability.resources[i].id) {
          resources.push($scope.availability.resources[i].name);
        }
      }
      if (resources.length > 1) {
        return resources[0] + ' +' + resources.length;
      } else {
        return resources[0];
      }
    };

    $scope.hasSessionsWithSlots = function () {
      if ($scope.availability) {
        for (var i = 0; i < $scope.availability.sessions.length; i++) {
          if ($scope.availability.sessions[i].hasSlotsLeft) {
            return true;
          }
        }
      }
    };
    // /Sessions

    // Customers
    $scope.getCustomers = function (searchString) {
      if (searchString) {
        ScheduleAttendanceService.getCustomers($scope.provider.id, searchString).then(
          function (customers) {
            $scope.customers = customers;
          },
          function (error) {
            ResponseCatcherService.catchError(error, true);
          }
        );
      }
    };

    $scope.changeCustomerType = function () {
      $scope.isNewCustomer = !$scope.isNewCustomer;
      clearFunction();
    };

    function clearFunction() {
      $scope.selected.customer = null;
    }

    // /Customers

    // Datepicker
    function getDayClass(calendarData) {
      var date = calendarData.date;
      var mode = calendarData.mode;

      if (mode === 'day') {
        var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

        for (var i = 0; i < datesAvailability.length; i++) {
          var currentDay = new Date(datesAvailability[i].date).setHours(0, 0, 0, 0);

          if (dayToCheck === currentDay) {
            if (datesAvailability[i].hasSlotLeft) {
              return 'available';
            } else {
              return 'unavailable';
            }
          }
        }
      }
    };

    $scope.datepickerOptions = {
      showWeeks: false,
      customClass: getDayClass
    };
    // /Datepicker

    // Ticket
    $scope.pastDate = function () {
      var today = new Date();
      today.setHours(0, 0, 0, 0);

      if ($scope.selected.date) {
        var selectedDate = angular.copy($scope.selected.date);
        selectedDate.setHours(0, 0, 0, 0);

        return selectedDate < today;
      }
      return false;
    };

    $scope.enableAutoCheckIn = function () {
      var date = new Date();
      return (new Date($scope.selected.session.start) <= date && new Date($scope.selected.session.end) >= date);
    };

    $scope.emitTicket = function () {
      $scope.isLoadingEmission = true;
      var emitTicketObject = {
        sessionId: $scope.selected.session.id,
        customer: $scope.selected.customer,
        attendanceEstimatedTime: $scope.session.averageTime,
        isSpecial: $scope.selected.special === "true",
        providerId: $scope.provider.id,
        autoCheckIn: $scope.autoCheckIn,
        serviceId: $scope.selected.service.id
      };
      ScheduleAttendanceService.emitTicket(emitTicketObject).then(
        function (response) {
          $scope.getSession($scope.selected.session.id);
          resetTicketStatus();
          $scope.isLoadingEmission = false;
          ResponseCatcherService.catchSuccess(response).then(
            function () {
              $uibModal.open({
                templateUrl: 'modules/attendance/sessions/ticket-details-modal.html',
                controller: 'TicketDetailsModalController',
                windowClass: 'stick-up',
                controllerAs: 'vm',
                resolve: {
                  resolve: function () {
                    return {
                      ticketId: response.messages[0].parameters.ticket.id
                    }
                      ;
                  }
                }
              });
            }
          )
        },
        function (error) {
          ResponseCatcherService.catchError(error);
        }).finally(function () {
          $scope.ticketForm.$setUntouched();
        });
    };

    $scope.isConfirmed = function (ticket) {
      return ticket.status.toLowerCase() !== 'authorized' && ticket.status.toLowerCase() !== 'cancelled';
    };

    $scope.canEmitTicket = function () {
      return $scope.selected.session && $scope.selected.customer && $scope.selected.customer.name && $scope.selected.customer.email && $scope.selected.customer.phone;
    };

    $scope.canConfirmTicket = function (ticket) {
      for (var i = 0; i < ticket.actions.length; i++) {
        if (ticket.actions[i].toLowerCase() === 'confirm') {
          return true;
        }
      }
    };

    $scope.canCancelTicket = function (ticket) {
      for (var i = 0; i < ticket.actions.length; i++) {
        if (ticket.actions[i].toLowerCase() === 'cancel') {
          return true;
        }
      }
    };

    $scope.doTicketAction = function (ticketId, action) {
      $scope.isLoadingSession = true;
      ScheduleAttendanceService.ticketAction(ticketId, action).then(
        function () {
          $scope.isLoadingSession = false;
          $scope.getSession($scope.selected.session.id);
        });
    };

    $scope.ticketDetails = function (ticketId) {
      $uibModal.open({
        templateUrl: 'modules/attendance/sessions/ticket-details-modal.html',
        controller: 'TicketDetailsModalController',
        windowClass: 'stick-up',
        controllerAs: 'vm',
        resolve: {
          resolve: function () {
            return {
              ticketId: ticketId
            };
          }
        }
      });
    };

    function resetTicketStatus() {
      $scope.selected.customer = null;
      $scope.selected.special = "false";
    }

    // /Ticket

    // Overbook
    $scope.canOverbook = function () {
      return $scope.selected.customer && $scope.selected.customer.name && $scope.selected.customer.email && $scope.selected.customer.phone;
    };

    $scope.overbook = function () {
      $scope.isLoadingEmission = true;
      var overbookObject = {
        serviceId: $scope.selected.service.id,
        name: $scope.selected.customer.name,
        email: $scope.selected.customer.email,
        phone: $scope.selected.customer.phone,
        id: $scope.selected.customer.id
      };
      OverbookingService.overbookUser(overbookObject).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response).then(
            function () {
              resetTicketStatus();
            }
          )
        },
        function (error) {
          ResponseCatcherService.catchError(error);
        }
      ).finally(function () {
        $scope.isLoadingEmission = false;
      })
    };
    // /Overbook
  }
})();
