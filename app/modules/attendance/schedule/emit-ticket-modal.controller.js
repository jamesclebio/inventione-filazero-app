;
(function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('EmitTicketModalController', EmitTicketModalController);

  EmitTicketModalController.$inject = ['$uibModalInstance', 'ScheduleAttendanceService', 'resolve', '$uibModal', 'ResponseCatcherService', 'ClientsAttendanceService'];

  function EmitTicketModalController($uibModalInstance, ScheduleAttendanceService, resolve, $uibModal, ResponseCatcherService, ClientsAttendanceService) {
    var vm = this;

    vm.selectedCustomer = {};
    vm.selectedSpecial = 'false';
    vm.providerId = resolve.providerId;
    vm.services = resolve.session.services;

    vm.customers = [];

    vm.cancel = cancel;
    vm.canEmitTicket = canEmitTicket;
    vm.changeCustomerType = changeCustomerType;
    vm.emitTicket = emitTicket;
    vm.getCustomers = getCustomers;

    init();

    function init(){
      if(vm.services.length === 1){
        vm.selectedService = vm.services[0];
      }
    }

    function cancel() {
      $uibModalInstance.close();
    }

    function canEmitTicket() {
      return vm.selectedCustomer && vm.selectedCustomer.name &&
        vm.selectedCustomer.email && vm.selectedCustomer.phone &&
        vm.selectedService && vm.selectedService.id;
    }

    function changeCustomerType() {
      vm.isNewCustomer = !vm.isNewCustomer;
      clearFunction();
    }

    function clearFunction() {
      vm.selectedCustomer = null;
    }

    function emitTicket() {
      vm.isLoading = true;
      var emitTicketObject = {
        sessionId: resolve.session.id,
        customer: vm.selectedCustomer,
        attendanceEstimatedTime: 15,
        isSpecial: vm.selectedSpecial === "true",
        providerId: resolve.providerId,
        serviceId: vm.selectedService.id
      };
      ScheduleAttendanceService.emitTicket(emitTicketObject).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response).then(
            function () {
              $uibModal.open({
                templateUrl: 'modules/attendance/sessions/ticket-details-modal.html',
                controller: 'TicketDetailsModalController',
                windowClass: 'stick-up',
                controllerAs: 'vm',
                resolve: {
                  resolve: function () {
                    return {
                      ticketId: response.messages[0].parameters.ticket.id
                    };
                  }
                }
              });
              cancel();
            }
          )
        },
        function (error) {
          ResponseCatcherService.catchError(error);
        }).finally(function(){
        vm.isLoading = false;
      });
    }

    function getCustomers(searchString) {
      if (searchString) {
        ClientsAttendanceService.getClients(searchString).then(
          function (customers) {
            vm.customers = customers;
          },
          function (error) {
            ResponseCatcherService.catchError(error, true);
            $uibModalInstance.close();
          }
        );
      }
    }

  }
})();
