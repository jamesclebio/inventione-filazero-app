;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider
      .state('panel', {
        url: '/p/:providerSlug/l/:locationId/panel/:panelId',
        data: {
          requireAuthentication: true
        },
        templateUrl: 'modules/attendance/panel/panel-attendance.html',
        controller: 'PanelAttendanceController',
        controllerAs: 'vm',
        resolve: {
          deps: ['$ocLazyLoad', function($ocLazyLoad) {
            return $ocLazyLoad.load([
              'nvd3',
              'animate-css',
              'services/panel.service.js',
              'services/attendance/sessions-attendance.service.js',
              'jquery.marquee'
            ], {
              insertBefore: '#lazyload_placeholder'
            }).then(function() {
              return $ocLazyLoad.load([
                'modules/attendance/panel/panel-attendance.controller.js'
              ]);
            });
          }],
          currentProvider: ['$stateParams', 'ProviderService', function($stateParams, ProviderService) {
            return ProviderService.verifyProvider($stateParams.providerSlug).then(
              function() {
                return ProviderService.getCurrentProvider();
              }
            );
          }]
        }
      });
  }
})();

