;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('PanelAttendanceController', PanelAttendanceController);

  PanelAttendanceController.$inject = [
    '$scope',
    '$timeout',
    '$q',
    '$stateParams',
    'ngAudio',
    'Pusher',
    'PanelService',
    'SessionsAttendanceService'
  ];

  function PanelAttendanceController($scope, $timeout, $q, $stateParams, ngAudio, Pusher, PanelService, SessionsAttendanceService) {
    var vm = this;

    vm.panel = {};

    vm.panelSound = ngAudio.load("assets/sounds/panel-notification.mp3");

    // Customer Services called
    vm.callsOnQueue = [];

    // Size
    vm.rowHeight = 0;
    vm.fullHeight = 0;
    vm.limit = 1;

    //Header
    vm.now = new Date();

    //Rotation
    vm.index = 0;
    var tempIndex, timer = 0;

    // Guide
    vm.showGuideTour = false;

    vm.findObjectInList = findObjectInList;
    vm.hasCustomerService = hasCustomerService;

    // Show/Hide columns
    vm.showColumn = showColumn;

    // Status
    vm.isCustomerServiceCalled = isCustomerServiceCalled;
    vm.customerServiceClasses = customerServiceClasses;

    // Order
    vm.orderBySession = orderBySession;
    vm.orderByStatus = orderByStatus;

    vm.customerName = customerName;

    $scope.$watch('vm.fullHeight', function () {
      calculateLimit();
    });

    $scope.$watch('vm.rowHeight', function () {
      calculateLimit();
    });

    init();

    function init() {
      updateTime();
      getPanelData($stateParams.locationId, $stateParams.panelId);
      rotatePanel();
      $timeout(showInfoImage, 300000);
    }

    //Time
    function updateTime() {
      vm.now = new Date();
      $timeout(updateTime, 1000);
    }

    function calculateLimit() {
      var limit = Math.round((vm.fullHeight - vm.headerHeight - vm.footerHeight - 90) / vm.rowHeight);
      if (!isNaN(limit)) {
        vm.limit = limit;
      }
    }

    // Get panel info
    function getPanelData(locationId, panelId) {
      var deferred = $q.defer();

      PanelService.getPanelData(locationId, panelId).then(
        function (panelData) {
          buildPanelObject(panelData);
          subscribeSessions();
          deferred.resolve();
        }
      );

      return deferred.promise;
    }

    // Build panel object
    function buildPanelObject(panelData) {
      vm.panel = {
        id: panelData.id,
        transitionTime: panelData.transitionTime,
        showAttendantOnCall: panelData.showAttendantOnCall,
        showAttendantOnListing: panelData.showAttendantOnListing,
        showCustomerOnCall: panelData.showCustomerOnCall,
        showCustomerOnListing: panelData.showCustomerOnListing,
        showRoomOnCall: panelData.showRoomOnCall,
        showRoomOnListing: panelData.showRoomOnListing,
        showServiceOnCall: panelData.showServiceOnCall,
        showServiceOnListing: panelData.showServiceOnListing
      };

      vm.serviceConfigs = panelData.serviceConfigs;
      _.forEach(vm.serviceConfigs, function (serviceConfig) {
        _.forEach(serviceConfig.serviceProvisions, function (service) {
          if (serviceConfig.concatedName) {
            serviceConfig.concatedName += ' - ' + service.name;
          } else {
            serviceConfig.concatedName = service.name;
          }
        })
      })
    }

    function customerName(customer) {
      if (customer.email.indexOf("@terminal") === -1) {
        return customer.name;
      } else {
        return 'Autoatendimento';
      }
    }

    function subscribeSessions() {
      _.each(vm.serviceConfigs, function (serviceConfig) {
        _.each(serviceConfig.serviceSessions, function (session) {
          subscribeSession(session.id);
        })
      })
    }

    function subscribeSession(sessionId) {
      Pusher.subscribe('session-' + sessionId, 'TicketNew', function (data) {

      });

      Pusher.subscribe('session-' + sessionId, 'TicketConfirm', function (data) {
        data.ticket.serviceProvisionId = data.ServiceId;
        insertCustomerServiceOnSessions(data.ticket, sessionId);
      });

      Pusher.subscribe('session-' + sessionId, 'TicketCheckin', function (data) {
        checkCustomerService(data.TicketId, sessionId);
      });

      Pusher.subscribe('session-' + sessionId, 'TicketCalled', function (data) {
        callCustomerService(data.TicketId, data.ticket.resource.id);
      });

      Pusher.subscribe('session-' + sessionId, 'TicketStart', function (data) {
        startCustomerService(data.TicketId);
      });

      Pusher.subscribe('session-' + sessionId, 'TicketReopened', function (data) {
        removeCustomerService(data.TicketId);
      });

      Pusher.subscribe('session-' + sessionId, 'TicketCancel', function (data) {
        removeCustomerService(data.TicketId);
      });

      Pusher.subscribe('session-' + sessionId, 'PrevisionChanged', function (data) {
        updateCustomerServicePrevision(data.TicketId, data.EstimatedServiceStart);
      });
    }

    // On confirmation event
    function getCustomerService(customerServiceId) {
      var deferred = $q.defer();

      SessionsAttendanceService.getResumedTicket(customerServiceId).then(
        function (customerService) {
          deferred.resolve(customerService);
        },
        function (error) {
          deferred.reject(error);
        }
      );

      return deferred.promise;
    }

    function insertCustomerServiceOnSessions(customerService, sessionId) {
      var index = _.findIndex(vm.serviceConfigs, function (serviceConfig) {
        return _.find(serviceConfig.serviceSessions, function (session) {
            return session.id === sessionId
          }) !== undefined;
      });

      customerService.forecast = customerService.prevision;
      customerService.priority = customerService.isSpecial;
      customerService.serviceSessionId = sessionId;

      if (vm.serviceConfigs[index].attendants.length === 1) {
        customerService.attendantId = vm.serviceConfigs[index].attendants[0].id;
      }


      vm.serviceConfigs[index].customerServices.push(customerService);
    }

    // On Checkin event
    function checkCustomerService(customerServiceId) {
      actOnCustomerService(customerServiceId, function (customerService) {
        customerService.status = 'Checked'
      })
    }

    // on Call event
    function callCustomerService(customerServiceId, attendantId) {
      vm.showGuideTour = false;
      timer.pause();
      actOnCustomerService(customerServiceId, function (customerService) {
        customerService.status = 'called';
        customerService.attendantId = attendantId;
        vm.callsOnQueue.push(customerService);
        vm.panelSound.loop = 1;
        vm.panelSound.play();
        if (vm.callsOnQueue.length === 1) {
          vm.currentCall = customerService;
          tempIndex = vm.index;
          setCurrentServiceConfig();
          $timeout(clearCurrentCall, 5000)
        }
      })
    }

    function clearCurrentCall() {
      vm.callsOnQueue.shift();
      if (vm.callsOnQueue.length > 0) {
        vm.currentCall = vm.callsOnQueue[0];
        setCurrentServiceConfig();
        $timeout(clearCurrentCall, 5000)
      } else {
        vm.index = tempIndex;
        timer.resume();
        vm.currentCall = undefined;
      }
    }

    function setCurrentServiceConfig() {
      if (vm.currentCall) {
        vm.index = _.findIndex(vm.serviceConfigs, function (serviceConfig) {
          return _.findIndex(serviceConfig.customerServices, function (customerService) {
              return vm.currentCall.id === customerService.id;
            }) !== -1;
        })
      }
    }

    // on Start event
    function startCustomerService(customerServiceId) {
      var index = _.findIndex(vm.callsOnQueue, function (call) {
        return customerServiceId === call.id;
      });
      if (index === -1) {
        removeCustomerService(customerServiceId);
      } else {
        $timeout(function () {
          startCustomerService(customerServiceId)
        }, 5000);
      }
    }

    // on Cancel and Reopen event
    function removeCustomerService(customerServiceId) {
      var serviceCustomerIndex = -1;
      var serviceConfigIndex = _.findIndex(vm.serviceConfigs, function (serviceConfig) {
        serviceCustomerIndex = _.findIndex(serviceConfig.customerServices, function (customerService) {
          return customerServiceId === customerService.id;
        });
        return serviceCustomerIndex !== -1;
      });

      vm.serviceConfigs[serviceConfigIndex].customerServices.splice(serviceCustomerIndex, 1);
    }

    // on PrevisionChanged event
    function updateCustomerServicePrevision(customerServiceId, customerForecast) {
      actOnCustomerService(customerServiceId, function (customerService) {
        customerService.forecast = customerForecast;
      })
    }

    // Do a action on the customer service of a session
    function actOnCustomerService(customerServiceId, action) {
      _.forEach(vm.serviceConfigs, function (serviceConfig) {
        _.find(serviceConfig.customerServices, function (customerService) {
          if (customerService.id === customerServiceId) {
            action(customerService);
          }
          return customerService.id === customerServiceId
        })
      })
    }

    function findObjectInList(list, objectId) {
      if (list && list.length > 0 && objectId) {
        return _.find(list, function (object) {
          return object.id === objectId;
        })
      }
    }

    // Order
    function orderBySession() {
      return function (serviceConfig) {
        var session = findObjectInList(vm.serviceConfigs[vm.index].serviceSessions, serviceConfig.serviceSessionId);
        if (session) {
          return session.estimatedStart;
        }
      }
    }

    function orderByStatus() {
      return function (serviceConfig) {
        switch (serviceConfig.status.toLowerCase()) {
          case 'called':
            return '0';

          default:
            return '1';
        }
      }
    }

    //Rotation
    function rotatePanel() {
      if (hasCustomerService(vm.serviceConfigs)) {
        var index = angular.copy(checkSize(vm.serviceConfigs, vm.index));

        while (vm.serviceConfigs[vm.index].id !== vm.serviceConfigs[index].id) {
          if (vm.serviceConfigs[index].customerServices.length > 0) {
            vm.index = index;
          } else {
            index = angular.copy(checkSize(vm.serviceConfigs, index));
          }
        }
      }
      timer = new Timer(rotatePanel, vm.panel.transitionTime * 1000, $timeout);
    }

    // Check empty list and go back to start if it is
    function checkSize(list, index) {
      if (list[index + 1]) {
        return index + 1;
      } else {
        return 0;
      }
    }

    // Check if any service config has customer service
    function hasCustomerService(serviceConfigs) {
      if (serviceConfigs) {
        for (var i = 0; i < serviceConfigs.length; i++) {
          if (serviceConfigs[i].customerServices.length > 0) {
            return true;
          }
        }
      } else {
        return false;
      }
    }

    // Guide Tour
    function showInfoImage() {
      vm.showGuideTour = true;
      $timeout(function () {
        vm.showGuideTour = false;
        $timeout(showInfoImage, 3000000);
      }, 30000);
    }

    // Customer service status control
    function customerServiceClasses(customerService) {
      var classes = '';
      if (customerService.status.toLowerCase() !== 'confirmed') {
        classes += 'checked '
      }
      if (customerService.priority) {
        classes += 'priority '
      }
      if (customerService.status.toLowerCase() === 'called') {
        classes += 'called '
      }
      if (vm.currentCall && customerService.id === vm.currentCall.id) {
        classes += 'zoom'
      }
      return classes;
    }

    function isCustomerServiceCalled(customerService) {
      return customerService.status.toLowerCase() === 'called';
    }

    // Show/Hide columns
    function showColumn(panelConfigurationOnList, panelConfigurationOnCall, customerService) {
      return panelConfigurationOnList || (panelConfigurationOnCall && vm.currentCall && customerService && customerService.id === vm.currentCall.id) || (!customerService && vm.currentCall && panelConfigurationOnCall);
    }
  }

  function Timer(callback, delay, $timeout) {
    var timerId, start, remaining = delay;

    this.pause = function () {
      $timeout.cancel(timerId);
      remaining -= new Date() - start;
    };

    this.resume = function () {
      start = new Date();
      $timeout.cancel(timerId);
      timerId = $timeout(callback, remaining);
    };

    this.resume();
  }

})();
