;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider.state('app.provider.print-session', {
      url: '/sessions/:sessionId/print-session?:orderBy',
      data: {
        requireAuthentication: true
      },
      templateUrl: 'modules/attendance/print-session/print-session.html',
      controller: 'PrintSessionController',
      controllerAs: 'vm',
      resolve: {
        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            'services/attendance/schedule-attendance.service.js'
          ], {
            insertBefore: '#lazyload_placeholder'
          }).then(function () {
            return $ocLazyLoad.load([
              'modules/attendance/print-session/print-session.controller.js'
            ]);
          });
        }]
      }
    });
  }
})();
