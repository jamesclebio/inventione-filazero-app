;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('PrintSessionController', PrintSessionController);

  PrintSessionController.$inject = [
    '$stateParams',
    '$timeout',
    'ScheduleAttendanceService'
  ];

  function PrintSessionController($stateParams, $timeout, ScheduleAttendanceService) {
    var vm = this;

    vm.print = print;
    vm.isConfirmed = isConfirmed;
    vm.findService = findService;
    vm.setOrder = setOrder;

    init();

    function init() {
      getSession($stateParams.sessionId);
      vm.order = $stateParams.orderBy;
    }

    function getSession(sessionId) {
      ScheduleAttendanceService.getSession(sessionId).then(
        function (session) {
          vm.session = session;
          $timeout(print, 1000);
        },
        function (error) {
          ResponseCatcherService.catchError(error, true);
        }
      )
    }

    function isConfirmed(ticket){
      return ticket.status.toLowerCase() !== 'authorized' && ticket.status.toLowerCase() !== 'reopened';
    }

    function print(){
      window.print();
    }

    function findService(serviceProvisionId){
      return _.find(vm.session.services, function(service){
        return service.id === serviceProvisionId;
      })
    }

    function setOrder(order){
      if (order === vm.order) {
        vm.order = '-' + order;
      } else {
        vm.order = order;
      }
    }

  }
})();
