; (function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('ClientRemoveModalController', ClientRemoveModalController);

  ClientRemoveModalController.$inject = [
    '$uibModalInstance',
    'ClientsAttendanceService',
    'resolve',
    'ResponseCatcherService'
  ];

  function ClientRemoveModalController($uibModalInstance, ClientsAttendanceService, resolve, ResponseCatcherService) {
    var vm = this;

    vm.client = resolve.client;
    vm.isLoading = false;

    vm.cancel = cancel;
    vm.confirm = confirm;

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }

    function confirm() {
      vm.isLoading = true;
      ClientsAttendanceService.deleteClient(vm.client.id).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response).then(
            function (messages) {
              $uibModalInstance.close();
            }
          );
        },
        function (error) {
          ResponseCatcherService.catchError(error);
        }).finally(function () {
          vm.isLoading = false;
        });
    }
  }
})();
