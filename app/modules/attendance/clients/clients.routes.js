; (function () {
  'use strict';

  angular
    .module('filazero-app')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider.state('app.provider.clients', {
      url: '/clients',
      data: {
        requireAuthentication: true
      },
      template: '<clients current-provider="$resolve.currentProvider"></clients>',
      resolve: {
        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            'services/client-attendance.service.js',
            'services/attendance/ticket-attendance.service.js'
          ], {
              insertBefore: '#lazyload_placeholder'
            }).then(function () {
              return $ocLazyLoad.load([
                'modules/attendance/clients/clients.component.js',
                'modules/attendance/clients/client-manager-modal.controller.js',
                'modules/attendance/clients/client-remove-modal.controller.js',
                'modules/attendance/sessions/ticket-details-modal.controller.js'
              ]);
            });
        }]
      }
    });
  }
} ());
