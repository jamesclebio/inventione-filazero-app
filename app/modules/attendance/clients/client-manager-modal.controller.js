; (function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('ClientManagerModalController', ClientManagerModalController);

  ClientManagerModalController.$inject = [
    '$uibModalInstance',
    'ClientsAttendanceService',
    '$uibModal',
    'resolve',
    'ResponseCatcherService'
  ];

  function ClientManagerModalController($uibModalInstance, ClientsAttendanceService, $uibModal, resolve, ResponseCatcherService) {
    var vm = this;

    vm.isLoading = false;
    vm.client = {};
    vm.currentPage = 1;
    vm.filteredTickets = [];
    vm.itensLimit = 6;
    vm.searchTicket = "";
    vm.order = 'lastAction';

    vm.cancel = cancel;
    vm.save = save;
    vm.showTicketDetails = showTicketDetails;

    // Initialization
    function init() {
      if (resolve.clientId) {
        vm.providerId = resolve.providerId;
        getClient(resolve.clientId).then(
          function (client) {
            vm.client = client;
          }
        )
      }
    }

    init();
    // /Initialization

    function getClient(clientId) {
      vm.isLoading = true;
      return ClientsAttendanceService.getClient(clientId).then(
        function (client) {
          return client;
        },
        function (error) {
          ResponseCatcherService.catchError(error);
          $uibModalInstance.dismiss();
        }).finally(
        function () {
          vm.isLoading = false;
        });
    }

    function save(client) {
      vm.isLoading = true;
      if (resolve.clientId) {
        ClientsAttendanceService.updateClient(client).then(
          function (response) {
            ResponseCatcherService.catchSuccess(response).then(
              function (messages) {
                $uibModalInstance.close();
              }
            );
          },
          function (error) {
            ResponseCatcherService.catchError(error);
          }).finally(
          function () {
            vm.isLoading = false;
          });
      } else {
        var addClientObject = {
          client: client,
          providerId: resolve.providerId
        };
        ClientsAttendanceService.addClient(addClientObject).then(
          function (response) {
            ResponseCatcherService.catchSuccess(response).then(
              function (messages) {
                $uibModalInstance.close();
              }
            );
          },
          function (error) {
            ResponseCatcherService.catchError(error);
          }).finally(
          function () {
            vm.isLoading = false;
          });
      }
    }

    function showTicketDetails(ticketId) {
      var modal = $uibModal.open({
        templateUrl: 'modules/attendance/sessions/ticket-details-modal.html',
        controller: 'TicketDetailsModalController',
        controllerAs: 'vm',
        windowClass: 'stick-up',
        resolve: {
          resolve: function () {
            return {
              ticketId: ticketId
            };
          }
        }
      });
    }

    function cancel() {
      $uibModalInstance.dismiss();
    }
  }
})();
