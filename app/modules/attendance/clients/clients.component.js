(function() {
'use strict';

  angular
    .module('filazero-app')
    .component('clients', {
      templateUrl: 'modules/attendance/clients/clients.html',
      controllerAs: 'vm',
      controller: ClientsComponent,
      bindings: {
        currentProvider: '='
      },
    });

  ClientsComponent.$inject = [
    '$scope',
    'ClientsAttendanceService',
    '$stateParams',
    '$uibModal',
    'ResponseCatcherService',
  ];


  function ClientsComponent($scope, ClientsAttendanceService, $stateParams, $uibModal, ResponseCatcherService) {
    var vm = this;

    vm.currentPage = 1;
    vm.filteredClients = [];
    vm.clientsList = [];
    vm.isLoading = true;
    vm.itensLimit = 10;
    vm.searchClients = "";
    vm.order = 'name';

    vm.getClients = getClients;
    vm.addClient = addClient;
    vm.editClient = editClient;
    vm.deleteClient = deleteClient;
    vm.openModal = openModal;
    vm.setOrder = setOrder;

    vm.$onInit = onInit;
    vm.$onChanges = onChanges;
    vm.$onDestory = onDestory;

    // Lifecycle hooks
    function onInit() {
      vm.isLoading = true;
      vm.getClients();
    };

    function onChanges(changesObj) { };

    function onDestory() { };
    // Lifecycle hooks

    // Clients
    function getClients() {
      vm.isLoading = true;
      ClientsAttendanceService.getClients().then(
        function (clients) {
          vm.clientsList = clients;
          vm.isLoading = false;
        },
        function (error) {
          ResponseCatcherService.catchError(error, true);
        });
    }

    function addClient() {
      var templateUrl = 'modules/attendance/clients/client-add-modal.html';
      var controller = 'ClientManagerModalController';
      var addClientObject = {
        providerId: vm.currentProvider.id
      };
      var size = 'md';
      var windowClass = 'stick-up';

      vm.openModal(templateUrl, controller, addClientObject, size, windowClass);
    }

    function editClient(clientId) {
      var templateUrl = 'modules/attendance/clients/client-edit-modal.html';
      var controller = 'ClientManagerModalController';
      var editClientObject = {
        clientId: clientId,
        providerId: vm.currentProvider.id
      };
      var size = 'lg';
      var windowSize = 'stick-up';

      vm.openModal(templateUrl, controller, editClientObject, size, windowSize);
    }

    function deleteClient(client) {
      var templateUrl = 'modules/attendance/clients/client-remove-modal.html';
      var controller = 'ClientRemoveModalController';
      var deleteClientObject = {
        client: client
      };
      var size = 'md';
      var windowClass = 'stick-up';

      vm.openModal(templateUrl, controller, deleteClientObject, size, windowClass);
    }
    // /Clients

    function openModal(templateUrl, controller, resolve, size, windowClass) {
      var modalInstance = $uibModal.open({
        templateUrl: templateUrl,
        controller: controller,
        controllerAs: 'vm',
        size: size,
        windowClass: windowClass,
        resolve: {
          resolve: function () {
            return resolve;
          }
        }
      });

      modalInstance.result.then(
        function () {
          vm.getClients();
        }
      );
    }

    function setOrder(order) {
      if (order === vm.order) {
        vm.order = '-' + order;
      } else {
        vm.order = order;
      }
    }
  }
})();
