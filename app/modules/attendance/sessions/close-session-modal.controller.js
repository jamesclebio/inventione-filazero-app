;(function(){
  'use strict';

  angular
    .module('filazero-app')
    .controller('CloseSessionModalController', CloseSessionModalController);

  CloseSessionModalController.$inject = ['$uibModalInstance', 'SessionsAttendanceService', 'resolve', 'ResponseCatcherService'];

  function CloseSessionModalController($uibModalInstance, SessionsAttendanceService, resolve, ResponseCatcherService){

    var vm = this;

    vm.session = resolve.session;
    vm.isLoading = false;

    vm.cancel = cancel;
    vm.confirm = confirm;

    function confirm(){
      vm.isLoading = true;
      SessionsAttendanceService.closeSession(vm.session.id).then(
        function (data) {
          ResponseCatcherService.catchSuccess(data).then(
            function(){
              $uibModalInstance.close();
            }
          )
        },
        function(error){
          ResponseCatcherService.catchError(error);
        }
      );
    }

    function cancel(){
      $uibModalInstance.dismiss('cancel');
    }
  }
})();
