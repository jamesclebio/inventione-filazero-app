(function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('BasicAttendanceController', BasicAttendanceController);

  BasicAttendanceController.$inject = [
    '$stateParams',
    'LocationsManagerService',
    'LocationsAdminService',
    'ResponseCatcherService',
    'SessionsAttendanceService',
    'ProviderService',
    'AuthService',
    '$state',
    '$location'
  ];

  function BasicAttendanceController($stateParams, LocationsManagerService, LocationsAdminService, ResponseCatcherService, SessionsAttendanceService, ProviderService, AuthService, $state, $location) {
    var vm = this;

    var locationId = $stateParams.locationId;

    vm.provider = ProviderService.getCurrentProvider();

    vm.logout = logout

    init();

    ////////////////

    function init() {
      getLocation(locationId);
      getSessions(locationId);
    }

    function getSessions(locationId) {
      vm.isLoadingSessions = true;
      SessionsAttendanceService.getSessions(locationId, new Date().toJSON()).then(
        function (sessions) {
          vm.sessions = sessions;
          if (sessions.length > 0) {
            vm.hasSessions = true;
          } else {
            vm.hasSessions = false;
          }
          for (var i = 0; i < vm.sessions.length; i++) {
            vm.sessions[i].servicesName = concatServicesName(vm.sessions[i].services);
          }
        },
        function (error) {
          ResponseCatcherService.catchError(error, true);
        }
      ).finally(function () {
        vm.isLoadingSessions = false;
      });
    }

    function concatServicesName(services) {
      var servicesName = '';
      for (var i = 0; i < services.length; i++) {
        if (i > 0) {
          servicesName += ", " + services[i].name
        } else {
          servicesName = services[i].name;
        }
      }
      return servicesName;
    }

    function getLocation(locationId) {
      LocationsAdminService.getLocation(locationId).then(
        function (location) {
          vm.location = location;
        },
        function (error) {
          ResponseCatcherService.catchError(error, true);
        }
      );
    }

    function logout() {
      AuthService.logOut();
      var redirectTo = encodeURIComponent($location.url());
      $state.go('login', { 'redirectTo': redirectTo });
    }
  }
})();
