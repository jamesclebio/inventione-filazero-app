;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('SessionBasicAttendanceController', SessionBasicAttendanceController);

  SessionBasicAttendanceController.$inject = ['SessionsAttendanceService', '$stateParams', 'Pusher', '$uibModal', 'PusherService', 'ResponseCatcherService', '$q', 'AuthService', '$state', '$location'];

  function SessionBasicAttendanceController(SessionsAttendanceService, $stateParams, Pusher, $uibModal, PusherService, ResponseCatcherService, $q, AuthService, $state, $location) {
    var vm = this;

    vm.isLoading = false;
    vm.session = {};
    vm.ticket = {};
    vm.providerSlug = $stateParams.providerSlug;
    vm.locationId = $stateParams.locationId;
    vm.isListShown = false;

    vm.selectResource = selectResource;
    vm.callNext = callNext;
    vm.hasAction = hasAction;
    vm.showTicketDetails = showTicketDetails;
    vm.ticketAction = ticketAction;
    vm.changeListVisibility = changeListVisibility;
    vm.logout = logout;

    init();

    function init() {
      getSession($stateParams.s);
      getTicketsList($stateParams.s);
    }

    function getSession(sessionId) {
      vm.isLoading = true;
      SessionsAttendanceService.getBasicSession(sessionId).then(
        function (response) {
          vm.session = response.session;
          vm.selectedResource = vm.session.resources[0];
          vm.session.servicesName = concatServicesName(vm.session.services);
          vm.ticket = response.ticket;
          pusherSubscribe();
        },
        function (error) {
          ResponseCatcherService.catchError(error);
        }
      ).finally(function () {
        vm.isLoading = false;
      })
    }

    function getTicketsList(sessionId){
      SessionsAttendanceService.getSession(sessionId).then(
        function (tickets) {
          vm.tickets = [];
          for(var i = 0; i < tickets.length; i++){
            if(tickets[i].status.toLowerCase() === 'checked'){
              vm.tickets.push(tickets[i]);
            }
          }

        },
        function (error) {
          ResponseCatcherService.catchError(error, true);
        }
      );
    }

    function callNext() {
      if (vm.ticket && vm.ticket.id) {
        if (hasAction('complete')) {
          ticketAction('complete').then(function () {
            performCallNext();
          });
          return;
        } else if (hasAction('reopen')) {
          ticketAction('reopen').then(function () {
            performCallNext();
          });
          return;
        }
      }
      performCallNext();
    }

    function performCallNext() {
      var callNextObject = {
        sessionId: $stateParams.s,
        resourceId: vm.selectedResource.id,
        callSpecial: vm.isSpecial
      };
      SessionsAttendanceService.callNext(callNextObject).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response).then(
            function () {
              vm.ticket = response.responseData;
            }
          )
        },
        function (error) {
          ResponseCatcherService.catchError(error);
        }
      );
    }

    function ticketAction(action) {
      var deferred = $q.defer();

      if (vm.ticket && vm.ticket.id) {
        if (vm.ticket.actions.indexOf(action) === -1) {
          //notificar usuário
        } else {
          var data = {resourceId: vm.selectedResource.id};
          SessionsAttendanceService.actionOnTicket(vm.ticket.id, action, data).then(
            function (response) {
              ResponseCatcherService.catchSuccess(response).then(
                function () {
                  getSession($stateParams.s);
                  deferred.resolve();
                }, function (error) {
                  ResponseCatcherService.catchError(error);
                  deferred.resolve();
                }
              )
            }, function (error) {
              ResponseCatcherService.catchError(error);
              deferred.resolve();
            }
          )
        }
      } else {
        //notificar
      }

      return deferred.promise;
    }

    function countTicket() {
      SessionsAttendanceService.countTicket($stateParams.s).then(
        function (response) {
          vm.session.noTicketsToCall = response.waitingAttendanceTicketCount;
        }
      )
    }

    function selectResource(resource) {
      vm.selectedResource = resource;
    }

    function concatServicesName(services) {
      var servicesName = '';
      for (var i = 0; i < services.length; i++) {
        if (i > 0) {
          servicesName += ", " + services[i].name
        } else {
          servicesName = services[i].name;
        }
      }
      return servicesName;
    }

    function hasAction(action) {
      if (vm.ticket && vm.ticket.id) {
        return vm.ticket.actions.indexOf(action) !== -1;
      } else {
        return false;
      }
    }

    function pusherSubscribe() {
      Pusher.subscribe('session-' + $stateParams.s, 'TicketCheckin', function (data) {
        countTicket();
        getTicketsList($stateParams.s);
      });
      Pusher.subscribe('session-' + $stateParams.s, 'TicketCancelled', function (data) {
        countTicket();
        getTicketsList($stateParams.s);
      });
      Pusher.subscribe('session-' + $stateParams.s, 'TicketCalled', function (data) {
        countTicket();
        getTicketsList($stateParams.s);
      });

      PusherService.then(function (pusher) {
        var state = pusher.connection.state;
        if (state === 'failed') {
          init();
        }
      });
    }

    function showTicketDetails(ticketId) {
      $uibModal.open({
        templateUrl: 'modules/attendance/sessions/ticket-details-modal.html',
        controller: 'TicketDetailsModalController',
        windowClass: 'stick-up',
        controllerAs: 'vm',
        resolve: {
          resolve: function () {
            return {
              ticketId: ticketId
            };
          }
        }
      });
    }

    function changeListVisibility() {
      vm.isListShown = !vm.isListShown;
    }

    function logout() {
      AuthService.logOut();
      var redirectTo = encodeURIComponent($location.url());
      $state.go('login', { 'redirectTo': redirectTo});
    }
  }
})();
