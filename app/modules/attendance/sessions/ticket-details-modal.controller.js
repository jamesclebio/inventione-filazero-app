; (function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('TicketDetailsModalController', TicketDetailsModalController);

  TicketDetailsModalController.$inject = ['TicketsAttendanceService', '$uibModalInstance', 'resolve', 'ResponseCatcherService', '$window'];

  function TicketDetailsModalController(TicketsAttendanceService, $uibModalInstance, resolve, ResponseCatcherService, $window) {

    var vm = this;
    vm.isLoadingTicket = true;

    vm.ticket = {};

    vm.canCancelTicket = canCancelTicket;
    vm.canCheckTicket = canCheckTicket;
    vm.canConfirmTicket = canConfirmTicket;
    vm.cancelTicket = cancelTicket;
    vm.checkTicket = checkTicket;
    vm.confirmTicket = confirmTicket;
    vm.close = close;
    vm.printTicket = printTicket;
    vm.changePreference = changePreference;
    vm.isTicketActive = isTicketActive;

    init();

    function canCancelTicket(ticket) {
      return canDoAction(ticket, 'cancel');
    }

    function canCheckTicket(ticket) {
      return canDoAction(ticket, 'checkin');
    }

    function canConfirmTicket(ticket) {
      return canDoAction(ticket, 'confirm');
    }

    function canDoAction(ticket, action) {
      return ticket.actions && ticket.actions.indexOf(action) !== -1;
    }

    function cancelTicket(ticket) {
      ticketAction(ticket, 'cancel');
    }

    function checkTicket(ticket) {
      ticketAction(ticket, 'check');
    }

    function confirmTicket(ticket) {
      ticketAction(ticket, 'confirm');
    }

    function close() {
      $uibModalInstance.close();
    }

    function printTicket() {
      $window.print();
    }

    function init() {
      getTicket(resolve.ticketId);
    }

    function getTicket(ticketId) {
      vm.isLoadingTicket = true;
      TicketsAttendanceService.getTicket(ticketId).then(
        function (ticket) {
          vm.ticket = ticket;
          vm.isLoadingTicket = false;
        },
        function (error) {
          ResponseCatcherService.catchError(error);
          $uibModalInstance.close();
        });
    }

    function ticketAction(ticket, action) {
      vm.isLoadingTicket = true;
      TicketsAttendanceService.actionOnTicket(ticket.id, action).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response).then(
            function (messages) {
              getTicket(resolve.ticketId);
            }
          )
        },
        function (error) {
          ResponseCatcherService.catchError(error);
          $uibModalInstance.close();
        }
      )
    }

    function isTicketActive(ticket) {
      var status = ticket.status.toLowerCase();
      return status !== 'started' && status !== 'completed' && status !== 'canceled';
    }

    function changePreference(){
      var data = {priority: vm.ticket.isSpecial};
      TicketsAttendanceService.changePreference(vm.ticket.id, data).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response, false, false, false);
        },
        function (error) {
          ResponseCatcherService.catchError(error);
        }
      )
    }
  }
})();
