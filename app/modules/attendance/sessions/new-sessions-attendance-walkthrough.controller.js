; (function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('NewSessionsAttendanceWalkthroughController', NewSessionsAttendanceWalkthroughController);

  NewSessionsAttendanceWalkthroughController.$inject = [
    '$state',
    'ProviderService',
    'WizardHandler',
    'localStorageService'
  ];

  function NewSessionsAttendanceWalkthroughController($state, ProviderService, WizardHandler, localStorageService) {
    var vm = this;
    var provider;

    vm.showPreviousButton = showPreviousButton;
    vm.showNextButton = showNextButton;
    vm.goToNextPage = goToNextPage;
    vm.goToPreviousPage = goToPreviousPage;
    vm.finish = finish;

    function initWalkthrough() {
      vm.steps = [
        {
          imageURL: 'assets/images/screenshots/new-attendance/service-configs-list.png',
          title: 'Indicadores de atendimento',
          content: 'Você perceberá logo quando alguma fila de atendimento está com atraso ou perto de atrasar, assim fica mais fácil decidir o que fazer para não chatear os clientes.'
        },
        {
          imageURL: 'assets/images/screenshots/new-attendance/checked-tickets-list.png',
          title: 'Previsão de atendimento',
          content: 'Não precisa se preocupar tanto com a posição dos clientes na fila, ele agora pode saber o horário previsto para seu atendimento. Compartilhe com seus clientes essa informação e incentive-o a acompanhar sua previsão de atendimento pelo celular.'
        },
        {
          imageURL: 'assets/images/screenshots/new-attendance/ticket-details.png',
          title: 'Detalhes do atendimento',
          content: 'Todas as informações do atendimento e do cliente estão disponíveis de maneira fácil.'
        },
        {
          imageURL: 'assets/images/screenshots/new-attendance/call-next.png',
          title: 'Atender clientes',
          content: 'Chame seus clientes no painel de atendimento, inicie e conclua o atendimento facilmente.'
        }
      ];

      provider = ProviderService.getCurrentProvider();
    }

    initWalkthrough();

    // Wizard
    function showNextButton() {
      return WizardHandler.wizard().currentStepNumber() !== 4;

    }

    function showPreviousButton() {
      return WizardHandler.wizard().currentStepNumber() !== 1;
    }

    function goToNextPage() {
      WizardHandler.wizard().goTo(WizardHandler.wizard().currentStepNumber());
    }

    function goToPreviousPage() {
      WizardHandler.wizard().goTo(WizardHandler.wizard().currentStepNumber() - 2);
    }

    function finish() {
      localStorageService.set('default-attendance-route', 'new');
      $state.go('app.provider.new-attendance', {providerSlug: provider.slug});
    }
    // /Wizard
  }
})();