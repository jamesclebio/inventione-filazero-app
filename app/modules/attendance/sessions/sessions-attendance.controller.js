﻿;
(function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('SessionsAttendanceController', SessionsAttendanceController);

  SessionsAttendanceController.$inject = [
    '$scope',
    'orderByFilter',
    'ProviderService',
    'SessionsAttendanceService',
    'LocationsManagerService',
    '$uibModal',
    '$state',
    'Pusher',
    'PusherService',
    'ResponseCatcherService',
    '$timeout',
    '$q',
    'ServiceConfigService',
    'localStorageService'
  ];

  function SessionsAttendanceController($scope, orderByFilter, ProviderService, SessionsAttendanceService, LocationsManagerService, $uibModal, $state, Pusher, PusherService, ResponseCatcherService, $timeout, $q, ServiceConfigService, localStorageService) {
    var location = LocationsManagerService.getCurrentLocation();
    $scope.hasSessions = false;
    $scope.closedSessionFilter = false;
    $scope.inProgressSessionFilter = true;
    $scope.notStartedSessionFilter = false;
    $scope.shownSessions = 0;
    $scope.searchTickets = '';
    $scope.serviceConfig = {};
    $scope.filteredServiceConfigs = [];
    $scope.provider = ProviderService.getCurrentProvider();

    var init = function () {
      location = LocationsManagerService.getCurrentLocation();

      if (location) {
        $scope.getServiceConfigs(location.id).then(
          function () {
            $scope.getSessions(location.id).then(
              function () {
                var i = 0;
                for (var j = 0; j < $scope.sessions.length; j++) {
                  for (i = 0; i < $scope.serviceConfigs.length; i++) {
                    if ($scope.serviceConfigs[i].id == $scope.sessions[j].serviceConfigId) {
                      $scope.serviceConfigs[i].sessions.push($scope.sessions[j]);
                    }
                  }
                }
                $scope.updateServiceConfigStatus();
                $scope.selectedServiceConfig = orderByFilter($scope.serviceConfigs, [$scope.serviceConfigOrder, 'name'])[0];
              }
            );
          }
        );
      }
    };

    $scope.$on('appLocationChanged', function () {
      init();
    });

    /*START GET METHODS*/
    $scope.getServiceConfigs = function (locationId) {
      var deferred = $q.defer();

      $scope.isLoadingAll = true;

      ServiceConfigService.getServiceConfigs(locationId).then(
        function (serviceConfigs) {
          $scope.serviceConfigs = serviceConfigs;

          deferred.resolve(serviceConfigs);
        }, function (error) {
          ResponseCatcherService.catchError(error, true);
          deferred.reject(error);
        }
      );

      return deferred.promise;
    };

    $scope.selectServiceConfig = function (serviceConfig) {
      $scope.selectedServiceConfig = serviceConfig;
    };

    $scope.serviceConfigOrder = function (serviceConfig) {
      if (serviceConfig.status === 'isLate') {
        return 0;
      }
      if (serviceConfig.status === 'onWarning') {
        return 1;
      }
      if (serviceConfig.status === 'onTime') {
        return 2;
      }
      if (serviceConfig.status === 'noTickets') {
        return 3;
      } else {
        return 4;
      }
    };

    $scope.getSessions = function (locationId) {
      var deferred = $q.defer();

      $scope.isLoadingAll = true;
      SessionsAttendanceService.getSessions(locationId, new Date().toJSON()).then(
        function (sessions) {
          $scope.sessions = sessions;

          for (var i = 0; i < $scope.serviceConfigs.length; i++) {
            $scope.serviceConfigs[i].sessions = [];
          }

          if (sessions.length > 0) {
            $scope.hasSessions = true;
            for (i = 0; i < sessions.length; i++) {
              sessionInitialization(sessions[i]);
              pusherSubscriber(sessions[i]);
            }
          } else {
            $scope.hasSessions = false;
            for (i = 0; i < $scope.serviceConfigs.length; i++) {
              $scope.serviceConfigs[i].status = 'noSessions';
            }
          }
          deferred.resolve(sessions);
        },
        function (error) {
          ResponseCatcherService.catchError(error, true);
          deferred.reject(error);
        }
      ).finally(function () {
        $scope.isLoadingAll = false;
      });

      return deferred.promise;
    };

    $scope.getSession = function (session, ticketId) {
      var found = false;
      if (ticketId) {
        for (var j = 0; j < session.tickets.length; j++) {
          if (ticketId === session.tickets[j].id) {
            session.tickets[j].isLoading = true;
            found = true;
            break;
          }
        }
      }
      SessionsAttendanceService.getSession(session.id).then(
        function (refreshedSessionTickets) {
          session.tickets = refreshedSessionTickets;
          sessionInitialization(session);
          if (found && ticketId) {
            session.tickets[j].isLoading = false;
          }
          for (var i = 0; i < $scope.serviceConfigs.length; i++) {
            $scope.serviceConfigPrevisionStatus($scope.serviceConfigs[i]);
          }
        },
        function (error) {
          ResponseCatcherService.catchError(error, true);
        }
      );
    };

    $scope.getSessionData = function (session) {
      SessionsAttendanceService.getSessionData(session.id).then(
        function (refreshedSessionData) {
          session.startDate = new Date(refreshedSessionData.estimatedStart);
          session.endDate = new Date(refreshedSessionData.estimatedEnd);
          session.ticketsNum = refreshedSessionData.emittedTicket;
          session.capacity = refreshedSessionData.capacity;
          sessionInitialization(session);
        },
        function (error) {
          ResponseCatcherService.catchError(error, true);
        }
      )
    };

    $scope.getTicket = function (ticketId, session) {
      var found = false;
      for (var j = 0; j < session.tickets.length; j++) {
        if (ticketId === session.tickets[j].id) {
          session.tickets[j].isLoading = true;
          found = true;
          break;
        }
      }
      SessionsAttendanceService.getResumedTicket(ticketId).then(
        function (refreshedTicket) {
          if (found) {
            session.tickets[j] = refreshedTicket;
            session.tickets[j].isLoading = false;
          }
          sessionInitialization(session);
        },
        function (error) {
          ResponseCatcherService.catchError(error, true);
        }
      );
    };
    /*END GET METHODS*/

    /*START POST METHODS*/
    $scope.canCallTicket = function (ticket) {
      return canDoAction(ticket, 'call');
    };

    $scope.canReopenTicket = function (ticket) {
      return canDoAction(ticket, 'reopen');
    };

    $scope.canStartTicket = function (ticket) {
      return canDoAction(ticket, 'start');
    };

    $scope.canCompleteTicket = function (ticket) {
      return canDoAction(ticket, 'complete');
    };

    $scope.canCancelTicket = function (ticket) {
      return canDoAction(ticket, 'cancel');
    };

    $scope.canConfirmTicket = function (ticket) {
      return canDoAction(ticket, 'confirm');
    };

    $scope.canCheckTicket = function (ticket) {
      return canDoAction(ticket, 'checkin');
    };

    function canDoAction(ticket, action) {
      for (var i = 0; i < ticket.actions.length; i++) {
        if (action === ticket.actions[i]) {
          return true;
        }
      }
      return false;
    }

    $scope.action = function (session, ticket, action, data) {
      var deferred = $q.defer();

      SessionsAttendanceService.actionOnTicket(ticket.id, action, data).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response);
          deferred.resolve(response);
        },
        function (error) {
          ResponseCatcherService.catchError(error);
          deferred.reject(error);
        }
      ).finally(function () {
        ticket.isLoading = false;
      });

      return deferred.promise;
    };

    $scope.actionOnTicket = function (session, ticket, action) {
      ticket.isLoading = true;
      var data = {};
      if (action === 'call' || action === 'start') {
        if (session.isShared) {
          if (session.selectedResource !== null) {
            data.resourceId = session.selectedResource.id;
            $scope.action(session, ticket, action, data);
          } else {
            var templateUrl = 'modules/attendance/sessions/call-next-attendance-modal.html';
            var controller = 'CallNextAttendanceController';
            var resolve = {
              session: session,
              ticket: ticket
            };
            openModal(templateUrl, controller, resolve).then(
              function (response) {
                if (response) {
                  data.resourceId = response;
                  $scope.action(session, ticket, action, data);
                }
              }).finally(function () {
                ticket.isLoading = false;
              });
          }
        } else {
          data.resourceId = session.resourcesRooms[0].resource.id;
          $scope.action(session, ticket, action, data);
        }
      } else {
        $scope.action(session, ticket, action, data);
      }
    };

    function callNextService(callNextObject, session) {
      var deferred = $q.defer();

      SessionsAttendanceService.callNext(callNextObject).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response, true, true, false);
          if (response.messages[0].code === "3003") {
            afterCallNext(response, session);
          }
          deferred.resolve(response);
        }, function (error) {
          ResponseCatcherService.catchError(error);
          deferred.reject(error);
        });

      return deferred.promise;
    }

    $scope.callNext = function (session) {
      var callNextObject = {
        sessionId: session.id,
        callSpecial: false
      };
      if (session.isShared) {
        if (session.selectedResource !== null) {
          callNextObject.resourceId = session.selectedResource.id;
          callNextService(callNextObject, session).then(
            function (response) {
              ResponseCatcherService.catchSuccess(response);
            },
            function (error) {
              ResponseCatcherService.catchError(error);
            }
          );
        } else {
          var templateUrl = 'modules/attendance/sessions/call-next-attendance-modal.html';
          var controller = 'CallNextAttendanceController';
          var resolve = {
            session: session,
            ticket: {}
          };
          openModal(templateUrl, controller, resolve).then(
            function (response) {
              if (response) {
                callNextObject.resourceId = response;
                callNextService(callNextObject, session).then(
                  function (response) {
                    ResponseCatcherService.catchSuccess(response);
                  },
                  function (error) {
                    ResponseCatcherService.catchError(error);
                  }
                );
              }
            });
        }
      } else {
        callNextObject.resourceId = session.resourcesRooms[0].resource.id;
        callNextService(callNextObject, session);
      }

    };

    function afterCallNext(response, session) {
      var ticketId = response.messages[0].parameters.selectedServiceId;
      var ticket = {};
      for (var i = 0; i < session.tickets.length; i++) {
        if (session.tickets[i].id === ticketId) {
          ticket = session.tickets[i];
          break;
        }
      }
      $scope.action(session, ticket, 'reopen').then(
        function () {
          $scope.callNext(session);
        }
      );
    }

    /*END POST METHODS*/

    /*START MODAL METHODS*/
    $scope.showTicketDetails = function (ticketId) {
      var templateUrl = 'modules/attendance/sessions/ticket-details-modal.html';
      var controller = 'TicketDetailsModalController';
      var resolve = {
        ticketId: ticketId
      };
      openModal(templateUrl, controller, resolve);
    };

    $scope.openEmitTicket = function (session) {
      var templateUrl = 'modules/attendance/schedule/emit-ticket-modal.html';
      var controller = 'EmitTicketModalController';
      var resolve = {
        session: session,
        providerId: $scope.provider.id
      };
      openModal(templateUrl, controller, resolve);
    };

    var openModal = function (templateUrl, controller, resolve) {
      var modal = $uibModal.open({
        templateUrl: templateUrl,
        controller: controller,
        controllerAs: 'vm',
        windowClass: 'stick-up',
        resolve: {
          resolve: function () {
            return resolve;
          }
        }
      });
      return modal.result;
    };
    /*END MODAL METHODS*/

    var sessionInitialization = function (session) {
      if (!session.isAllShown) {
        session.isAllShown = false;
        session.ticketsLimit = 5;
        session.checkedTicketsFilter = true;
        session.confirmedTicketsFilter = true;
        session.scheduledTicketsFilter = true;
        session.completedTicketsFilter = false;
        session.cancelledTicketsFilter = false;

        session.startDate = new Date(session.startDate);
        session.endDate = new Date(session.endDate);
      }

      session.waitingServiceTickets = 0;
      session.calledTickets = 0;
      for (var i = 0; i < session.tickets.length; i++) {
        if ($scope.waitingForService(session.tickets[i])) {
          session.waitingServiceTickets++;
        }
        if ($scope.hasCalled(session.tickets[i])) {
          session.calledTickets++;
        }
      }
      session.servicesName = concatServicesName(session.services);

      session.next = getNext(session);

      if (session.isShared && !session.selectedResource) {
        session.selectedResource = null;
      }
    };

    function concatServicesName(services) {
      var servicesName = '';
      for (var i = 0; i < services.length; i++) {
        if (i > 0) {
          servicesName += ", " + services[i].name
        } else {
          servicesName = services[i].name;
        }
      }
      return servicesName;
    }

    /* START SHOW/HIDE */
    $scope.hasAction = function (ticket, action) {
      for (var i = 0; i < ticket.actions.length; i++) {
        if (ticket.actions[i].name == action) {
          return true;
        }
      }
      return false;
    };

    $scope.hasCalled = function (ticket) {
      return ticket.status.toLowerCase() === "called";
    };

    $scope.hasPrevision = function (ticket) {
      return ticket.prevision != null;
    };

    $scope.hasStarted = function (ticket) {
      return ticket.status.toLowerCase() === "started";
    };

    $scope.hasTickets = function (session) {
      return session.tickets.length > 0;
    };

    $scope.isFinalized = function (ticket) {
      return ticket.status.toLowerCase() === "completed" || ticket.status.toLowerCase() === "canceled";
    };

    $scope.isInAttendance = function (ticket) {
      return ticket.status.toLowerCase() === "started" || ticket.status.toLowerCase() === "called";
    };

    $scope.waitingForService = function (ticket) {
      return ticket.status.toLowerCase() === "checked" || ticket.status.toLowerCase() === "called";
    };

    function getNext(session) {
      for (var i = 0; i < session.tickets.length; i++) {
        if (!$scope.isInAttendance(session.tickets[i]) && !$scope.isFinalized(session.tickets[i]) && session.tickets[i].checked) {
          session.next = session.tickets[i];
          return session.tickets[i];
        }
      }
    }

    $scope.isNext = function (session, ticket) {
      return session.next && session.next.id === ticket.id;
    };

    $scope.sessionsFound = function () {
      return $scope.filteredSessions === 0 || $scope.shownSessions === 0;
    };

    $scope.showCallAction = function (ticket) {
      return ticket.actions.indexOf('confirm') === -1 && ticket.actions.indexOf('checkin') === -1 && ticket.actions.indexOf('reopen') === -1;
    };

    /* END SHOW/HIDE */

    /* START FILTERS */

    $scope.changedSessionFilter = function () {
      if (!$scope.closedSessionFilter && !$scope.inProgressSessionFilter && !$scope.notStartedSessionFilter) {
        $scope.closedSessionFilter = true;
        $scope.inProgressSessionFilter = true;
        $scope.notStartedSessionFilter = true;
      }
    };

    $scope.showSession = function () {
      return function (session) {
        if ($scope.selectedServiceConfig && session.serviceConfigId === $scope.selectedServiceConfig.id) {
          var now = new Date();
          if (session.endDate < now && $scope.closedSessionFilter) {
            for (var i = 0; i < session.tickets.length; i++) {
              if (session.tickets[i].status == "opened" && (session.tickets[i].confirmed || session.tickets[i].checked)) {
                if (!$scope.inProgressSessionFilter) {
                  return false;
                }
              }
            }
            return true;
          } else if (session.startDate < now && $scope.inProgressSessionFilter) {
            if (session.endDate > now) {
              return true;
            }
            for (var j = 0; j < session.tickets.length; j++) {
              if (!session.tickets[j].status != "completed" && !session.tickets[j].status != "canceled" && session.tickets[j].confirmed) {
                $scope.shownSessions++;
                return true
              }
            }
            return false;
          } else {
            return session.startDate > now && $scope.notStartedSessionFilter;
          }
        } else {
          return false;
        }
      }
    };

    $scope.showTickets = function (session) {
      return function (ticket) {
        if (ticket.checked && session.checkedTicketsFilter && !$scope.isFinalized(ticket)) {
          return true;
        } else if (ticket.confirmed && session.confirmedTicketsFilter && !ticket.checked && !$scope.isFinalized(ticket)) {
          return true;
        } else if (!ticket.confirmed && !ticket.checked && session.scheduledTicketsFilter && !$scope.isFinalized(ticket)) {
          return true;
        } else if (session.completedTicketsFilter && ticket.status.toLowerCase() === "completed") {
          return true;
        } else {
          return session.cancelledTicketsFilter && ticket.status.toLowerCase() === "canceled";
        }
      };
    };
    /* END FILTERS */

    // Session actions
    $scope.goToSession = function (session) {
      var url = $state.href('session-basic-attendance', {
        providerSlug: $scope.provider.slug,
        locationId: location.id,
        s: session.id
      });

      window.open(url, '_blank', 'width=570, height=665, top=0, left=0, resizable=0, scrollbars=0, location=0, status=0');
    };

    $scope.selectResource = function (session, resource) {
      if (resource) {
        session.selectedResource = resource;
      } else {
        session.selectedResource = null;
      }
    };

    $scope.showAll = function (session) {
      session.ticketsLimit = session.tickets.length;
      session.isAllShown = true;
      session.ticketsShown = [];
    };

    $scope.showLess = function (session) {
      session.ticketsShown = [];
      session.isAllShown = false;
      session.ticketsLimit = 5;
    };

    $scope.closeSession = function (session) {
      var templateUrl = 'modules/attendance/sessions/close-session-modal.html';
      var controller = 'CloseSessionModalController';
      var resolve = {
        session: session
      };
      openModal(templateUrl, controller, resolve).then(
        function () {
          $scope.getSessions(location.id);
        });
    };

    // Prevision checkers

    $scope.serviceConfigPrevisionStatus = function (serviceConfig) {
      var hasNext = false;
      serviceConfig.status = '';
      serviceConfig.waitingServiceTickets = 0;
      serviceConfig.calledTickets = 0;
      if (serviceConfig.sessions.length > 0 && $scope.sessions) {
        for (var i = 0; i < serviceConfig.sessions.length; i++) {
          serviceConfig.waitingServiceTickets += serviceConfig.sessions[i].waitingServiceTickets;
          serviceConfig.calledTickets += serviceConfig.sessions[i].calledTickets;
          if (serviceConfig.sessions[i].next) {
            hasNext = true;
            if (new Date(serviceConfig.sessions[i].next.prevision).getTime() >= new Date().getTime()) {
              if (serviceConfig.status === '') {
                serviceConfig.status = 'onTime';
              }
            } else if (new Date(serviceConfig.sessions[i].next.prevision).getTime() < new Date().getTime() && new Date().getTime() - new Date(serviceConfig.sessions[i].next.prevision).getTime() <= 900000 && !$scope.isLate(serviceConfig)) {
              serviceConfig.status = 'onWarning';
            } else {
              serviceConfig.status = 'isLate';
            }
          }
        }
        if (!hasNext) {
          serviceConfig.status = 'noTickets';
        }
      } else {
        serviceConfig.status = 'noSessions';
      }
    };

    $scope.updateServiceConfigStatus = function () {
      for (var i = 0; i < $scope.serviceConfigs.length; i++) {
        $scope.serviceConfigPrevisionStatus($scope.serviceConfigs[i]);
      }
      $timeout(function () { $scope.updateServiceConfigStatus() }, 30000);
    };

    $scope.isOnTime = function (serviceConfig) {
      return serviceConfig.status === 'onTime';
    };

    $scope.isWarning = function (serviceConfig) {
      return serviceConfig.status === 'onWarning';
    };

    $scope.isLate = function (serviceConfig) {
      return serviceConfig.status === 'isLate';
    };

    $scope.noTickets = function (serviceConfig) {
      return serviceConfig.status === 'noTickets';
    };

    $scope.noSessions = function (serviceConfig) {
      return serviceConfig.status === 'noSessions';
    };

    $scope.addServiceConfig = function () {
      var templateUrl = 'modules/service-config/service-config-add-modal.html';
      var controller = 'ServiceConfigAddModalController';

      openModal(templateUrl, controller, {}, 'md');
    };

    $scope.goToNewAttendanceRoute = function () {
      if (localStorageService.get('default-attendance-route')) {
        localStorageService.set('default-attendance-route', 'new');
      }
      $state.go('app.provider.new-attendance', { providerSlug: $scope.provider.slug })
    }

    // Pusher Configurations
    function pusherSubscriber(session) {
      Pusher.subscribe('session-' + session.id, 'TicketNew', function (data) {
        $scope.getSession(session, data.TicketId);
        $scope.getSessionData(session);
      });
      Pusher.subscribe('session-' + session.id, 'TicketStart', function (data) {
        $scope.getSession(session, data.TicketId);
      });
      Pusher.subscribe('session-' + session.id, 'TicketComplete', function (data) {
        $scope.getTicket(data.TicketId, session);
      });

      Pusher.subscribe('session-' + session.id, 'TicketReopened', function (data) {
        $scope.getSession(session, data.TicketId);
      });
      Pusher.subscribe('session-' + session.id, 'TicketConfirm', function (data) {
        $scope.getSession(session, data.TicketId);
      });
      Pusher.subscribe('session-' + session.id, 'TicketCheckin', function (data) {
        $scope.getSession(session, data.TicketId);
      });
      Pusher.subscribe('session-' + session.id, 'TicketCalled', function (data) {
        $scope.getSession(session, data.TicketId);
      });
      Pusher.subscribe('session-' + session.id, 'TicketCancel', function (data) {
        $scope.getSession(session, data.TicketId);
        $timeout(function () {
          $scope.getSession(session);
          $scope.getSessionData(session);
        }, 10000);
      });

      Pusher.subscribe('session-' + session.id, 'SessionChanged', function () {
        $scope.getSessionData(session)
      });

      PusherService.then(function (pusher) {
        var state = pusher.connection.state;
        if (state === 'failed') {
          init();
        }
      });
    }

    init();
  }
})();
