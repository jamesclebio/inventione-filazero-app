; (function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('NewSessionsAttendanceController', NewSessionsAttendanceController);

  NewSessionsAttendanceController.$inject = [
    '$scope',
    '$q',
    '$timeout',
    '$state',
    '$anchorScroll',
    '$location',
    'orderByFilter',
    'ResponseCatcherService',
    'NotificationsService',
    'ProviderService',
    'LocationsManagerService',
    'ServiceConfigService',
    'SessionsAttendanceService',
    'Pusher',
    '$uibModal',
    'TicketsAttendanceService',
    'localStorageService'
  ];

  function NewSessionsAttendanceController($scope, $q, $timeout, $state, $anchorScroll, $location, orderByFilter, ResponseCatcherService, NotificationsService, ProviderService, LocationsManagerService, ServiceConfigService, SessionsAttendanceService, Pusher, $uibModal, TicketsAttendanceService, localStorageService) {
    var vm = this;
    var location, updateServiceConfigTimeout;

    vm.statusLists = [false, false, false, false, false, false, false];
    vm.unseenTickets = [false, false, false, false, false, false, false];
    vm.selected = {};
    vm.specialTicketsCount = 0;
    vm.isCurrentResourcePinned = true;
    vm.hasChooseResouceError = false;
    vm.noResourceSelectedErrorMessage = 'É necessário selecionar um atendente para realizar uma chamada.';

    vm.setCurrentServiceConfig = setCurrentServiceConfig;
    vm.isCurrentServiceConfig = isCurrentServiceConfig;
    vm.hasServiceConfigMoreThanOneSession = hasServiceConfigMoreThanOneSession;
    vm.isServiceConfigsListCollapsed = isServiceConfigsListCollapsed;
    vm.collapseServiceConfigsList = collapseServiceConfigsList;

    vm.setCurrentSession = setCurrentSession;
    vm.isCurrentSession = isCurrentSession;
    vm.hasCurrentSession = hasCurrentSession;

    vm.setCurrentResource = setCurrentResource;
    vm.hasCurrentResource = hasCurrentResource;

    vm.initStatusLists = initStatusLists;
    vm.changeStatusListDetailsState = changeStatusListDetailsState;
    vm.getStatusListDetailsState = getStatusListDetailsState;
    vm.getUnseenTicketsState = getUnseenTicketsState;

    vm.emitTicket = emitTicket;
    vm.changeTicketDetailsState = changeTicketDetailsState;
    vm.areTicketDetailsOpened = areTicketDetailsOpened;
    vm.isTicketOfStatus = isTicketOfStatus;
    vm.isSelectedTicket = isSelectedTicket;
    vm.clearSelectedTicket = clearSelectedTicket;
    vm.isNotAnonymous = isNotAnonymous;

    vm.hasTicketPrevision = hasTicketPrevision;
    vm.hasTicketResource = hasTicketResource;
    vm.isTicketLoading = isTicketLoading;

    vm.getTicketStatusClass = getTicketStatusClass;
    vm.isTicketCalled = isTicketCalled;

    vm.showPrevisionIndicator = showPrevisionIndicator;
    vm.showResourceIndicator = showResourceIndicator;

    vm.cannotTicketDoAnyAction = cannotTicketDoAnyAction;
    vm.canConfirmTicket = canConfirmTicket;
    vm.canCheckTicket = canCheckTicket;
    vm.canReopenTicket = canReopenTicket;
    vm.canCallTicket = canCallTicket;
    vm.canStartTicket = canStartTicket;
    vm.canCompleteTicket = canCompleteTicket;
    vm.canCancelTicket = canCancelTicket;
    vm.confirmTicket = confirmTicket;
    vm.checkTicket = checkTicket;
    vm.reopenTicket = reopenTicket;
    vm.callTicket = callTicket;
    vm.startTicket = startTicket;
    vm.completeTicket = completeTicket;
    vm.cancelTicket = cancelTicket;
    vm.changePriority = changePriority;

    vm.callNextTicket = callNextTicket;
    vm.callNextPriorityService = callNextPriorityService;

    vm.orderServiceConfigs = orderServiceConfigs;
    vm.isServiceConfigOnTime = isServiceConfigOnTime;
    vm.isServiceConfigOnWarning = isServiceConfigOnWarning;
    vm.isServiceConfigLate = isServiceConfigLate;
    vm.hasServiceConfigTickets = hasServiceConfigTickets;

    vm.goToOldAttendanceRoute = goToOldAttendanceRoute;

    // Initialization
    function init() {
      vm.provider = ProviderService.getCurrentProvider();
      location = LocationsManagerService.getCurrentLocation();

      vm.isLoading = true;
      if (location) {
        getServiceConfigs(location.id).then(
          function () {
            getSessions(location.id)
              .finally(
                function () {
                  updateServiceConfigsPrevisionStatus();
                  setCurrentServiceConfig(orderByFilter(vm.serviceConfigs, [vm.orderServiceConfigs, 'name'])[0]);
                  setCurrentSession(vm.currentServiceConfig.sessions[0]);
                  if(vm.currentSession != null && vm.currentSession.tickets != null){
                    initStatusLists(vm.currentSession.tickets);
                  }
                  vm.isLoading = false;
                }
            );
          }
        );
      }
      localStorageService.set('default-attendance-route', 'new');
    }

    init();

    $scope.$on('appLocationChanged', function () {
      init();
    });
    // /Initialization

    // Listeners
    $scope.$watch('vm.selectedTicket', function (newValue, oldValue) {
      if (newValue && newValue != oldValue) {
        scrollToTicket(newValue);
      }
    });

    $scope.$watch('vm.currentServiceConfig.selectedResource', function (newValue, oldValue) {
      if (newValue) {
        vm.hasChooseResouceError = false;
      }
    });
    // /Listeners

    // Service Configurations
    function getServiceConfigs(locationId) {
      var deferred = $q.defer();
      vm.isLoading = true;

      ServiceConfigService.getServiceConfigs(locationId).then(
        function (serviceConfigs) {
          vm.serviceConfigs = serviceConfigs;

          for (var i = 0; i < vm.serviceConfigs.length; i++) {
            vm.serviceConfigs[i].sessions = [];
          }

          deferred.resolve();
        }, function (error) {
          ResponseCatcherService.catchError(error, true);
          deferred.reject();
        }
      );

      return deferred.promise;
    }

    function setCurrentServiceConfig(serviceConfig) {
      vm.currentServiceConfig = serviceConfig;
      setCurrentSession(vm.currentServiceConfig.sessions[0]);
    }

    function isCurrentServiceConfig(serviceConfig) {
      if (vm.currentServiceConfig) {
        return serviceConfig.id === vm.currentServiceConfig.id;
      }
    }

    function hasServiceConfigMoreThanOneSession(serviceConfig) {
      if (serviceConfig && serviceConfig.sessions) {
        return serviceConfig.sessions.length > 1;
      }
    }

    function isServiceConfigsListCollapsed() {
      return !!vm.serviceConfigsListCollapsed;
    }

    function collapseServiceConfigsList() {
      vm.serviceConfigsListCollapsed = !vm.serviceConfigsListCollapsed;
    }

    function updateServiceConfigCounterTicketsForAttendance(serviceConfigId){

      var serviceConfig = _.findWhere(vm.serviceConfigs, {id : serviceConfigId});

      serviceConfig.totalTicketsForAttendance =
          _.chain(serviceConfig.sessions)
            .pluck('tickets')
            .flatten(true)
            .filter(function (ticket) {
              return isTicketOfStatus('checked')(ticket) || isTicketOfStatus('called')(ticket)
            }).value().length;
    }

    // / Service Configurations

    // Sessions
    function getSessions(locationId) {
      var deferred = $q.defer();

      SessionsAttendanceService.getSessionsV2(locationId, new Date().toJSON()).then(
        function (sessions) {

          var ticket;
          for (var i = 0; i < sessions.length; i++) {
            for (var j = 0; j < vm.serviceConfigs.length; j++) {
              if (sessions[i].serviceConfigId === vm.serviceConfigs[j].id) {
                vm.serviceConfigs[j].sessions.push(sessions[i]);
              }
            }
          }

          for (i = 0; i < vm.serviceConfigs.length; i++) {
            vm.serviceConfigs[i].totalTicketsForAttendance = 0;
            for (j = 0; j < vm.serviceConfigs[i].sessions.length; j++) {
              if (vm.serviceConfigs[i].sessions[j].resources.length === 1) {
                vm.serviceConfigs[i].sessions[j].selectedResource = vm.serviceConfigs[i].sessions[j].resources[0];
                vm.isCurrentResourcePinned = true;
              }
              sessionPusherSubscriber(vm.serviceConfigs[i].sessions[j]);
              for (var k = 0; k < vm.serviceConfigs[i].sessions[j].tickets.length; k++) {
                ticket = vm.serviceConfigs[i].sessions[j].tickets[k];
                if (isTicketChecked(ticket) || isTicketCalled(ticket)) {
                  vm.serviceConfigs[i].totalTicketsForAttendance++;
                }
                ticketPusherSubscriber(ticket);
              }
            }

            if (vm.serviceConfigs[i].sessions.length > 0 && vm.serviceConfigs[i].sessions[0].resources.length === 1) {
              vm.serviceConfigs[i].selectedResource = vm.serviceConfigs[i].sessions[0].resources[0];
            }
          }

          deferred.resolve(sessions);
        },
        function (error) {
          ResponseCatcherService.catchError(error, true);
          deferred.reject(error);
        }
      );

      return deferred.promise;
    }

    function setCurrentSession(session) {
      if (session) {
        vm.currentSession = session;
        updateCounterPriorityTickets();
      } else {
        vm.currentSession = null;
      }
    }

    function isCurrentSession(session) {
      if (vm.currentSession) {
        return session.id === vm.currentSession.id;
      }
    }

    function hasCurrentSession() {
      return !!vm.currentSession;
    }

    function updateCounterPriorityTickets() {
      vm.specialTicketsCount =
        _.chain(vm.currentSession.tickets)
          .where({isSpecial: true})
          .filter(function (ticket) {
            return isTicketOfStatus('checked')(ticket) || isTicketOfStatus('called')(ticket)
          }).value().length;
    }
    // /Sessions

    // Resources
    function setCurrentResource() {
      vm.isCurrentResourcePinned = !vm.isCurrentResourcePinned;
    }
    function unsetCurrentResource() {
      vm.currentServiceConfig.selectedResource = null;
    }

    function hasCurrentResource() {
      return !!vm.currentServiceConfig.selectedResource;
    }
    // /Resources

    // Status Lists
    function initStatusLists (tickets) {
      _.forEach(tickets, expandStatusListByTicket);
    }

    function expandStatusListByTicket(ticket) {
      vm.statusLists = [
        vm.statusLists[0] || isTicketAuthorized(ticket) || isTicketReopened(ticket),
        vm.statusLists[1] || isTicketConfirmed(ticket),
        vm.statusLists[2] || isTicketChecked(ticket),
        vm.statusLists[3] || isTicketCalled(ticket),
        vm.statusLists[4] || isTicketStarted(ticket),
        vm.statusLists[5],
        vm.statusLists[6]
      ];
      // After the update, if there is any group open
      //  with the unseen indicator, then remove it
      for (var i = 0; i < vm.statusLists.length; i++) {
        if (vm.statusLists[i] && vm.unseenTickets[i]) {
          vm.unseenTickets[i] = false;
        }
      }
    }

    function flagStatusListByTicket(ticket) {
      vm.unseenTickets = [
        vm.unseenTickets[0] || (!vm.statusLists[0] && (isTicketAuthorized(ticket) || isTicketReopened(ticket))),
        vm.unseenTickets[1] || (!vm.statusLists[1] && isTicketConfirmed(ticket)),
        vm.unseenTickets[2] || (!vm.statusLists[2] && isTicketChecked(ticket)),
        vm.unseenTickets[3] || (!vm.statusLists[3] && isTicketCalled(ticket)),
        vm.unseenTickets[4] || (!vm.statusLists[4] && isTicketStarted(ticket)),
        vm.unseenTickets[5] || (!vm.statusLists[5] && isTicketCompleted(ticket)),
        vm.unseenTickets[6] || (!vm.statusLists[6] && isTicketCanceled(ticket))
      ];
    }

    function changeStatusListDetailsState(statusList) {
      vm.statusLists[statusList] = !vm.statusLists[statusList];
      if (vm.statusLists[statusList]) {
        vm.unseenTickets[statusList] = false;
      }
    }

    function getStatusListDetailsState(statusList) {
      return vm.statusLists[statusList];
    }

    function getUnseenTicketsState(statusList, listHasTicket) {
      return vm.unseenTickets[statusList] && listHasTicket;
    }
    // /Status Lists

    // Tickets
    function emitTicket(session) {
      var modal = $uibModal.open({
        templateUrl: 'modules/attendance/schedule/emit-ticket-modal.html',
        controller: 'EmitTicketModalController',
        controllerAs: 'vm',
        windowClass: 'stick-up',
        resolve: {
          resolve: function () {
            return {
              session: session,
              providerId: vm.provider.id
            };
          }
        }
      });
    }

    function changeTicketDetailsState(ticket) {
      ticket.detailsOpened = !ticket.detailsOpened;
    }

    function areTicketDetailsOpened(ticket) {
      return !!ticket && ticket.detailsOpened ? 'on' : null;
    }

    function isTicketOfStatus(status) {
      return function (ticket) {
        if (status.toUpperCase() === 'AUTHORIZED') {
          return !!ticket && ticket.status.toUpperCase() === status.toUpperCase() || ticket.status.toUpperCase() === 'REOPENED';
        }
        return !!ticket && ticket.status.toUpperCase() === status.toUpperCase();
      }
    }

    function scrollToTicket(ticket) {
      expandStatusListByTicket(ticket);

      var newHash = 'ticket-' + ticket.id;
      if ($location.hash() !== newHash) {
        $location.hash(newHash);
      } else {
        $anchorScroll();
      }
    }

    function isSelectedTicket(ticket) {
      return !!ticket && ticket === vm.selectedTicket ? 'highlighted' : null;
    }

    function clearSelectedTicket() {
      vm.selectedTicket = null;
    }

    function isNotAnonymous(ticket) {
      return ticket.customer.email.indexOf("@terminal") === -1;
    }
    // /Tickets

    // Ticket Data
    function updateTicketData(ticket, newTicket) {
      angular.extend(ticket, newTicket);
      flagStatusListByTicket(newTicket);
    }

    function hasTicketPrevision(ticket) {
      return !!ticket.prevision && !(isTicketAuthorized(ticket) || isTicketReopened(ticket));
    }

    function hasTicketResource(ticket) {
      return !!ticket.resource;
    }

    function isTicketLoading(ticket) {
      return ticket.isLoading;
    }
    // /Ticket Data

    // Ticket Status
    function getTicketStatusClass(ticket) {
      if (isTicketAuthorized(ticket) || isTicketReopened(ticket)) {
        return 'scheduled'
      }
      if (isTicketConfirmed(ticket)) {
        return 'confirmed'
      }
      if (isTicketChecked(ticket)) {
        return 'checked'
      }
      if (isTicketCalled(ticket)) {
        return 'called'
      }
      if (isTicketStarted(ticket)) {
        return 'called'
      }
      if (isTicketCompleted(ticket)) {
        return 'finalized'
      }
      if (isTicketCanceled(ticket)) {
        return 'canceled'
      }
    }

    function isTicketAuthorized(ticket) {
      return !!ticket && ticket.status.toUpperCase() === "AUTHORIZED"
    }

    function isTicketConfirmed(ticket) {
      return !!ticket && ticket.status.toUpperCase() === "CONFIRMED"
    }

    function isTicketChecked(ticket) {
      return !!ticket && ticket.status.toUpperCase() === "CHECKED"
    }

    function isTicketCalled(ticket) {
      return !!ticket && ticket.status.toUpperCase() === "CALLED"
    }

    function isTicketReopened(ticket) {
      return !!ticket && ticket.status.toUpperCase() === "REOPENED"
    }

    function isTicketStarted(ticket) {
      return !!ticket && ticket.status.toUpperCase() === "STARTED"
    }

    function isTicketCompleted(ticket) {
      return !!ticket && ticket.status.toUpperCase() === "COMPLETED"
    }

    function isTicketCanceled(ticket) {
      return !!ticket && ticket.status.toUpperCase() === "CANCELED"
    }
    // /Ticket Status

    // Indicators
    function showPrevisionIndicator(ticket) {
      return (isTicketConfirmed(ticket) || isTicketChecked(ticket));
    }

    function showResourceIndicator(ticket) {
      return (isTicketCalled(ticket) || isTicketStarted(ticket) || isTicketCompleted(ticket));
    }
    // /Indicators

    // Ticket Actions
    function cannotTicketDoAnyAction(ticket) {
      return (ticket.actions.length === 0);
    }

    function canConfirmTicket(ticket) {
      return canDoAction(ticket, 'confirm');
    }

    function canCheckTicket(ticket) {
      return canDoAction(ticket, 'checkin');
    }

    function canReopenTicket(ticket) {
      return canDoAction(ticket, 'reopen');
    }

    function canCallTicket(ticket) {
      return canDoAction(ticket, 'call');
    }

    function canStartTicket(ticket) {
      return canDoAction(ticket, 'start');
    }

    function canCompleteTicket(ticket) {
      return canDoAction(ticket, 'complete');
    }

    function canCancelTicket(ticket) {
      return canDoAction(ticket, 'cancel');
    }

    function canDoAction(ticket, action) {
      for (var i = 0; i < ticket.actions.length; i++) {
        if (action === ticket.actions[i]) {
          return true;
        }
      }
    }

    function confirmTicket(ticket) {
      doAction(ticket, 'confirm');
    }

    function checkTicket(ticket) {
      doAction(ticket, 'check');
    }

    function reopenTicket(ticket) {
      doAction(ticket, 'reopen');
    }

    function callTicket(ticket) {
      var data = {};
      if (hasCurrentResource()) {
        data.resourceId = vm.currentServiceConfig.selectedResource.id;
        doAction(ticket, 'call', data);
      } else {
        NotificationsService.error(vm.noResourceSelectedErrorMessage);
        vm.hasChooseResouceError = true;
      }
      if (!vm.isCurrentResourcePinned) {
        unsetCurrentResource();
      }
    }

    function startTicket(ticket) {
      var data = {};
      if (hasCurrentResource()) {
        data.resourceId = vm.currentServiceConfig.selectedResource.id;
        doAction(ticket, 'start', data);
      } else {
        NotificationsService.error(vm.noResourceSelectedErrorMessage);
        vm.hasChooseResouceError = true;
      }
      if (!vm.isCurrentResourcePinned) {
        unsetCurrentResource();
      }
    }

    function completeTicket(ticket) {
      doAction(ticket, 'complete');
    }

    function cancelTicket(ticket) {
      doAction(ticket, 'cancel');
    }

    function doAction(ticket, action, data) {
      var deferred = $q.defer();

      ticket.isLoading = true;
      SessionsAttendanceService.actionOnTicket(ticket.id, action, data).then(
        function (response) {
          updateServiceConfigsPrevisionStatus();
          angular.extend(ticket, response.responseData);
          ResponseCatcherService.catchSuccess(response);
          expandStatusListByTicket(ticket);
          deferred.resolve(response);
        },
        function (error) {
          ResponseCatcherService.catchError(error);
          deferred.reject(error);
        }
      ).finally(function () {
        ticket.detailsOpened = false;
        ticket.isLoading = false;
      });

      return deferred.promise;
    }

    function changePriority(ticket) {
      var data = { priority: ticket.isSpecial };
      TicketsAttendanceService.changePreference(ticket.id, data).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response, false, false, false);
        },
        function (error) {
          ResponseCatcherService.catchError(error);
        }
      )
    }
    // /Ticket Actions

    // Call Next
    function callNextTicket(special) {
      var data = {};
      if (hasCurrentResource()) {
        data.sessionId = vm.currentSession.id;
        data.resourceId = vm.currentServiceConfig.selectedResource.id;
        data.callSpecial = special;

        SessionsAttendanceService.callNext(data).then(
          function (response) {
            ResponseCatcherService.catchSuccess(response);
            expandStatusListByTicket(response.responseData);
          },
          function (error) {
            ResponseCatcherService.catchError(error);
          }
        );
      } else {
        NotificationsService.error(vm.noResourceSelectedErrorMessage);
        vm.hasChooseResouceError = true;
      }
      if (!vm.isCurrentResourcePinned) {
        unsetCurrentResource();
      }
    }
    // /Call Next

    // Call Next
    function callNextPriorityService() {
      var data = {};
      if (hasCurrentResource()) {
        data.sessionId = vm.currentSession.id;
        data.resourceId = vm.currentServiceConfig.selectedResource.id;

        SessionsAttendanceService.callNextPriorityService(data).then(
          function (response) {
            ResponseCatcherService.catchSuccess(response);
            expandStatusListByTicket(response.responseData);
          },
          function (error) {
            ResponseCatcherService.catchError(error);
          }
        );
      } else {
        NotificationsService.error(vm.noResourceSelectedErrorMessage);
        vm.hasChooseResouceError = true;
      }
      if (!vm.isCurrentResourcePinned) {
        unsetCurrentResource();
      }
    }
    // /Call Next

    // Prevision Status
    function updateServiceConfigsPrevisionStatus() {
      var currentTime = new Date();
      var nextTicket;
      for (var i = 0; i < vm.serviceConfigs.length; i++) {
        nextTicket = getNextTicketFromServiceConfig(vm.serviceConfigs[i]);
        if (nextTicket) {
          nextTicket.prevision = new Date(nextTicket.prevision);
          if (nextTicket.prevision.getTime() >= currentTime.getTime()) {
            vm.serviceConfigs[i].previsionStatus = 'onTime';
          } else {
            if (nextTicket.prevision.getTime() >= (new Date(currentTime.getTime() - 15 * 60000)).getTime()) {
              vm.serviceConfigs[i].previsionStatus = 'onWarning';
            } else {
              vm.serviceConfigs[i].previsionStatus = 'isLate';
            }
          }
        } else {
          vm.serviceConfigs[i].previsionStatus = 'noTickets';
        }
      }

      updateServiceConfigTimeout = $timeout(function () {
        updateServiceConfigsPrevisionStatus()
      }, 30000);
    }

    function getNextTicketFromServiceConfig(serviceConfig) {
      var nextTicket;
      var ticket;
      if (serviceConfig.sessions.length) {
        for (var i = 0; i < serviceConfig.sessions.length; i++) {
          for (var j = 0; j < serviceConfig.sessions[i].tickets.length; j++) {
            ticket = serviceConfig.sessions[i].tickets[j];
            if (ticket.prevision && (isTicketChecked(ticket) || isTicketCalled(ticket))) {
              if (nextTicket) {
                if (nextTicket.prevision > ticket.prevision) {
                  nextTicket = ticket;
                }
              } else {
                nextTicket = ticket;
              }
            }
          }
        }
        return nextTicket;
      }
    }

    function orderServiceConfigs(serviceConfig) {
      if (serviceConfig.previsionStatus === 'isLate') {
        return 0;
      } else if (serviceConfig.previsionStatus === 'onWarning') {
        return 1;
      } else if (serviceConfig.previsionStatus === 'onTime') {
        return 2;
      } else if (serviceConfig.previsionStatus === 'noTickets') {
        return 3;
      } else {
        return 4;
      }
    }

    function isServiceConfigOnTime(serviceConfig) {
      return serviceConfig.previsionStatus === 'onTime';
    }

    function isServiceConfigOnWarning(serviceConfig) {
      return serviceConfig.previsionStatus === 'onWarning';
    }

    function isServiceConfigLate(serviceConfig) {
      return serviceConfig.previsionStatus === 'isLate';
    }

    function hasServiceConfigTickets(serviceConfig) {
      return serviceConfig.previsionStatus !== 'noTickets';
    }
    // /Prevision Status

    // Routing
    function goToOldAttendanceRoute() {
      localStorageService.set('default-attendance-route', 'old');
      $state.go('app.provider.attendance', { providerSlug: vm.provider.slug })
    }

    // /Routing

    // Pusher
    function sessionPusherSubscriber(session) {
      Pusher.subscribe('session-' + session.id, 'TicketNew', function (data) {
        session.tickets.push(data.ticket);
        flagStatusListByTicket(data.ticket);
        ticketPusherSubscriber(data.ticket);
      });
    }

    function ticketPusherSubscriber(ticket) {
      Pusher.subscribe('ticket-' + ticket.id, 'TicketConfirm', function (data) {
        updateTicketData(ticket, data.ticket);
      });

      Pusher.subscribe('ticket-' + ticket.id, 'TicketCheckin', function (data) {
        updateTicketData(ticket, data.ticket);
      });

      Pusher.subscribe('ticket-' + ticket.id, 'TicketCalled', function (data) {
        updateTicketData(ticket, data.ticket);
      });

      Pusher.subscribe('ticket-' + ticket.id, 'TicketReopened', function (data) {
        updateTicketData(ticket, data.ticket);
      });

      Pusher.subscribe('ticket-' + ticket.id, 'TicketStart', function (data) {
        updateTicketData(ticket, data.ticket);
      });

      Pusher.subscribe('ticket-' + ticket.id, 'TicketComplete', function (data) {
        updateTicketData(ticket, data.ticket);
      });

      Pusher.subscribe('ticket-' + ticket.id, 'TicketCancel', function (data) {
        updateTicketData(ticket, data.ticket);
      });

      Pusher.subscribe('ticket-' + ticket.id, 'PrevisionChanged', function (data) {
        updateTicketData(ticket, data.ticket);
      });

      Pusher.subscribe('ticket-' + ticket.id, 'TicketChanged', function (data) {
        updateTicketData(ticket, data.ticket);
        updateCounterPriorityTickets();
        updateServiceConfigCounterTicketsForAttendance(data.ServiceConfigId);
      });
    }
    // /Pusher
  }
})();
