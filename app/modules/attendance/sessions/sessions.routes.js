angular
  .module('filazero-app')
  .config(['$stateProvider', function ($stateProvider) {
    'use strict';
    $stateProvider
      .state('app.provider.attendance-direct', {
        url: '/attendance/services/:serviceId/sessions/:sessionId',
        data: {
          requireAuthentication: true
        },
        templateUrl: 'modules/attendance/sessions/sessions.html',
        controller: 'SessionsAttendanceController'
      });

    $stateProvider
      .state('app.provider.attendance', {
        url: '/attendance',
        data: {
          requireAuthentication: true
        },
        templateUrl: 'modules/attendance/sessions/sessions-attendance.html',
        controller: 'SessionsAttendanceController',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              'services/attendance/sessions-attendance.service.js',
              'services/attendance/ticket-attendance.service.js',
              'services/attendance/schedule-attendance.service.js',
              'services/locations.service.js',
              'services/provider.service.js',
              'services/manager/locations-manager.service.js',
              'services/service-config.service.js',
              'services/client-attendance.service.js',
              'services/manager/rooms-manager.service.js'
            ], {
                insertBefore: '#lazyload_placeholder'
              }).then(function () {
                return $ocLazyLoad.load([
                  'modules/attendance/sessions/sessions-attendance.controller.js',
                  'modules/attendance/sessions/call-next-attendance-modal.controller.js',
                  'modules/attendance/sessions/ticket-details-modal.controller.js',
                  'modules/attendance/schedule/emit-ticket-modal.controller.js',
                  'modules/attendance/sessions/close-session-modal.controller.js',
                  'modules/service-config/service-config-add-modal.controller.js',
                ]);
              });
          }]
        }
      })
      .state('app.provider.new-attendance', {
        url: '/new-attendance',
        data: {
          requireAuthentication: true
        },
        templateUrl: 'modules/attendance/sessions/new-sessions-attendance.html',
        controller: 'NewSessionsAttendanceController',
        controllerAs: 'vm',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              'services/provider.service.js',
              'services/manager/locations-manager.service.js',
              'services/service-config.service.js',
              'services/attendance/sessions-attendance.service.js',
              'services/attendance/schedule-attendance.service.js',
              'services/client-attendance.service.js'
            ], {
                insertBefore: '#lazyload_placeholder'
              }).then(function () {
                return $ocLazyLoad.load([
                  'modules/attendance/sessions/new-sessions-attendance.controller.js',
                  'modules/attendance/schedule/emit-ticket-modal.controller.js',
                ]);
              });
          }]
        }
      })
      .state('app.provider.new-attendance-walkthrough', {
        url: '/new-attendance-walkthrough',
        data: {
          requireAuthentication: true
        },
        templateUrl: 'modules/attendance/sessions/new-sessions-attendance-walkthrough.html',
        controller: 'NewSessionsAttendanceWalkthroughController',
        controllerAs: 'vm',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              'services/provider.service.js'
            ], {
                insertBefore: '#lazyload_placeholder'
              }).then(function () {
                return $ocLazyLoad.load([
                  'modules/attendance/sessions/new-sessions-attendance-walkthrough.controller.js',
                ]);
              });
          }]
        }
      })
      .state('session-basic-attendance', {
        url: '/p/:providerSlug/locations/:locationId/attendance/:s',
        data: {
          requireAuthentication: true
        },
        templateUrl: 'modules/attendance/sessions/basic-attendance/session-basic-attendance.html',
        controller: 'SessionBasicAttendanceController',
        controllerAs: 'vm',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              'services/attendance/ticket-attendance.service.js',
              'services/attendance/sessions-attendance.service.js'
            ], {
                insertBefore: '#lazyload_placeholder'
              }).then(function () {
                return $ocLazyLoad.load([
                  'modules/attendance/sessions/basic-attendance/session-basic-attendance.controller.js',
                  'modules/attendance/sessions/ticket-details-modal.controller.js'
                ]);
              });
          }],
          currentProvider: ['$stateParams', 'ProviderService', function ($stateParams, ProviderService) {
            return ProviderService.verifyProvider($stateParams.providerSlug).then(
              function () {
                return ProviderService.getCurrentProvider();
              }
            );
          }]
        }
      })
      .state('basic-attendance', {
        url: '/p/:providerSlug/locations/:locationId/basic-attendance',
        data: {
          requireAuthentication: true
        },
        templateUrl: 'modules/attendance/sessions/basic-attendance/basic-attendance.html',
        controller: 'BasicAttendanceController',
        controllerAs: 'vm',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              'services/attendance/sessions-attendance.service.js',
              'services/manager/locations-manager.service.js',
              'services/locations.service.js'
            ], {
                insertBefore: '#lazyload_placeholder'
              }).then(function () {
                return $ocLazyLoad.load([
                  'modules/attendance/sessions/basic-attendance/basic-attendance.controller.js'
                ]);
              });
          }],
          currentProvider: ['$stateParams', 'ProviderService', function ($stateParams, ProviderService) {
            return ProviderService.verifyProvider($stateParams.providerSlug).then(
              function () {
                return ProviderService.getCurrentProvider();
              }
            );
          }]
        }
      });
  }]);
