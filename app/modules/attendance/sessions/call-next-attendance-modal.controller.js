;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('CallNextAttendanceController', CallNextAttendanceController);

  CallNextAttendanceController.$inject = ['$uibModalInstance', 'resolve'];

  function CallNextAttendanceController($uibModalInstance, resolve) {
    var vm = this;

    vm.ticket = {};
    vm.resourcesRooms = {};
    vm.selectedResource = {};

    vm.callNext = callNext;
    vm.cancel = cancel;

    init();

    function init(){
      vm.ticket = resolve.ticket;
      vm.resourcesRooms = resolve.session.resourcesRooms;
      vm.selectedResource = vm.resourcesRooms[0];
    }

    function callNext() {
      if(vm.selectedResource.resource){
        $uibModalInstance.close(vm.selectedResource.resource.id);
      }
    }

    function cancel() {
      $uibModalInstance.close();
    }
  }
}());
