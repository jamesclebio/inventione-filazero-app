;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('SelfServiceTerminalController', SelfServiceTerminalController);

  SelfServiceTerminalController.$inject = ['$timeout', '$stateParams', '$window', 'TerminalsService', 'TicketingService', 'ResponseCatcherService', 'ProviderService', 'LocationsAdminService'];

  function SelfServiceTerminalController($timeout, $stateParams, $window, TerminalsService, TicketingService, ResponseCatcherService, ProviderService, LocationsAdminService) {

    var vm = this;
    var timeout;

    vm.currentPage = 1;
    vm.emitted = false;
    vm.terminal = {};
    vm.ticket = {};
    vm.today = new Date();
    vm.isTicketSpecial = undefined;

    vm.canShowMore = canShowMore;
    vm.deselectService = deselectService;
    vm.isSpecial = isSpecial;
    vm.isNotSpecial = isNotSpecial;
    vm.selectService = selectService;
    vm.showNext = showNext;
    vm.showPrevious = showPrevious;

    init();

    function init(){
      fireDigestEverySecond();
      getLocationData();
      getTerminalData($stateParams.locationId, $stateParams.terminalId);
    }

    // Requests

    function getLocationData(){
      ProviderService.getProviderBySlug($stateParams.providerSlug).then(
        function(response){
          vm.provider = response;
        }
      );
      LocationsAdminService.getLocation($stateParams.providerSlug, $stateParams.locationId).then(
        function(response){
          vm.location = response;
        }
      );
    }

    function getTerminalData(locationId, terminalId){
      TerminalsService.getTerminal(locationId, terminalId).then(
        function(terminal){
          vm.terminal = terminal;
          if(vm.terminal.services.length === 1){
            vm.selectedService = vm.terminal.services[0];
          }
        }
      );
    }

    // Atualizar hora
    function fireDigestEverySecond() {
      vm.today = new Date();
      $timeout(fireDigestEverySecond , 1000);
    }

    function canShowMore(){
      if(vm.terminal.services){
        var item = (vm.currentPage)*8;
        return vm.terminal.services[item];
      }else{
        return false;
      }
    }

    // Page Change
    function showNext(){
      vm.currentPage = vm.currentPage + 1;
    }

    function showPrevious(){
      vm.currentPage = vm.currentPage - 1;
    }

    // Service Selected
    function selectService(serviceId){
      for(var i = 0; i < vm.terminal.services.length; i++){
        if(vm.terminal.services[i].id === serviceId){
          vm.selectedService = vm.terminal.services[i];
        }
      }
      $timeout(restartTerminal , 15000);
    }

    function deselectService(){
      clearTimeout(timeout);
      vm.selectedService = {};
    }

    // Is special?
    function isSpecial(){
      vm.isTicketSpecial = true;
      emitTicket();
    }

    function isNotSpecial(){
      vm.isTicketSpecial = false;
      emitTicket();
    }

    // Ticket Emission
    function emitTicket(){
      var ticket = {
        isSpecial: vm.isTicketSpecial
      };

      vm.isLoading = true;

      TicketingService.emitTerminalTicket($stateParams.locationId, $stateParams.terminalId, vm.selectedService.id, ticket).then(
        function(response){
          ResponseCatcherService.catchSuccess(response, false, false, false, false);
          clearTimeout(timeout);
          vm.isLoading = false;
          vm.ticket = response.responseData;
          vm.emitted = true;
          vm.responseCode = response.messages[0].code;
          if(vm.responseCode === "1007"){
            $timeout($window.print , 0);
          }
          $timeout(refreshTerminal , 5000);
        }
      )
    }

    // Reiniciar o terminal
    function refreshTerminal(){
      if(vm.terminal.services.length === 1){
        vm.selectedService = vm.terminal.services[0];
      }else{
        vm.selectedService = {};
      }
      vm.isTicketSpecial = undefined;
      vm.timedOut = false;
      vm.emitted = false;
      vm.responseCode = {};
    }

    function restartTerminal(){
      if(vm.isTicketSpecial === undefined && !vm.emitted && vm.selectedService.id){
        vm.timedOut = true;
        if(vm.terminal.services.length > 1){
          timeout = $timeout(refreshTerminal , 5000);
        }
      }
    }
  }
})();
