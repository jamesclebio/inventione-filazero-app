;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider.state('self-service-terminal', {
      url: '/p/:providerSlug/l/:locationId/self-service-terminal/:terminalId',
      data: {
        requireAuthentication: true
      },
      templateUrl: 'modules/attendance/terminal/self-service-terminal.html',
      controller: 'SelfServiceTerminalController',
      controllerAs: 'vm',
      resolve:{
        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
          return $ocLazyLoad.load([], {
            insertBefore: '#lazyload_placeholder'
          }).then(function () {
            return $ocLazyLoad.load([
              'modules/attendance/terminal/self-service-terminal.controller.js',
              'services/terminals.service.js',
              'services/ticketing.service.js',
              'services/provider.service.js',
              'services/locations.service.js'
            ]);
          });
        }],
        currentProvider: ['$stateParams', 'ProviderService', function($stateParams, ProviderService) {
          return ProviderService.verifyProvider($stateParams.providerSlug).then(
            function() {
              return ProviderService.getCurrentProvider();
            }
          );
        }]
      }
    });
  }
}());
