; (function () {
	'use strict';

	angular
		.module('filazero-app')
		.controller('RoomRemoveModalController', RoomRemoveModalController);

	RoomRemoveModalController.$inject = ['LocationsManagerService', 'RoomsManagerService', '$uibModalInstance', 'resolve', 'ResponseCatcherService'];

	function RoomRemoveModalController(LocationsManagerService, RoomsManagerService, $uibModalInstance, resolve, ResponseCatcherService) {
		var vm = this;

		vm.room = resolve || null;

		vm.closeModal = closeModal;
		vm.removeRoom = removeRoom;

		function removeRoom() {
      vm.isLoading = true;
			var removeRoomObject = {
				locationId: LocationsManagerService.getCurrentLocation().id,
				room: vm.room
			};
			RoomsManagerService.removeRoom(removeRoomObject).then(
				function (response) {
          ResponseCatcherService.catchSuccess(response).then(
            function(){
              $uibModalInstance.close();
            }
          );
        },
				function (error) {
          ResponseCatcherService.catchError(error);
				}).finally(function(){
        vm.isLoading = false;
      });
		}

		function closeModal() {
      $uibModalInstance.dismiss();
    }
	}
})();
