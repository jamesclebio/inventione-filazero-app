;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('RoomManagerModalController', RoomManagerModalController);

  RoomManagerModalController.$inject = ['LocationsManagerService', 'RoomsManagerService', '$uibModalInstance', 'resolve', 'ResponseCatcherService'];

  function RoomManagerModalController(LocationsManagerService, RoomsManagerService, $uibModalInstance, resolve, ResponseCatcherService) {
    var vm = this;

    vm.room = resolve || null;

    vm.closeModal = closeModal;
    vm.isNewModal = isNewModal;
    vm.save = save;

    init();

    function init() {
      if (!isNewModal()) {
        var location = LocationsManagerService.getCurrentLocation();
        getRoom(location.id, resolve.roomId);
      }
    }

    function save(room) {
      if (isNewModal()) {
        addRoom(room);
      } else {
        editRoom(room);
      }
    }

    function getRoom(locationId, roomId) {
      vm.isLoading = true;
      RoomsManagerService.getRoom(locationId, roomId).then(
        function (room) {
          vm.room = room;
          vm.isLoading = false;
        },
        function (error) {
          ResponseCatcherService.catchError(error);
          $uibModalInstance.close();
        });
    }

    function addRoom(room) {
      vm.isLoading = true;
      var addRoomObject = {
        locationId: LocationsManagerService.getCurrentLocation().id,
        room: room
      };
      RoomsManagerService.addRoom(addRoomObject).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response).then(
            function () {
              $uibModalInstance.close();
            });
        },
        function () {
          ResponseCatcherService.catchError(error);
        }).finally(
        function () {
          vm.isLoading = false;
        }
      );
    }

    function editRoom(room) {
      vm.isLoading = true;
      var editRoomObject = {
        locationId: LocationsManagerService.getCurrentLocation().id,
        room: room
      };
      RoomsManagerService.updateRoom(editRoomObject).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response).then(
            function () {
              $uibModalInstance.close();
            });
        },
        function () {
          ResponseCatcherService.catchError(error);
        }).finally(
        function () {
          vm.isLoading = false;
        }
      );
    }

    function closeModal() {
      $uibModalInstance.dismiss();
    }

    function isNewModal() {
      return !resolve;
    }
  }
})();
