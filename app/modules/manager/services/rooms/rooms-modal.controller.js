; (function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('RoomsModalController', RoomsModalController);

  RoomsModalController.$inject = ['LocationsManagerService', 'RoomsManagerService', '$uibModal', 'ResponseCatcherService'];

  function RoomsModalController(LocationsManagerService, RoomsManagerService, $uibModal, ResponseCatcherService) {
    var vm = this;
    var location;

    vm.currentPage = 1;
    vm.itensLimit = 6;
    vm.order = 'name';

    vm.setOrder = setOrder;
    vm.addRoom = addRoom;
    vm.editRoom = editRoom;
    vm.removeRoom = removeRoom;

    function init() {
      location = LocationsManagerService.getCurrentLocation();

      if (location) {
        getRooms(location.id);
      }
    }

    // Rooms
    function getRooms(locationId) {
      vm.isLoading = true;
      RoomsManagerService.getRooms(locationId).then(
        function (response) {
          vm.rooms = response;
        },
        function(error){
          ResponseCatcherService.catchError(error, true);

        }).finally(function(){
        vm.isLoading = false;
      });
    }

    function addRoom() {
      var modalInstance = openModal(
        'modules/manager/services/rooms/room-manager-modal.html',
        'RoomManagerModalController'
        );

      modalInstance.result.then(
        function () {
          getRooms(location.id);
        });
    }

    function editRoom(room) {
      var resolve = {
        roomId: room.id
      };

      var modalInstance = openModal(
        'modules/manager/services/rooms/room-manager-modal.html',
        'RoomManagerModalController',
        resolve
        );

      modalInstance.result.then(
        function () {
          getRooms(location.id);
        });
    }

    function removeRoom(room) {
      var modalInstance = openModal(
        'modules/manager/services/rooms/room-remove-modal.html',
        'RoomRemoveModalController',
        room
        );

      modalInstance.result.then(
        function () {
          getRooms(location.id);
        });
    }
    // /Rooms

    function setOrder(order) {
      if (order === vm.order) {
        vm.order = '-' + order;
      } else {
        vm.order = order;
      }
    }

    function openModal(templateUrl, controller, resolve) {
      return $uibModal.open({
        templateUrl: templateUrl,
        controller: controller,
        controllerAs: 'vm',
        size: 'md',
        windowClass: 'stick-up',
        resolve: {
          resolve: function () {
            return resolve;
          }
        }
      });
    }

    init();
  }
})();
