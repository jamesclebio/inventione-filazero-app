;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('ServiceRemoveModalController', ServiceRemoveModalController);

  ServiceRemoveModalController.$inject = [
    'LocationsManagerService',
    'ServicesManagerService',
    '$uibModalInstance',
    'resolve',
    'ResponseCatcherService'
  ];

  function ServiceRemoveModalController(LocationsManagerService, ServicesManagerService, $uibModalInstance, resolve, ResponseCatcherService) {
    var vm = this;

    vm.service = resolve || null;

    vm.closeModal = closeModal;
    vm.removeService = removeService;

    function removeService() {
      vm.isLoading = true;
      var locationId = LocationsManagerService.getCurrentLocation().id;

      ServicesManagerService.removeService(locationId, vm.service.id).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response).then(
            function (messages) {
              $uibModalInstance.close();
            }
          )
        },
        function (error) {
          ResponseCatcherService.catchError(error);
        }).finally(function () {
        vm.isLoading = false;
      });
    }

    function closeModal() {
      $uibModalInstance.dismiss();
    }
  }
})();
