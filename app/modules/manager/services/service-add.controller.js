;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('ServiceAddController', ServiceAddController);

  ServiceAddController.$inject = [
    'LocationsManagerService',
    'ServicesManagerService',
    'RoomsManagerService',
    'ResourcesManagerService',
    '$scope',
    '$uibModal',
    '$state',
    'currentProvider',
    'ResponseCatcherService'
  ];

  function ServiceAddController(LocationsManagerService, ServicesManagerService, RoomsManagerService, ResourcesManagerService, $scope, $uibModal, $state, currentProvider, ResponseCatcherService) {
    var vm = this;
    var location;

    vm.addRoom = addRoom;
    vm.addResource = addResource;
    vm.addResourceAndRoom = addResourceAndRoom;
    vm.removeResourceAndRoom = removeResourceAndRoom;
    vm.isResourceSelected = isResourceSelected;
    vm.hasSelectedResourceAndRoom = hasSelectedResourceAndRoom;
    vm.searchServices = searchServices;
    vm.save = save;
    vm.cancel = cancel;

    vm.service = {};
    vm.service.tags = [];
    vm.service.update = true;
    vm.service.restrictedSchedule = true;
    vm.service.resourcesRooms = [];
    vm.selected = {};

    // Initialization
    function init() {
      vm.provider = currentProvider;
      location = LocationsManagerService.getCurrentLocation();

      if (location) {
        getRooms(location.id);
        getResources(location.id);
      }
    }

    init();
    // /Initialization

    // Listeners
    $scope.$on('appLocationChanged', function () {
      location = LocationsManagerService.getCurrentLocation();
      getRooms(location.id);
      getResources(location.id);
    });
    // /Listeners

    // Services
    function searchServices(queryString) {
      return ServicesManagerService.searchServices(queryString).then(
        function (response) {
          return response;
        },
        function (error) {
          ResponseCatcherService.catchError(error);
        });
    }

    function save(service) {
      addService(service);
    }

    function addService(service) {
      vm.isLoading = true;
      var addServiceObject = {
        locationId: location.id,
        service: service
      };

      for (var i = 0; i < addServiceObject.service.tags.length; i++) {
        addServiceObject.service.tags[i] = addServiceObject.service.tags[i].text;
      }

      ServicesManagerService.addService(addServiceObject).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response).then(
            function () {
              $state.go('app.provider.services', {'providerSlug': vm.provider.slug});
            }
          );
        },
        function (error) {
          ResponseCatcherService.catchError(error);
        }).finally(function () {
          vm.isLoading = false;
        });
    }

    // /Services

    // Resources
    function getResources(locationId) {
      vm.isLoadingResourcesOrRooms = true;
      ResourcesManagerService.getResources(locationId).then(
        function (response) {
          vm.resources = response;
          vm.isLoadingResourcesOrRooms = false;
        },
        function (error) {
          ResponseCatcherService.catchError(error, true)
        });
    }

    function addResource() {
      openModal(
        'modules/manager/services/resources/resource-manager-modal.html',
        'ResourceManagerModalController'
      ).result.then(
        function () {
          getResources(location.id);
        });
    }

    // /Resources

    // Rooms
    function getRooms(locationId) {
      vm.isLoadingResourcesOrRooms = true;
      RoomsManagerService.getRooms(locationId).then(
        function (response) {
          vm.rooms = response;
          vm.isLoadingResourcesOrRooms = false;
        },
        function (error) {
          ResponseCatcherService.catchError(error, true)
        });
    }

    function addRoom() {
      var modalInstance = openModal(
        'modules/manager/services/rooms/room-manager-modal.html',
        'RoomManagerModalController'
      );

      modalInstance.result.then(
        function () {
          getRooms(location.id);
        });
    }

    // /Rooms

    // Resource and Room
    function addResourceAndRoom(selected) {
      vm.isLoadingResourcesOrRooms = true;
      if (vm.hasSelectedResourceAndRoom()) {
        vm.service.resourcesRooms.push(
          {
            'resource': {
              'id': selected.resource.id,
              'name': selected.resource.name
            },
            'room': {
              'id': selected.room.id,
              'name': selected.room.name
            }
          }
        );
        vm.selected.resource = null;
        vm.selected.room = null;
      }
      vm.isLoadingResourcesOrRooms = false;
    }

    function removeResourceAndRoom(resourceAndRoom) {
      vm.isLoadingResourcesOrRooms = true;
      if (vm.service.resourcesRooms.length > 1) {
        for (var i = 0; i < vm.service.resourcesRooms.length; i++) {
          if (vm.service.resourcesRooms[i].resource.id === resourceAndRoom.resource.id) {
            vm.service.resourcesRooms.splice(i, 1);
            break;
          }
        }
      }
      vm.isLoadingResourcesOrRooms = false;
    }

    function isResourceSelected(resource) {
      for (var i = 0; i < vm.service.resourcesRooms.length; i++) {
        if (vm.service.resourcesRooms[i].resource.id === resource.id) {
          return true;
        }
      }
    }

    function hasSelectedResourceAndRoom() {
      return (vm.selected.resource && vm.selected.room);
    }

    // /Resource and Room

    function cancel() {
      $state.go('app.provider.services', {'providerSlug': vm.provider.slug});
    }

    function openModal(templateUrl, controller, resolve) {
      return $uibModal.open({
        templateUrl: templateUrl,
        controller: controller,
        controllerAs: 'vm',
        size: 'md',
        windowClass: 'stick-up',
        resolve: {
          resolve: function () {
            return resolve;
          }
        }
      });
    }
  }
})();
