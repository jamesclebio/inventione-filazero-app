; (function() {
  'use strict';

  angular
    .module('filazero-app')
    .controller('ServiceEditController', ServiceEditController);

  ServiceEditController.$inject = [
    'LocationsManagerService',
    'ServicesManagerService',
    'RoomsManagerService',
    'ResourcesManagerService',
    '$scope',
    '$uibModal',
    '$state',
    '$stateParams',
    'currentProvider',
    'ResponseCatcherService'
  ];

  function ServiceEditController(LocationsManagerService, ServicesManagerService, RoomsManagerService, ResourcesManagerService, $scope, $uibModal, $state, $stateParams, currentProvider, ResponseCatcherService) {
    var vm = this;
    var location, serviceId;

    vm.addRoom = addRoom;
    vm.addResource = addResource;
    vm.addResourceAndRoom = addResourceAndRoom;
    vm.removeResourceAndRoom = removeResourceAndRoom;
    vm.isResourceSelected = isResourceSelected;
    vm.hasSelectedResourceAndRoom = hasSelectedResourceAndRoom;
    vm.searchServices = searchServices;
    vm.save = save;
    vm.cancel = cancel;

    vm.service = {};
    vm.service.tags = [];
    vm.service.update = true;
    vm.service.resourcesRooms = [];
    vm.selected = {};

    // Initialization
    function init() {
      vm.provider = currentProvider;
      location = LocationsManagerService.getCurrentLocation();
      serviceId = $stateParams.serviceId;

      if (location) {
        getService(location.id, serviceId);
        getRooms(location.id);
        getResources(location.id);
      }
    }

    init();
    // /Initialization

    // Listeners
    $scope.$on('appLocationChanged', function() {
      location = LocationsManagerService.getCurrentLocation();
      serviceId = $stateParams.serviceId;

      if (location) {
        getService(location.id, serviceId);
        getRooms(location.id);
        getResources(location.id);
      }
    });
    // /Listeners

    // Services
    function searchServices(queryString) {
      return ServicesManagerService.searchServices(queryString).then(
        function(response) {
          return response;
        },
        function(error){
          ResponseCatcherService.catchError(error);
        });
    }

    function save(service) {
      editService(service);
    }

    function getService(locationId, serviceId) {
      vm.isLoading = true;
      ServicesManagerService.getService(locationId, serviceId).then(
        function(response) {
          vm.service = response;
          vm.isLoading = false;
        },
        function(error){
          ResponseCatcherService.catchError(error);
        });
    }

    function editService(service) {
      vm.isLoading = true;
      var editServiceObject = {
        locationId: location.id,
        service: service
      };

      for (var i = 0; i < editServiceObject.service.tags.length; i++) {
        editServiceObject.service.tags[i] = editServiceObject.service.tags[i].text;
      }

      ServicesManagerService.editService(editServiceObject).then(
        function(response) {
          ResponseCatcherService.catchSuccess(response).then(
            function(){
              $state.go('app.provider.services', { 'providerSlug': vm.provider.slug });
            }
          );
          vm.isLoading = false;
        },
        function(error){
          ResponseCatcherService.catchError(error);
        });
    }

    // /Services

    // Resources
    function getResources(locationId) {
      vm.isLoading = true;
      ResourcesManagerService.getResources(locationId).then(
        function(response) {
          vm.resources = response;
          vm.isLoading = false;
        },
        function(error){
          ResponseCatcherService.catchError(error);
        });
    }

    function addResource() {
      openModal(
        'modules/manager/services/resources/resource-manager-modal.html',
        'ResourceManagerModalController'
      ).result.then(
        function() {
          getResources(location.id);
        });
    }

    // /Resources

    // Rooms
    function getRooms(locationId) {
      vm.isLoading = true;
      RoomsManagerService.getRooms(locationId).then(
        function(response) {
          vm.rooms = response;
          vm.isLoading = false;
        },
        function(error){
          ResponseCatcherService.catchError(error);
        });
    }

    function addRoom() {
      openModal(
        'modules/manager/services/rooms/room-manager-modal.html',
        'RoomManagerModalController'
      ).result.then(
        function() {
          getRooms(location.id);
        });
    }

    // /Rooms

    // Resource and Room
    function addResourceAndRoom(resourceAndRoom) {
      vm.isLoadingResourcesOrRooms = true;
      if (vm.hasSelectedResourceAndRoom()) {
        ServicesManagerService.addServiceResource(location.id, serviceId, resourceAndRoom).then(
          function(response) {
            ResponseCatcherService.catchSuccess(response).then(
              function(){
                vm.selected.resource = null;
                vm.selected.room = null;
                getService(location.id, serviceId);
              }
            );
          },
          function(error){
            ResponseCatcherService.catchError(error);
          }).finally(function(){
          vm.isLoadingResourcesOrRooms = false;
        });
      }
    }

    function removeResourceAndRoom(resourceAndRoom) {
      vm.isLoadingResourcesOrRooms = true;
      if (vm.service.resourcesRooms.length > 1) {
        ServicesManagerService.removeServiceResource(location.id, serviceId, resourceAndRoom.id).then(
          function(response) {
            ResponseCatcherService.catchSuccess(response).then(
              function(){
                getService(location.id, serviceId);
              }
            )
          },
        function(error){
          ResponseCatcherService.catchError(error);
        }).finally(function(){
          vm.isLoadingResourcesOrRooms = false;
        });
      }
    }

    function isResourceSelected(resource) {
      for (var i = 0; i < vm.service.resourcesRooms.length; i++) {
        if (vm.service.resourcesRooms[i].resource.id === resource.id) {
          return true;
        }
      }
    }

    function hasSelectedResourceAndRoom() {
      return (vm.selected.resource && vm.selected.room);
    }

    // /Resource and Room

    function cancel() {
      $state.go('app.provider.services', { 'providerSlug': vm.provider.slug });
    }

    function openModal(templateUrl, controller, resolve) {
      return $uibModal.open({
        templateUrl: templateUrl,
        controller: controller,
        controllerAs: 'vm',
        size: 'md',
        windowClass: 'stick-up',
        resolve: {
          resolve: function() {
            return resolve;
          }
        }
      });
    }
  }
})();
