﻿; (function() {
  'use strict';

  angular
    .module('filazero-app')
    .controller('ServicesController', ServicesController);

  ServicesController.$inject = [
    '$scope',
    'ServicesManagerService',
    'LocationsManagerService',
    '$uibModal',
    '$q',
    '$state',
    '$stateParams',
    'currentProvider',
    'ResponseCatcherService'
  ];

  function ServicesController($scope, ServicesManagerService, LocationsManagerService, $uibModal, $q, $state, $stateParams, currentProvider, ResponseCatcherService) {
    var vm = this;
    var location;

    vm.currentPage = 1;
    vm.isLoading = true;
    vm.itensLimit = 10;
    vm.order = 'name';
    vm.provider = currentProvider;

    vm.openManagementModal = openManagementModal;
    vm.getServices = getServices;
    vm.addService = addService;
    vm.editService = editService;
    vm.removeService = removeService;
    vm.setOrder = setOrder;

    // Initialization
    var init = function() {
      location = LocationsManagerService.getCurrentLocation();
      if (location) {
        getServices(location.id);
      }
    };

    init();
    // /Initialization

    // Listeners
    $scope.$on('appLocationChanged', function() {
      init();
    });
    // /Listeners

    // Services
    function getServices(locationId) {
      vm.isLoading = true;
      ServicesManagerService.getServices(locationId).then(
        function(response) {
          vm.services = response;
          vm.isLoading = false;
        },
        function(error) {
          ResponseCatcherService.catchError(error, true);
        });
    }

    function addService() {
      $state.go('app.provider.service-add', { 'providerSlug': vm.provider.slug });
    }

    function editService(service) {
      $state.go('app.provider.service-edit', { 'providerSlug': vm.provider.slug, 'serviceId': service.id });
    }

    function removeService(service) {
      var modalInstance = openModal(
        'modules/manager/services/service-remove-modal.html',
        'ServiceRemoveModalController',
        'stick-up',
        service
      );

      modalInstance.result.then(
        function() {
          getServices(location.id);
        }
      );
    }
    // /Services

    function openManagementModal() {
      openModal(
        'modules/manager/services/rooms-resources-modal.html',
        'RoomsResourcesModalController',
        'stick-up'
      );
    }

    function setOrder(order) {
      if (order === vm.order) {
        vm.order = '-' + order;
      } else {
        vm.order = order;
      }
    }

    function openModal(templateUrl, controller, windowClass, resolve) {
      return $uibModal.open({
        templateUrl: templateUrl,
        controller: controller,
        controllerAs: 'vm',
        size: 'lg',
        windowClass: windowClass,
        resolve: {
          resolve: function() {
            return resolve;
          }
        }
      });
    }
  }
})();
