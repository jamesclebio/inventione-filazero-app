;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('ResourcesModalController', ResourcesModalController);

  ResourcesModalController.$inject = [
    'ProviderService',
    'LocationsManagerService',
    'ResourcesManagerService',
    'UserProviderService',
    '$uibModal',
    '$q',
    'ResponseCatcherService'
  ];

  function ResourcesModalController(ProviderService, LocationsManagerService, ResourcesManagerService, UserProviderService, $uibModal, $q, ResponseCatcherService) {
    var vm = this;
    var provider, location;

    vm.currentPage = 1;
    vm.itensLimit = 6;
    vm.order = 'name';

    vm.setOrder = setOrder;
    vm.addResource = addResource;
    vm.editResource = editResource;
    vm.removeResource = removeResource;

    function init() {
      provider = ProviderService.getCurrentProvider();
      location = LocationsManagerService.getCurrentLocation();

      if (location) {
        getResources(location.id);
      }
    }

    // Resources
    function getResources(locationId) {
      vm.isLoading = true;
      ResourcesManagerService.getResources(locationId).then(
        function (response) {
          vm.resources = response;
        },
        function(error){
          ResponseCatcherService.catchError(error,true);
        }
      ).finally(function(){
        vm.isLoading = false;
      });
    }

    function addResource() {
      openModal(
        'modules/manager/services/resources/resource-manager-modal.html',
        'ResourceManagerModalController'
      ).result.then(
        function () {
          getResources(location.id);
        });
    }

    function editResource(resource) {
      var resolve = {
        resourceId: resource.id
      };

      openModal(
        'modules/manager/services/resources/resource-manager-modal.html',
        'ResourceManagerModalController',
        resolve
      ).result.then(
        function () {
          getResources(location.id);
        });
    }

    function removeResource(resource) {
      var modalInstance = openModal(
        'modules/manager/services/resources/resource-remove-modal.html',
        'ResourceRemoveModalController',
        resource
      );

      modalInstance.result.then(
        function () {
          getResources(location.id);
        });
    }

    // /Resources

    function setOrder(order) {
      if (order === vm.order) {
        vm.order = '-' + order;
      } else {
        vm.order = order;
      }
    }

    function openModal(templateUrl, controller, resolve) {
      return $uibModal.open({
        templateUrl: templateUrl,
        controller: controller,
        controllerAs: 'vm',
        size: 'md',
        windowClass: 'stick-up',
        resolve: {
          resolve: function () {
            return resolve;
          }
        }
      });
    }

    init();
  }
})();
