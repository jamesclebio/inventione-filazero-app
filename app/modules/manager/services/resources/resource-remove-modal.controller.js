; (function () {
	'use strict';

	angular
		.module('filazero-app')
		.controller('ResourceRemoveModalController', ResourceRemoveModalController);

	ResourceRemoveModalController.$inject = ['LocationsManagerService', 'ResourcesManagerService', '$uibModalInstance', 'resolve', 'ResponseCatcherService'];

	function ResourceRemoveModalController(LocationsManagerService, ResourcesManagerService, $uibModalInstance, resolve, ResponseCatcherService) {
		var vm = this;

		vm.resource = resolve || null;

		vm.closeModal = closeModal;
		vm.removeResource = removeResource;

		function removeResource() {
      vm.isLoading = true;
			var removeResourceObject = {
				locationId: LocationsManagerService.getCurrentLocation().id,
				resource: vm.resource
			};
			ResourcesManagerService.removeResource(removeResourceObject).then(
				function (response) {
					ResponseCatcherService.catchSuccess(response).then(
            function () {
              $uibModalInstance.close();
            }
          )
				},
				function (error) {
          ResponseCatcherService.catchError(error);
				}).finally(function(){
        vm.isLoading = false;
      });
		}

		function closeModal() {
      $uibModalInstance.dismiss();
    }
	}
})();
