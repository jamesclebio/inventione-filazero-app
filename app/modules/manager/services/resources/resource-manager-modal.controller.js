; (function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('ResourceManagerModalController', ResourceManagerModalController);

  ResourceManagerModalController.$inject = [
    'LocationsManagerService',
    'ResourcesManagerService',
    '$uibModalInstance',
    'UserProviderService',
    'ProviderService',
    'resolve',
    'ResponseCatcherService'
  ];

  function ResourceManagerModalController(LocationsManagerService, ResourcesManagerService, $uibModalInstance, UserProviderService, ProviderService, resolve, ResponseCatcherService) {
    var vm = this;

    vm.closeModal = closeModal;
    vm.disableUser = disableUser;
    vm.isNewModal = isNewModal;
    vm.save = save;

    init();

    function init() {
      var provider = ProviderService.getCurrentProvider();
      vm.resource = {
        staffs: []
      };
      if (!isNewModal()) {
        var location = LocationsManagerService.getCurrentLocation();
        getResource(provider.id, location.id, resolve.resourceId);
      } else {
        getUsers(provider.id);
      }
    }

    function save(resource) {
      if (isNewModal()) {
        addResource(resource);
      } else {
        editResource(resource);
      }
    }

    function getResource(providerId, locationId, resourceId) {
      vm.isLoading = true;
      ResourcesManagerService.getResource(locationId, resourceId).then(
        function (resource) {
          vm.resource = resource;
          getUsers(providerId);
        },
        function (error) {
          ResponseCatcherService.catchError(error);
          $uibModalInstance.dismiss();
        }).finally(
        function () {
          vm.isLoading = false;
        });
    }

    function addResource(resource) {
      vm.isLoading = true;
      var addResourceObject = {
        locationId: LocationsManagerService.getCurrentLocation().id,
        resource: resource
      };
      ResourcesManagerService.addResource(addResourceObject).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response).then(
            function () {
              vm.resource.id = response.messages[0].parameters.resourceId;
              $uibModalInstance.close(vm.resource);
            }
          );
        },
        function () {
          ResponseCatcherService.catchError(error);
        }).finally(function () {
          vm.isLoading = false;
        });
    }

    function editResource(resource) {
      vm.isLoading = true;
      var editResourceObject = {
        locationId: LocationsManagerService.getCurrentLocation().id,
        resource: resource
      };
      ResourcesManagerService.editResource(editResourceObject).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response).then(
            function () {
              $uibModalInstance.close();
            }
          );
        },
        function (error) {
          ResponseCatcherService.catchError(error);
        }).finally(function () {
          vm.isLoading = false;
        });
    }

    function closeModal() {
      $uibModalInstance.dismiss();
    }

    function disableUser(user) {
      for (var j = 0; j < vm.resource.staffs.length; j++) {
        if (user.id === vm.resource.staffs[j].id) {
          return true;
        }
      }
    }

    function isNewModal() {
      return !resolve;
    }

    function getUsers(providerId) {
      vm.isLoading = true;
      return UserProviderService.getUsers(providerId).then(
        function (users) {
          vm.users = users;
        },
        function (error) {
          ResponseCatcherService.catchError(error).then;
          $uibModalInstance.dismiss();
        }).finally(function () {
          vm.isLoading = false;
        });
    }
  }
})();
