angular
  .module('filazero-app')
  .config(['$stateProvider', function ($stateProvider) {
    'use strict';
    $stateProvider
      .state('app.provider.services', {
        url: '/services',
        data: {
          requireAuthentication: true
        },
        templateUrl: 'modules/manager/services/services.html',
        controller: 'ServicesController',
        controllerAs: 'vm',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              'services/manager/resources-manager.service.js',
              'services/manager/services-manager.service.js',
              'services/manager/locations-manager.service.js',
              'services/manager/rooms-manager.service.js',
              'services/user-provider.service.js'
            ], {
              insertBefore: '#lazyload_placeholder'
            }).then(function () {
              return $ocLazyLoad.load([
                'modules/manager/services/services.controller.js',
                'modules/manager/services/service-remove-modal.controller.js',
                'modules/manager/services/rooms-resources-modal.controller.js',
                'modules/manager/services/rooms/rooms-modal.controller.js',
                'modules/manager/services/rooms/room-manager-modal.controller.js',
                'modules/manager/services/rooms/room-remove-modal.controller.js',
                'modules/manager/services/resources/resources-modal.controller.js',
                'modules/manager/services/resources/resource-manager-modal.controller.js',
                'modules/manager/services/resources/resource-remove-modal.controller.js'
              ]);
            });
          }]
        }
      })

      .state('app.provider.service-add', {
        url: '/services/add',
        data: {
          requireAuthentication: true
        },
        templateUrl: 'modules/manager/services/service-manager.html',
        controller: 'ServiceAddController',
        controllerAs: 'vm',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              'ng-tags-input',
              'services/manager/resources-manager.service.js',
              'services/manager/services-manager.service.js',
              'services/manager/locations-manager.service.js',
              'services/manager/rooms-manager.service.js'
            ], {
                insertBefore: '#lazyload_placeholder'
              }).then(function () {
                return $ocLazyLoad.load([
                  'modules/manager/services/service-add.controller.js',
                  'modules/manager/services/rooms/room-manager-modal.controller.js',
                  'modules/manager/services/resources/resource-manager-modal.controller.js'
                ]);
              });
          }]
        }
      })

      .state('app.provider.service-edit', {
        url: '/services/:serviceId/edit',
        data: {
          requireAuthentication: true
        },
        templateUrl: 'modules/manager/services/service-manager.html',
        controller: 'ServiceEditController',
        controllerAs: 'vm',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              'ng-tags-input',
              'services/manager/resources-manager.service.js',
              'services/manager/services-manager.service.js',
              'services/manager/locations-manager.service.js',
              'services/manager/rooms-manager.service.js'
            ], {
                insertBefore: '#lazyload_placeholder'
              }).then(function () {
                return $ocLazyLoad.load([
                  'modules/manager/services/service-edit.controller.js',
                  'modules/manager/services/rooms/room-manager-modal.controller.js',
                  'modules/manager/services/resources/resource-manager-modal.controller.js'
                ]);
              });
          }]
        }
      });
  }]);
