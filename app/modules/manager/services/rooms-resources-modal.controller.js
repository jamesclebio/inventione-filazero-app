; (function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('RoomsResourcesModalController', RoomsResourcesModalController);

  RoomsResourcesModalController.$inject = ['$state', '$uibModalInstance'];

  function RoomsResourcesModalController($state, $uibModalInstance) {
    var vm = this;

    vm.isCurrentTab = isCurrentTab;
    vm.setCurrentTab = setCurrentTab;
    vm.closeModal = closeModal;

    var currentTab = 'tabRooms';

    function isCurrentTab(tabName) {
      return currentTab === tabName;
    }

    function setCurrentTab(tabName) {
      currentTab = tabName;
    }

    function closeModal() {
      $uibModalInstance.dismiss();
    }
  }
})();
