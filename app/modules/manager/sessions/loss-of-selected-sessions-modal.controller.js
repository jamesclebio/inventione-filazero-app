(function() {
'use strict';

  angular
    .module('filazero-app')
    .controller('LossOfSelectedSessionsModalController', LossOfSelectedSessionsModalController);

  LossOfSelectedSessionsModalController.$inject = [
    '$uibModalInstance',
    'resolve'
  ];

  function LossOfSelectedSessionsModalController($uibModalInstance, resolve) {
    var vm = this;

    vm.selectedSessionsQuantity = resolve.selectedSessionsQuantity;

    vm.cancel = cancel;
    vm.close = close;

    function cancel() {
      $uibModalInstance.dismiss();
    }

    function close() {
      $uibModalInstance.close();
    }
  }
})();
