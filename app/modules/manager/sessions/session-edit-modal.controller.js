; (function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('SessionEditModalController', SessionEditModalController);

  SessionEditModalController.$inject = ['$uibModalInstance', 'resolve', 'SessionsManagerService', 'ResponseCatcherService'];

  function SessionEditModalController($uibModalInstance, resolve, SessionsManagerService, ResponseCatcherService) {
    var vm = this;

    vm.isLoading = false;

    vm.cancel = cancel;
    vm.invalidTime = invalidTime;
    vm.confirm = confirm;

    init();

    function cancel() {
      $uibModalInstance.dismiss();
    }

    function init() {
      vm.session = angular.copy(resolve.session);
      if (vm.session) {
        vm.session.duration = new Date(0, 0, 0, 0, vm.session.duration, 0, 0);
      }
      vm.sessionsQuantity = resolve.sessionsQuantity;
    }

    function invalidTime(time) {
      return time === undefined || time === null;
    }

    function confirm(session) {
      if (!vm.sessionsQuantity) {
        editSession(vm.session.id);
      } else {
        bulkEditSessions(resolve);
      }
    }

    function editSession(sessions) {
      var editSessionObject = {
        session: {}
      };
      editSessionObject.session = $.extend(true, {}, vm.session);
      editSessionObject.session.duration = editSessionObject.session.duration.getHours() + ":" + editSessionObject.session.duration.getMinutes();
      SessionsManagerService.editSession(editSessionObject).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response).then(
            function () {
              $uibModalInstance.close();
            }
          )
        },
        function (error) {
          ResponseCatcherService.catchError(error);
        }
      );
    }

    function bulkEditSessions(params) {
      params.session = {};
      params.session.estimatedStart = vm.session.estimatedStart;
      params.session.duration = vm.session.duration.getHours() + ":" + vm.session.duration.getMinutes();
      params.session.capacity = vm.session.capacity;

      SessionsManagerService.bulkEditSessions(params).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response).then(
            function () {
              $uibModalInstance.close();
            }
          )
        },
        function (error) {
          ResponseCatcherService.catchError(error);
        }
      );
    }
  }
})();
