; (function () {
  'use strict';

  angular
    .module('filazero-app')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider.state('app.provider.sessions', {
      url: '/sessions?serviceId&resourceId&roomId&serviceConfigs&startDate&endDate',
      data: {
        requireAuthentication: true
      },
      templateUrl: 'modules/manager/sessions/sessions.html',
      controller: 'SessionsController',
      controllerAs: 'vm',
      resolve: {
        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            'services/manager/services-manager.service.js',
            'services/manager/resources-manager.service.js',
            'services/manager/rooms-manager.service.js',
            'services/manager/sessions-manager.service.js',
            'services/manager/locations-manager.service.js',
            'services/service-config.service.js',
            'datepicker'
          ], {
              insertBefore: '#lazyload_placeholder'
            }).then(function () {
              return $ocLazyLoad.load([
                'modules/manager/sessions/sessions.controller.js',
                'modules/manager/sessions/session-edit-modal.controller.js',
                'modules/manager/sessions/session-remove-modal.controller.js',
                'modules/manager/sessions/loss-of-selected-sessions-modal.controller.js',
                'modules/service-config/service-config-add-modal.controller.js',
                'modules/service-config/service-config-remove-modal.controller.js'
              ]);
            });
        }]
      }
    });
  }
})();
