; (function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('SessionRemoveModalController', SessionRemoveModalController);

  SessionRemoveModalController.$inject = ['$uibModalInstance', 'resolve', 'SessionsManagerService', 'ResponseCatcherService'];

  function SessionRemoveModalController($uibModalInstance, resolve, SessionsManagerService, ResponseCatcherService) {
    var vm = this;

    vm.isLoading = false;

    vm.cancel = cancel;
    vm.confirm = confirm;

    init();

    function init() {
      vm.session = resolve.session;
      vm.sessionsQuantity = resolve.sessionsQuantity;
    }

    function cancel() {
      $uibModalInstance.dismiss();
    }

    function confirm() {
      if (vm.session) {
        removeSession(vm.session.id);
      } else {
        bulkRemoveSessions(resolve);
      }
    }

    function removeSession(sessionId) {
      vm.isLoading = true;
      SessionsManagerService.removeSession(sessionId).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response).then(
            function () {
              $uibModalInstance.close();
            }
          )
        },
        function (error) {
          ResponseCatcherService.catchError(error);
        }
      ).finally(function () {
        vm.isLoading = false;
      });
    }

    function bulkRemoveSessions(params) {
      vm.isLoading = true;
      SessionsManagerService.bulkRemoveSessions(params).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response).then(
            function () {
              $uibModalInstance.close();
            }
          )
        },
        function (error) {
          ResponseCatcherService.catchError(error);
        }
      ).finally(function () {
        vm.isLoading = false;
      });
    }
  }
})();
