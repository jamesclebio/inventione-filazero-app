;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('SessionsController', SessionsController);

  SessionsController.$inject = [
    '$scope',
    '$uibModal',
    '$stateParams',
    '$q',
    'LocationsManagerService',
    'SessionsManagerService',
    'ServicesManagerService',
    'ResourcesManagerService',
    'RoomsManagerService',
    'ServiceConfigService',
    'currentProvider',
    'ResponseCatcherService',
    '$state'
  ];

  function SessionsController($scope, $uibModal, $stateParams, $q, LocationsManagerService, SessionsManagerService, ServicesManagerService, ResourcesManagerService, RoomsManagerService, ServiceConfigService, currentProvider, ResponseCatcherService, $state) {
    var vm = this;

    vm.location = {};
    vm.provider = {};

    vm.currentPage = 1;
    vm.sessionsLimit = 10;
    vm.selectedSessionsQuantity = 0;

    vm.dateGroups = [];
    vm.sessions = [];
    vm.services = [];
    vm.serviceConfigs = [];
    vm.serviceConfigsFilter = [];
    vm.rooms = [];

    vm.searchSessions = searchSessions;
    vm.getSessions = getSessions;
    vm.editSession = editSession;
    vm.bulkEditSessions = bulkEditSessions;
    vm.removeSession = removeSession;
    vm.bulkRemoveSessions = bulkRemoveSessions;
    vm.changeAllFiltersVisibility = changeAllFiltersVisibility;

    vm.openStartDatepicker = openStartDatepicker;
    vm.openEndDatepicker = openEndDatepicker;

    vm.applyServiceConfigFilter = applyServiceConfigFilter;
    vm.addServiceConfig = addServiceConfig;
    vm.deselectAllServiceConfigs = deselectAllServiceConfigs;
    vm.removeServiceConfig = removeServiceConfig;

    vm.checkAllDaysOfWeek = checkAllDaysOfWeek;
    vm.selectAllVisibleSessions = selectAllVisibleSessions;
    vm.selectAllSessions = selectAllSessions;
    vm.deselectAllSessions = deselectAllSessions;
    vm.checkSession = checkSession;

    // Initialization
    init();

    function init() {
      vm.provider = currentProvider;
      vm.location = LocationsManagerService.getCurrentLocation();

      vm.selectedService = $stateParams.serviceId;
      vm.selectedResource = $stateParams.resourceId;
      vm.selectedRoom = $stateParams.roomId;

      initServiceConfigsFilters($stateParams.serviceConfigs);

      vm.isLoadingSessions = true;

      if (vm.selectedResource || vm.selectedRoom) {
        changeAllFiltersVisibility(true);
      }

      initDatepicker();

      if (vm.location) {
        getServices(vm.location.id);
        getResources(vm.location.id);
        getRooms(vm.location.id);
        getSessions(vm.location.id);
        getServiceConfigs(vm.location.id);
      }
    }

    function initDatepicker() {
      if ($stateParams.startDate) {
        vm.startDate = new Date($stateParams.startDate);
      } else {
        vm.startDate = new Date();
      }

      if ($stateParams.endDate && new Date($stateParams.endDate) > vm.startDate) {
        vm.endDate = new Date($stateParams.endDate);
      } else {
        vm.endDate = new Date(new Date(vm.startDate).setDate(vm.startDate.getDate() + 30));
      }
    }

    // /Initialization

    // Listeners
    $scope.$on('appLocationChanged',
      function () {
        init();
      }
    );

    $scope.$watch('vm.currentPage',
      function (newValue) {
        if (newValue && vm.location) {
          getSessions(vm.location.id);
        }
      }
    );

    $scope.$watch('vm.sessionsLimit',
      function (newValue) {
        if (newValue && vm.location) {
          vm.currentPage = 1;
          getSessions(vm.location.id);
        }
      }
    );
    // /Listeners

    // Sessions
    function searchSessions(locationId) {
      vm.currentPage = 1;

      if (vm.selectedSessionsQuantity) {
        var modalInstance = openLossOfSelectedSessionsModal();

        modalInstance.result.then(
          function () {
            vm.getSessions(locationId);
          }
        );
      } else {
        getSessions(locationId);
      }
    }

    function getSessions(locationId) {
      vm.isLoadingSessions = true;
      vm.selectedSessionsQuantity = 0;
      vm.allSessionsSelected = false;
      vm.allVisibleSessionsSelected = false;

      $state.transitionTo('app.provider.sessions', {
        providerSlug: $stateParams.providerSlug,
        serviceId: vm.selectedService,
        startDate: vm.startDate.toJSON(),
        endDate: vm.endDate.toJSON(),
        resourceId: vm.selectedResource,
        roomId: vm.selectedRoom,
        serviceConfigs: vm.serviceConfigsFilter
      }, {notify: false});

      var filter = {
        limit: vm.sessionsLimit,
        offset: (vm.currentPage - 1) * vm.sessionsLimit,
        serviceId: vm.selectedService,
        startDate: vm.startDate,
        endDate: vm.endDate,
        serviceConfigs: vm.serviceConfigsFilter
      };

      if (vm.showAllFilters) {
        filter.resourceId = vm.selectedResource;
        filter.roomId = vm.selectedRoom;
      }

      SessionsManagerService.getSessions(locationId, filter).then(
        function (response) {
          if(response.sessions){
            vm.sessions = _.map(response.sessions.items, function (item) {
              item.serviceConfig = _.find(response.serviceConfigs, function (obj) {
                return obj.id === item.serviceConfigId;
              });
              return item;
            });
          }
          vm.totalSessions = response.sessions.totalItems;
          if (vm.selectedEndDate || vm.selectedStartDate) {
            vm.selectedEndDate = '';
            vm.selectedStartDate = '';
          }
          vm.isFiltered = false;
          vm.isLoadingSessions = false;

          vm.dateGroups = groupSessionsByDates(vm.sessions);
        },
        function (error) {
          ResponseCatcherService.catchError(error, true);
        }
      );
    }

    function groupSessionsByDates(sessions) {
      var j = 0;
      var added = false;
      var dates = [];
      var dateGroup = {
        date: '',
        sessions: []
      };

      for (var i = 0; i < sessions.length; i++) {
        sessions[i].estimatedStartTime = new Date(sessions[i].estimatedStartTime);
        dateGroup.date = new Date(
          sessions[i].estimatedStartTime.getFullYear(),
          sessions[i].estimatedStartTime.getMonth(),
          sessions[i].estimatedStartTime.getDate());
        dateGroup.sessions = [];

        added = false;

        for (j = 0; j < dates.length; j++) {
          if (dateGroup.date.getTime() === dates[j].date.getTime()) {
            dates[j].sessions.push(sessions[i]);
            added = true;
            break;
          }
        }
        if (!added) {
          dateGroup.sessions.push(sessions[i]);
          dates.push(angular.copy(dateGroup));
        }
      }
      return dates;
    }

    function editSession(session) {
      var resolve = {
        session: session
      };

      var templateUrl = 'modules/manager/sessions/session-edit-modal.html';
      var controller = 'SessionEditModalController';

      var modalInstance = openModal(templateUrl, controller, resolve, 'md');

      modalInstance.result.then(
        function () {
          vm.getSessions(vm.location.id);
        }
      );
    }

    function bulkEditSessions() {
      if (vm.selectedSessionsQuantity > 1) {
        var selectedSessions = [];

        if (!vm.allSessionsSelected) {
          for (var i = 0; i < vm.sessions.length; i++) {
            if (vm.sessions[i].isSelected) {
              selectedSessions.push(vm.sessions[i].id);
            }
          }
        }

        var resolve = {
          sessionsQuantity: vm.selectedSessionsQuantity,
          allSessions: vm.allSessionsSelected,
          sessions: selectedSessions
        };

        if (vm.allSessionsSelected) {
          resolve.serviceId = vm.selectedService;
          resolve.resourceId = vm.selectedResource;
          resolve.roomId = vm.selectedRoom;
          resolve.startDate = vm.startDate.toJSON();
          resolve.endDate = vm.endDate.toJSON()
        }

        var templateUrl = 'modules/manager/sessions/session-edit-modal.html';
        var controller = 'SessionEditModalController';

        var modalInstance = openModal(templateUrl, controller, resolve, 'md');

        modalInstance.result.then(
          function () {
            vm.getSessions(vm.location.id);
          }
        );
      }
    }

    function removeSession(session) {
      var resolve = {
        session: session
      };

      var templateUrl = 'modules/manager/sessions/session-remove-modal.html';
      var controller = 'SessionRemoveModalController';

      var modalInstance = openModal(templateUrl, controller, resolve, 'md');

      modalInstance.result.then(
        function () {
          vm.getSessions(vm.location.id);
        }
      );
    }

    function bulkRemoveSessions() {
      if (vm.selectedSessionsQuantity > 1) {
        var selectedSessions = [];

        if (!vm.allSessionsSelected) {
          for (var i = 0; i < vm.sessions.length; i++) {
            if (vm.sessions[i].isSelected) {
              selectedSessions.push(vm.sessions[i].id);
            }
          }
        }

        var resolve = {
          sessionsQuantity: vm.selectedSessionsQuantity,
          allSessions: vm.allSessionsSelected,
          sessions: selectedSessions
        };

        if (vm.allSessionsSelected) {
          resolve.serviceId = vm.selectedService;
          resolve.resourceId = vm.selectedResource;
          resolve.roomId = vm.selectedRoom;
          resolve.startDate = vm.startDate.toJSON();
          resolve.endDate = vm.endDate.toJSON()
        }

        var templateUrl = 'modules/manager/sessions/session-remove-modal.html';
        var controller = 'SessionRemoveModalController';

        var modalInstance = openModal(templateUrl, controller, resolve, 'md');

        modalInstance.result.then(
          function () {
            vm.getSessions(vm.location.id);
          }
        );
      }
    }

    function selectAllVisibleSessions(selected) {
      vm.allVisibleSessionsSelected = selected || vm.allVisibleSessionsSelected;

      if (vm.allVisibleSessionsSelected) {
        vm.selectedSessionsQuantity = vm.sessionsLimit;
      } else {
        vm.allSessionsSelected = false;
        vm.selectedSessionsQuantity = 0;
      }

      for (var i = 0; i < vm.sessions.length; i++) {
        vm.sessions[i].isSelected = vm.allVisibleSessionsSelected;
      }
    }

    function selectAllSessions() {
      selectAllVisibleSessions(true);
      vm.selectedSessionsQuantity = vm.totalSessions;
      vm.allSessionsSelected = true;
    }

    function deselectAllSessions() {
      vm.selectedSessionsQuantity = 0;
      vm.allSessionsSelected = false;
      vm.allVisibleSessionsSelected = false;

      for (var i = 0; i < vm.sessions.length; i++) {
        vm.sessions[i].isSelected = false;
      }
    }

    function checkSession(session) {
      if (session.isSelected) {
        selectSession(session);
      } else {
        deselectSession(session);
      }
    }

    function selectSession(session) {
      vm.selectedSessionsQuantity += 1;
      for (var i = 0; i < vm.sessions.length; i++) {
        if (!vm.sessions[i].isSelected) {
          return;
        }
      }
      vm.allVisibleSessionsSelected = true;
    }

    function deselectSession(session) {
      if (vm.allSessionsSelected) {
        vm.allSessionsSelected = false;
        vm.selectedSessionsQuantity = vm.sessionsLimit - 1;
      } else {
        vm.selectedSessionsQuantity -= 1;
      }
      vm.allVisibleSessionsSelected = false;
    }

    // /Sessions

    // Filters
    function getServices(locationId) {
      var deferred = $q.defer();

      ServicesManagerService.getServices(locationId).then(
        function (services) {
          vm.services = services;
          deferred.resolve(services);
        }, function (error) {
          ResponseCatcherService.catchError(error, true);
          deferred.reject(error);
        }
      );

      return deferred.promise;
    }

    function getResources(locationId) {
      var deferred = $q.defer();

      ResourcesManagerService.getResources(locationId).then(
        function (resources) {
          vm.resources = resources;
          deferred.resolve(resources);
        }, function (error) {
          ResponseCatcherService.catchError(error, true);
          deferred.reject(error);
        }
      );

      return deferred.promise;
    }

    function getRooms(locationId) {
      var deferred = $q.defer();

      RoomsManagerService.getRooms(locationId).then(
        function (rooms) {
          vm.rooms = rooms;
          deferred.resolve(rooms);
        }, function (error) {
          ResponseCatcherService.catchError(error, true);
          deferred.reject(error);
        }
      );

      return deferred.promise;
    }

    function deselectAllServiceConfigs() {
      for (var i = 0; i < vm.serviceConfigs.length; i++) {
        vm.serviceConfigs[i].isSelected = false;
      }
      vm.serviceConfigsFilter = [];
      vm.getSessions(vm.location.id);
    }

    function initServiceConfigsFilters(serviceConfigs) {
      if (serviceConfigs) {
        if ((typeof serviceConfigs) !== 'string') {
          for (var i = 0; i < serviceConfigs.length; i++) {
            serviceConfigs[i] = parseInt(serviceConfigs[i]);
          }
          vm.serviceConfigsFilter = serviceConfigs;
        } else {
          serviceConfigs = parseInt(serviceConfigs);
          vm.serviceConfigsFilter = [];
          vm.serviceConfigsFilter.push(serviceConfigs);
        }
      }
    }

    function getServiceConfigs(locationId) {
      var deferred = $q.defer();

      vm.isLoadingServices = true;

      ServiceConfigService.getServiceConfigs(locationId).then(
        function (serviceConfigs) {
          vm.serviceConfigs = serviceConfigs;
          for (var i = 0; i < vm.serviceConfigs.length; i++) {
            if (vm.serviceConfigsFilter.indexOf(vm.serviceConfigs[i].id) !== -1) {
              vm.serviceConfigs[i].isSelected = true;
            }
          }
          deferred.resolve(serviceConfigs);
        }, function (error) {
          ResponseCatcherService.catchError(error, true);
          deferred.reject(error);
        }
      ).finally(function () {
        vm.isLoadingServices = false;
      });

      deferred.resolve(vm.serviceConfigs);

      return deferred.promise;
    }

    function removeServiceConfig(serviceConfig) {
      var templateURL = 'modules/service-config/service-config-remove-modal.html';
      var controller = 'ServiceConfigRemoveModalController';

      openModal(templateURL, controller, serviceConfig, 'md');
    }

    function applyServiceConfigFilter(serviceConfig) {
      for (var i = 0; i < vm.serviceConfigs.length; i++) {
        if (vm.serviceConfigs[i].id === serviceConfig.id) {
          if (serviceConfig.isSelected) {
            vm.serviceConfigsFilter.push(serviceConfig.id);
            break;
          } else {
            vm.serviceConfigsFilter.splice(vm.serviceConfigsFilter.indexOf(serviceConfig.id), 1);
            break;
          }
        }
      }
      vm.getSessions(vm.location.id);
    }

    function addServiceConfig() {
      var templateUrl = 'modules/service-config/service-config-add-modal.html';
      var controller = 'ServiceConfigAddModalController';

      openModal(templateUrl, controller, {}, 'md');
    }

    function checkAllDaysOfWeek() {
      for (var i = 0; i < vm.daysOfWeek.length; i++) {
        vm.daysOfWeek[i].isSelected = true;
      }
    }

    function changeAllFiltersVisibility(showAllFilters) {
      vm.showAllFilters = showAllFilters;
    }

    // /Filters

    // Datepickers
    function openStartDatepicker() {
      vm.startDateOpened = true;
    }

    function openEndDatepicker() {
      vm.endDateOpened = true;
    }

    // /Datepickers

    function openLossOfSelectedSessionsModal() {
      var resolve = {
        selectedSessionsQuantity: vm.selectedSessionsQuantity
      };

      var templateUrl = 'modules/manager/sessions/loss-of-selected-sessions-modal.html';
      var controller = 'LossOfSelectedSessionsModalController';

      return openModal(templateUrl, controller, resolve, 'md');
    }

    function openModal(templateUrl, controller, resolve, size) {
      return $uibModal.open({
        templateUrl: templateUrl,
        controller: controller,
        controllerAs: 'vm',
        size: size || 'md',
        windowClass: 'stick-up',
        resolve: {
          resolve: function () {
            return resolve;
          }
        }
      });
    }
  }
})();
