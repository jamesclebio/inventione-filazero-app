; (function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('PanelsController', PanelsController);

  PanelsController.$inject = [
    '$scope',
    'PanelsManagerService',
    '$uibModal',
    'LocationsManagerService',
    '$state',
    'currentProvider',
    'ResponseCatcherService'
  ];

  function PanelsController($scope, PanelsManagerService, $uibModal, LocationsManagerService, $state, currentProvider, ResponseCatcherService) {
    var vm = this;

    vm.isLoading = true;
    vm.currentPage = 1;
    vm.itensLimit = 10;
    vm.location = {};
    vm.order = 'name';
    vm.provider = currentProvider;

    vm.filteredPanels = [];
    vm.panelsList = [];

    vm.addPanel = addPanel;
    vm.editPanel = editPanel;
    vm.getPanels = getPanels;
    vm.openModal = openModal;
    vm.removePanel = removePanel;
    vm.setOrder = setOrder;
    vm.viewPanel = viewPanel;

    // Initialization
    $scope.$on('appLocationChanged', function () {
      init();
    });

    function init() {
      vm.location = LocationsManagerService.getCurrentLocation();

      if (vm.location) {
        vm.getPanels(vm.location.id);
      }
    }

    init();
    // /Initialization

    // Panels
    function getPanels(locationId) {
      vm.isLoading = true;
      PanelsManagerService.getPanels(locationId).then(
        function (panels) {
          vm.panelsList = panels;
          vm.isLoading = false;
        },
        function(error) {
          ResponseCatcherService.catchError(error, true);
        });
    }

    function addPanel() {
      var templateUrl = 'modules/manager/panels/panel-manager-modal.html';
      var controller = 'PanelManagerModalController';
      var addPanelObject = {
        locationId: vm.location.id
      };
      var size = 'md';
      vm.openModal(templateUrl, controller, addPanelObject, size);
    }

    function editPanel(panelId) {
      var editPanelObject = {
        locationId: vm.location.id,
        panelId: panelId,
        providerId: vm.provider.id
      };

      var templateUrl = 'modules/manager/panels/panel-manager-modal.html';
      var controller = 'PanelManagerModalController';

      vm.openModal(templateUrl, controller, editPanelObject, 'md');
    }

    function removePanel(panel) {
      var removePanelObject = {
        locationId: vm.location.id,
        panel: panel
      };
      var templateUrl = 'modules/manager/panels/panel-remove-modal.html';
      var controller = 'PanelRemoveModalController';

      vm.openModal(templateUrl, controller, removePanelObject, 'md');
    }

    function viewPanel(panel) {
      var url = '';
      if (panel.type === "basic") {
        url = $state.href('panelBasic', { providerSlug: vm.provider.slug, panelId: panel.id });
      } else {
        url = $state.href('panelFull', { providerSlug: vm.provider.slug, panelId: panel.id });
      }
      return url;
    }
    // /Panels

    function openModal(templateUrl, controller, resolve, size) {
      var modalInstance = $uibModal.open({
        templateUrl: templateUrl,
        controller: controller,
        controllerAs: 'vm',
        size: size,
        windowClass: 'stick-up',
        resolve: {
          resolve: function () {
            return resolve;
          }
        }
      });

      modalInstance.result.then(
        function () {
          vm.getPanels(vm.location.id);
        }
      );
    }

    function setOrder(order) {
      if (order === vm.order) {
        vm.order = '-' + order;
      } else {
        vm.order = order;
      }
    }
  }
})();
