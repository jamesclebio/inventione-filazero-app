;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider.state('app.provider.panels', {
      url: '/panels',
      data:{
        requireAuthentication: true
      },
      templateUrl: 'modules/manager/panels/panels.html',
      controller: 'PanelsController',
      controllerAs: 'vm',
      resolve: {
        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            'services/manager/panels-manager.service.js',
            'services/manager/services-manager.service.js',
            'services/manager/locations-manager.service.js',
            'services/service-config.service.js'
          ], {
            insertBefore: '#lazyload_placeholder'
          }).then(function () {
            return $ocLazyLoad.load([
              'modules/manager/panels/panels.controller.js',
              'modules/manager/panels/panel-manager-modal.controller.js',
              'modules/manager/panels/panel-remove-modal.controller.js'
            ]);
          });
        }]
      }
    });
  }
}());
