; (function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('PanelManagerModalController', PanelManagerModalController);

  PanelManagerModalController.$inject = [
    'resolve',
    '$uibModalInstance',
    'PanelsManagerService',
    'ServiceConfigService',
    '$q',
    'ResponseCatcherService'
  ];

  function PanelManagerModalController(resolve, $uibModalInstance, PanelsManagerService, ServiceConfigService, $q, ResponseCatcherService) {

    var vm = this;
    vm.panel = {};
    vm.serviceConfigs = [];
    vm.isMultipleServiceConfig = false;

    vm.cancel = cancel;
    vm.isEditing = isEditing;
    vm.save = save;
    vm.serviceAlreadySelected = serviceAlreadySelected;
    vm.serviceSelected = serviceSelected;

    // Initialization
    function init() {
      vm.isLoading = true;
      getAllServiceConfigs(resolve.locationId).then(
        function (services) {
          vm.serviceConfigs = services;
          if (vm.isEditing()) {
            getPanel(resolve.panelId).then(
              function (panel) {
                vm.panel = panel;
                vm.isMultipleServiceConfig = vm.panel.serviceConfigs.length > 1;
                vm.isLoading = false;
              }
            );
          } else {
            vm.isLoading = false;
            vm.panel = {
              name: '',
              serviceConfigs: [],
              transitionTime: 10,
              showCustomerOnListing : true,
              showCustomerOnCall : true,
              showServiceOnListing : true,
              showServiceOnCall : true,
              showRoomOnListing : true,
              showRoomOnCall : true,
              showAttendantOnListing : true,
              showAttendantOnCall : true
            }
          }
        }
      );
    }

    init();
    // /Initialization

    function getPanel(panelId) {
      var deferred = $q.defer();

      PanelsManagerService.getPanel(resolve.providerId, panelId).then(
        function (panel) {
          deferred.resolve(panel);
        },
        function (error) {
          ResponseCatcherService.catchError(error);
          $uibModalInstance.dismiss();
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function getAllServiceConfigs(locationId) {
      var deferred = $q.defer();

      ServiceConfigService.getServiceConfigs(locationId).then(
        function (configs) {
          deferred.resolve(configs);
        },
        function (error) {
          ResponseCatcherService.catchError(error);
          $uibModalInstance.dismiss();
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function save() {
      vm.isLoading = true;

      var panelObject = {
        panel: {},
        locationId: ''
      };
      panelObject.locationId = resolve.locationId;
      panelObject.panel = jQuery.extend(true, {}, vm.panel);

      for (var i = 0; i < panelObject.panel.serviceConfigs.length; i++) {
        panelObject.panel.serviceConfigs[i] = panelObject.panel.serviceConfigs[i].id;
      }

      if (vm.isEditing()) {
        PanelsManagerService.editPanel(panelObject).then(
          function (response) {
            ResponseCatcherService.catchSuccess(response).then(
              function (messages) {
                $uibModalInstance.close();
              }
            );
          },
          function (error) {
            ResponseCatcherService.catchError(error);
          }).finally(
          function () {
            vm.isLoading = false;
          });
      } else {
        PanelsManagerService.addPanel(panelObject).then(
          function (response) {
            ResponseCatcherService.catchSuccess(response).then(
              function (messages) {
                $uibModalInstance.close();
              }
            );
          },
          function (error) {
            ResponseCatcherService.catchError(error);
          }).finally(
          function () {
            vm.isLoading = false;
          });
      }
    }

    function isEditing() {
      return resolve.panelId;
    }

    function serviceAlreadySelected(service) {
      for (var i = 0; i < vm.panel.serviceConfigs.length; i++) {
        if (vm.panel.serviceConfigs[i].id === service.id) {
          return true;
        }
      }
      return false;
    }

    function serviceSelected() {
      vm.isMultipleServiceConfig = vm.panel.serviceConfigs.length > 1
    }

    function cancel() {
      $uibModalInstance.dismiss();
    }
  }
})();
