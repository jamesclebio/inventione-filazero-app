; (function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('PanelRemoveModalController', PanelRemoveModalController);

  PanelRemoveModalController.$inject = [
    '$uibModalInstance',
    'PanelsManagerService',
    'resolve',
    'ResponseCatcherService'
  ];

  function PanelRemoveModalController($uibModalInstance, PanelsManagerService, resolve, ResponseCatcherService) {

    var vm = this;

    vm.panel = resolve.panel;
    vm.locationId = resolve.locationId;

    vm.cancel = cancel;
    vm.confirm = confirm;

    function confirm() {
      vm.isLoading = true;
      PanelsManagerService.removePanel(vm.locationId, vm.panel.id).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response).then(
            function (messages) {
              $uibModalInstance.close();
            }
          );
        },
        function (error) {
          ResponseCatcherService.catchError(error);
        }
      ).finally(function(){
        vm.isLoading = false;
      });
    }

    function cancel() {
      $uibModalInstance.dismiss();
    }
  }
})();


