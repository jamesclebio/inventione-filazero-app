;(function(){
  'use strict';

  angular
    .module('filazero-app')
    .controller('SessionModelSelectModalController', SessionModelSelectModalController);

  SessionModelSelectModalController.$inject = ['resolve', '$uibModalInstance', '$uibModal', 'SessionGeneratorManagerService', 'ResponseCatcherService'];

  function SessionModelSelectModalController(resolve, $uibModalInstance, $uibModal, SessionGeneratorManagerService, ResponseCatcherService){

    var vm = this;

    vm.currentPage = 1;
    vm.isLoading = false;
    vm.itensLimit = 5;
    vm.maskOptions = {
      maskDefinitions:{'5': /[0-5]/},
      clearOnBlur: false
    };
    vm.order = '';
    vm.searchSessionModel = '';
    vm.sessionModel = {
      description: '',
      start: '',
      duration: '',
      slots: ''
    };

    vm.filteredSessionModels = [];
    vm.selectedSessionModels = [];
    vm.sessionModels = [];

    vm.addSessionModel = addSessionModel;
    vm.getSessionModels = getSessionModels;
    vm.init = init;
    vm.removeSessionModel = removeSessionModel;
    vm.sessionModelSelectionChanged = sessionModelSelectionChanged;
    vm.setOrder = setOrder;
    vm.useSelected = useSelected;
    vm.close = close;

    vm.init();

    function addSessionModel(sessionModel){
      var newSessionModel = $.extend(true, {}, sessionModel);
      newSessionModel.start = newSessionModel.start.getHours() + ':' + newSessionModel.start.getMinutes();
      newSessionModel.duration = newSessionModel.duration.slice(0,2) + ':' + newSessionModel.duration.slice(2,4);

      vm.isLoading = true;
      SessionGeneratorManagerService.addSessionModel(newSessionModel, resolve.providerId).then(
        function(response){
          ResponseCatcherService.catchSuccess(response).then(
            function(){
              vm.sessionModel = {};
              vm.getSessionModels(resolve.providerId);
            }
          )
        },function(error){
          ResponseCatcherService.catchError(error);
        })
    }

    function getSessionModels(providerId) {
      vm.isLoading = true;
      return SessionGeneratorManagerService.getSessionModels(providerId).then(
        function (sessionModels) {
          for(var i = 0; i < sessionModels.length; i++){
            for(var j = 0; j < vm.selectedSessionModels.length; j++){
              if(sessionModels[i].id === vm.selectedSessionModels[j].id){
                sessionModels[i].isSelected = true;
              }
            }
          }
          vm.sessionModels = sessionModels;
          vm.isLoading = false;
          return vm.sessionModels;
        },
        function(error){
          ResponseCatcherService.catchError(error);
        }
      )
    }

    function init(){
      vm.order = 'description';
      vm.sessionModels = resolve.sessionModels;
      vm.selectedSessionModels = $.extend(true, [], resolve.selectedSessionModels );
      for(var i = 0; i < vm.sessionModels.length; i++){
        for(var j = 0; j < vm.selectedSessionModels.length; j++){
          if(vm.sessionModels[i].id === vm.selectedSessionModels[j].id){
            vm.sessionModels[i].isSelected = true;
          }
        }
      }
    }

    function removeSessionModel(sessionModel){
      var resolveRemove = {
        sessionModel: sessionModel
      };
      $uibModal.open({
        templateUrl: 'modules/manager/sessionsGenerator/session-model-remove-modal.html',
        controller: 'SessionModelRemoveModalController',
        controllerAs: 'vm',
        size: 'md',
        windowClass: 'stick-up',
        resolve: {
          resolve: function () {
            return resolveRemove;
          }
        }
      }).result.then(
        function(){
          vm.getSessionModels(resolve.providerId);
        }
      );
    }

    function sessionModelSelectionChanged(isSelected, id){
      if(isSelected){
        for(var i = 0; i < vm.filteredSessionModels.length; i++){
          if(vm.filteredSessionModels[i].id == id){
            vm.selectedSessionModels.push(vm.filteredSessionModels[i]);
          }
        }
      }else{
        for(i = 0; i < vm.selectedSessionModels.length; i++){
          if(vm.selectedSessionModels[i].id === id){
            vm.selectedSessionModels.splice(i, 1);
            break;
          }
        }
      }
    }

    function setOrder(order) {
      if (order === vm.order) {
        vm.order = '-' + order;
      } else {
        vm.order = order;
      }
    }

    function useSelected() {
      $uibModalInstance.close(vm.selectedSessionModels);
    }

    function close() {
      $uibModalInstance.close();
    }
  }
})();
