;(function(){
  'use strict';

  angular
    .module('filazero-app')
    .controller('SessionModelRemoveModalController', SessionModelRemoveModalController);

  SessionModelRemoveModalController.$inject = ['$uibModalInstance', 'SessionGeneratorManagerService', 'resolve', 'ResponseCatcherService'];

  function SessionModelRemoveModalController($uibModalInstance, SessionGeneratorManagerService, resolve, ResponseCatcherService){

    var vm = this;

    vm.sessionModel = {
      id: resolve.sessionModel.id,
      name: resolve.sessionModel.description
    };

    vm.cancel = cancel;
    vm.confirm = confirm;

    function cancel(){
      $uibModalInstance.dismiss('cancel');
    }

    function confirm(){
      vm.isLoading = true;
      SessionGeneratorManagerService.removeSessionModel(vm.sessionModel.id).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response).then(
            function(){
              $uibModalInstance.close();
            }
          )
        },
        function (error) {
          vm.isLoading = false;
          ResponseCatcherService.catchError(error);
        }
      ).finally(function(){
        vm.isLoading = false;
      });
    }
  }
}());

