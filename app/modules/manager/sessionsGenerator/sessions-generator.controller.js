;
(function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('SessionsGeneratorController', SessionsGeneratorController);

  SessionsGeneratorController.$inject = [
    '$scope',
    '$q',
    '$uibModal',
    'SessionGeneratorManagerService',
    'currentProvider',
    'NotificationsService',
    'ResponseCatcherService',
    'ServicesManagerService',
    'LocationsManagerService',
    'ResourcesManagerService',
    'RoomsManagerService',
    'ServiceConfigService'
  ];

  function SessionsGeneratorController($scope, $q, $uibModal, SessionGeneratorManagerService, currentProvider, NotificationsService, ResponseCatcherService, ServicesManagerService, LocationsManagerService, ResourcesManagerService, RoomsManagerService, ServiceConfigService) {
    var vm = this;

    vm.isLoading = true;
    vm.provider = {};
    vm.selectedEndDate = '';
    vm.selectedResource = {};
    vm.selectedStartDate = '';
    vm.selectedServiceConfig = {};
    vm.status = {};

    vm.weekdays = [];
    vm.selectedSessionModels = [];
    vm.serviceConfigs = [];
    vm.sessionModels = [];

    vm.checkAll = checkAll;
    vm.clearAll = clearAll;
    vm.disableGenerate = disableGenerate;
    vm.generateSessions = generateSessions;
    vm.getSessionModels = getSessionModels;
    vm.openEndDatepicker = openEndDatepicker;
    vm.openStartDatepicker = openStartDatepicker;
    vm.removeSessionModel = removeSessionModel;
    vm.serviceConfigSelected = serviceConfigSelected;
    vm.showSessionModelsModal = showSessionModelsModal;

    init();

    function init() {
      vm.isLoadingAll = true;
      vm.provider = currentProvider;
      vm.location = LocationsManagerService.getCurrentLocation();

      initWeekdays();

      vm.status = {
        startOpened: false,
        endOpened: false
      };

      if (vm.location) {
        getServiceConfigs(vm.location.id).then(
          function(){
            vm.isLoadingAll = false;
          }
        );
      }
    }

    // Weekdays
    function initWeekdays() {
      vm.weekdays = [{name: 'sun', value: 0}, {name: 'mon', isSelected: false}, {
        name: 'tue',
        isSelected: false
      }, {name: 'wed', isSelected: false}, {name: 'thu', isSelected: false}, {
        name: 'fri',
        isSelected: false
      }, {name: 'sat'}];
    }

    function checkAll() {
      for (var i = 0; i < vm.weekdays.length; i++) {
        vm.weekdays[i].isSelected = true;
      }
    }

    function getServiceConfigs(locationId) {
      var deferred = $q.defer();

      ServiceConfigService.getServiceConfigs(locationId).then(
        function (serviceConfigs) {
          vm.serviceConfigs = serviceConfigs;
          deferred.resolve(serviceConfigs);
        }, function (error) {
          ResponseCatcherService.catchError(error, true);
          deferred.reject(error);
        }
      );
      return deferred.promise;
    }

    function serviceConfigSelected(){
      if(vm.selectedServiceConfig.room){
        vm.isLoadingServiceConfig = true;
        var promises = [
          getRoom(vm.selectedServiceConfig.roomId),
          getServiceProvisions(vm.selectedServiceConfig.serviceProvisions),
          getAttendants(vm.selectedServiceConfig.attendants)
        ];
        $q.all(promises).then(
          function(response){
            vm.selectedServiceConfig.room = response[0];
            vm.selectedServiceConfig.serviceProvisions = response[1];
            vm.selectedServiceConfig.attendants = response[2];
          }
        ).finally(function(){
          vm.isLoadingServiceConfig = false;
        })
      }
    }

    function getRoom(roomId){
      var deferred = $q.defer();

      RoomsManagerService.getRoom(vm.location.id, roomId).then(
        function (room) {
          deferred.resolve(room);
        },
        function (error) {
          ResponseCatcherService.catchError(error);
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function getServiceProvisions(services){
      var deferred = $q.defer();

      var promises = [];

      for(var i = 0; i < services.length; i++){
        var service = services[i];
        promises.push(getService(typeof service === 'number' ? service : service.id));
      }

      $q.all(promises).then(
        function(services){
          deferred.resolve(services);
        },
        function(error){
          deferred.reject(error);
        }
      );

      return deferred.promise;
    }

    function getService(serviceId){
      var deferred = $q.defer();

      ServicesManagerService.getService(vm.location.id, serviceId).then(
        function(service) {
          deferred.resolve(service);
        },
        function(error){
          ResponseCatcherService.catchError(error);
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function getAttendants(attendants){
      var deferred = $q.defer();
      var promises = [];

      for(var i = 0; i < attendants.length; i++){
        var attendant = attendants[i];
        promises.push(getAttendant(typeof attendant === 'number' ? attendant : attendant.id));
      }

      $q.all(promises).then(
        function(attendants){
          deferred.resolve(attendants);
        },
        function(error){
          deferred.reject(error);
        }
      );

      return deferred.promise;
    }

    function getAttendant(attendantId){
      var deferred = $q.defer();

      ResourcesManagerService.getResource(vm.location.id, attendantId).then(
        function (attendant) {
          deferred.resolve(attendant);
        },
        function (error) {
          ResponseCatcherService.catchError(error);
          deferred.reject(error);
        });

      return deferred.promise;
    }

    // Generate Sessions
    function generateSessions() {
      var generateObject = {
        sessionModelIds: [],
        weekdays: [],
        startDate: null,
        endDate: null
      };
      if (vm.selectedStartDate) {
        generateObject.startDate = vm.selectedStartDate.toJSON()
      }
      if (vm.selectedEndDate) {
        generateObject.endDate = vm.selectedEndDate.toJSON()
      }

      for (var i = 0; i < vm.weekdays.length; i++) {
        if (vm.weekdays[i].isSelected) {
          generateObject.weekdays.push(i);
        }
      }
      for (i = 0; i < vm.selectedSessionModels.length; i++) {
        generateObject.sessionModelIds.push(vm.selectedSessionModels[i].id)
      }

      showErrorMessages(generateObject);

      if (!disableGenerate()) {
        vm.isLoadingAll = true;
        SessionGeneratorManagerService.generate(vm.location.id, vm.selectedServiceConfig.id, generateObject).then(
          function(response){
            ResponseCatcherService.catchSuccess(response).then(
              function () {
                vm.clearAll();
              }
            );
          },
          function (error) {
            ResponseCatcherService.catchError(error);
          }
        ).finally(function () {
          vm.isLoadingAll = false;
        });
      }
    }

    function disableGenerate() {
      var selectedWeekdays;
      for (var i = 0; i < vm.weekdays.length; i++) {
        if (vm.weekdays[i].isSelected) {
          selectedWeekdays = true;
          break;
        }
      }
      return vm.selectedSessionModels.length === 0 || !vm.selectedStartDate || !vm.selectedEndDate || !selectedWeekdays;
    }

    function showErrorMessages(generateObject) {
      if (!vm.selectedServiceConfig.id) {
        return NotificationsService.error("A configuração de atendimento deve ser selecionada");
      }

      if (generateObject.sessionModelIds.length === 0) {
        return NotificationsService.error("Ao menos um modelo de sessão deve ser selecionado");
      }

      if (generateObject.weekdays.length === 0) {
        return NotificationsService.error("Ao menos um dia da semana deve ser selecionado.");
      }

      if (generateObject.startDate === null || generateObject.endDate === null) {
        return NotificationsService.error("As datas de inicio e fim devem ser selecionadas");
      }
    }

    function clearAll() {
      vm.selectedServiceConfig = {};
      vm.selectedSessionModels = [];
      initWeekdays();
      vm.selectedStartDate = '';
      vm.selectedEndDate = '';
    }

    // Session Model
    function getSessionModels(providerId) {
      return SessionGeneratorManagerService.getSessionModels(providerId).then(
        function (sessionModels) {
          vm.sessionModels = sessionModels;
          return vm.sessionModels;
        },
        function (error) {
          ResponseCatcherService.catchError(error, true);
        }
      )
    }

    function removeSessionModel(index) {
      vm.selectedSessionModels.splice(index, 1);
    }

    function showSessionModelsModal() {
      vm.isLoadingSessionModels = true;
      var resolve = {
        sessionModels: [],
        providerId: vm.provider.id,
        selectedSessionModels: vm.selectedSessionModels
      };
      vm.getSessionModels(vm.provider.id).then(
        function (sessionModels) {
          resolve.sessionModels = sessionModels;
          $uibModal.open({
            templateUrl: 'modules/manager/sessionsGenerator/session-model-select-modal.html',
            controller: 'SessionModelSelectModalController',
            controllerAs: 'vm',
            size: 'lg',
            windowClass: 'stick-up',
            resolve: {
              resolve: function () {
                return resolve;
              }
            }
          }).result.then(
            function (sessionModels) {
              vm.selectedSessionModels = sessionModels;
            }
          );
        }
      ).finally(function () {
        vm.isLoadingSessionModels = false;
      });
    }

    // Datepicker
    function openStartDatepicker($event) {
      $event.preventDefault();
      $event.stopPropagation();

      vm.status.endOpened = false;
      vm.status.startOpened = !vm.status.startOpened;
    }

    function openEndDatepicker($event) {
      $event.preventDefault();
      $event.stopPropagation();

      vm.status.startOpened = false;
      vm.status.endOpened = !vm.status.endOpened;
    }

    function resetDatepickerConfig() {
      var startDate;
      if (vm.selectedStartDate) {
        startDate = new Date(vm.selectedStartDate);
      } else {
        startDate = new Date();
      }
      vm.startDateConfig = {
        minDate: new Date(),
        maxDate: new Date(vm.selectedEndDate)
      };

      vm.endDateConfig = {
        minDate: startDate,
        maxDate: new Date(2020, 5, 22)
      };
    }

    $scope.$watch('vm.selectedEndDate', function () {
      resetDatepickerConfig();
    });

    $scope.$watch('vm.selectedStartDate', function () {
      resetDatepickerConfig();
    });

    $scope.$on('appLocationChanged', function () {
      init();
    });
  }
})();
