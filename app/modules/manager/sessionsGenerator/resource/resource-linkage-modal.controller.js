;
(function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('ResourceLinkageModalController', ResourceLinkageModalController);

  ResourceLinkageModalController.$inject = ['resolve', '$uibModalInstance', 'ServicesManagerService', 'ResponseCatcherService'];

  function ResourceLinkageModalController(resolve, $uibModalInstance, ServicesManagerService, ResponseCatcherService) {

    var vm = this;

    vm.resource = {};
    vm.service = {};
    vm.selectedRoom = {};

    vm.rooms = [];

    vm.cancel = cancel;
    vm.confirm = confirm;

    init();

    function addResourceAndRoom(resource, room, serviceId) {
      var resourceAndRoom = {
        resource: resource,
        room: room
      };
      ServicesManagerService.addServiceResource(resolve.locationId, serviceId, resourceAndRoom).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response).then(
            function(){
              $uibModalInstance.close();
            }
          )
        });
    }

    function init(){
      vm.resource = resolve.resource;
      vm.service = resolve.service;
      vm.rooms = resolve.rooms;
    }

    function cancel(){
      $uibModalInstance.dismiss('cancel');
    }

    function confirm(resource, room, serviceId){
      addResourceAndRoom(resource, room, serviceId);
    }

  }
})();
