;
(function () {
  'use strict';

  angular
    .module('filazero-app')
    .config(['$stateProvider', function ($stateProvider) {
      $stateProvider
        .state('app.provider.session-add', {
          url: '/sessions/add',
          data: {
            requireAuthentication: true
          },
          templateUrl: 'modules/manager/sessionsGenerator/sessions-generator.html',
          controller: 'SessionsGeneratorController',
          controllerAs: 'vm',
          resolve: {
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
              return $ocLazyLoad.load([
                'datepicker',
                'services/manager/sessions-generator-manager.service.js',
                'services/manager/services-manager.service.js',
                'services/manager/locations-manager.service.js',
                'services/manager/rooms-manager.service.js',
                'services/notifications.service.js',
                'services/manager/resources-manager.service.js',
                'services/service-config.service.js'
              ], {
                insertBefore: '#lazyload_placeholder'
              }).then(function () {
                return $ocLazyLoad.load([
                  'modules/manager/sessionsGenerator/sessions-generator.controller.js',
                  'modules/manager/sessionsGenerator/session-model-select-modal.controller.js',
                  'modules/manager/sessionsGenerator/session-model-remove-modal.controller.js'
                ]);
              });
            }]
          }
        }
      );
    }]);
}());
