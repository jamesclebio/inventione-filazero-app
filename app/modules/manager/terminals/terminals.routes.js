;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider.state('app.provider.terminals', {
      url: '/manager/terminals',
      data: {
        requireAuthentication: true
      },
      templateUrl: 'modules/manager/terminals/terminals.html',
      controller: 'TerminalsController',
      controllerAs: 'vm',
      resolve: {
        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            'services/terminals.service.js',
            'services/manager/services-manager.service.js',
            'services/manager/locations-manager.service.js'
          ], {
            insertBefore: '#lazyload_placeholder'
          }).then(function () {
            return $ocLazyLoad.load([
              'modules/manager/terminals/terminals.controller.js',
              'modules/manager/terminals/terminal-manager-modal.controller.js',
              'modules/manager/terminals/terminal-remove-modal.controller.js'
            ]);
          });
        }]
      }
    });
  }
}());
