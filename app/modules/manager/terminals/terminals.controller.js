; (function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('TerminalsController', TerminalsController);

  TerminalsController.$inject = [
    '$scope',
    '$uibModal',
    'LocationsManagerService',
    'TerminalsService',
    'ResponseCatcherService',
    'ProviderService'
  ];

  function TerminalsController($scope, $uibModal, LocationsManagerService, TerminalsService, ResponseCatcherService, ProviderService) {
    var vm = this;

    vm.location = {};
    vm.terminal = {};

    vm.currentPage = 1;
    vm.itensLimit = 10;
    vm.order = 'name';

    vm.filteredTerminals = [];
    vm.terminalsList = [];

    vm.addTerminal = addTerminal;
    vm.updateTerminal = updateTerminal;
    vm.getTerminals = getTerminals;
    vm.deleteTerminal = deleteTerminal;
    vm.setOrder = setOrder;

    // Initialization
    $scope.$on('appLocationChanged', function () {
      init();
    });

    function init() {
      vm.providerSlug = ProviderService.getCurrentProvider().slug;
      vm.location = LocationsManagerService.getCurrentLocation();

      if (vm.location) {
        vm.getTerminals(vm.location.id);
      }
    }

    init();
    // /Initialization

    // Terminals
    function getTerminals(locationId) {
      vm.isLoading = true;
      TerminalsService.getTerminals(locationId).then(
        function (terminals) {
          vm.terminalsList = terminals;
        },
        function (error) {
          ResponseCatcherService.catchError(error);
        }).finally(function(){
        vm.isLoading = false;
      });
    }

    function addTerminal() {
      var templateUrl = 'modules/manager/terminals/terminal-manager-modal.html';
      var controller = 'TerminalManagerModalController';
      var addTerminalObject = {
        locationId: vm.location.id
      };
      var size = 'md';
      openModal(templateUrl, controller, addTerminalObject, size);
    }

    function updateTerminal(terminalId) {
      var updateTerminalObject = {
        locationId: vm.location.id,
        terminalId: terminalId
      };

      var templateUrl = 'modules/manager/terminals/terminal-manager-modal.html';
      var controller = 'TerminalManagerModalController';

      openModal(templateUrl, controller, updateTerminalObject, 'md');
    }

    function deleteTerminal(terminal) {
      var deleteTerminalObject = {
        locationId: vm.location.id,
        terminal: terminal
      };
      var templateUrl = 'modules/manager/terminals/terminal-remove-modal.html';
      var controller = 'TerminalRemoveModalController';

      openModal(templateUrl, controller, deleteTerminalObject, 'md');
    }
    // /Terminals

    function openModal(templateUrl, controller, resolve, size) {
      var modalInstance = $uibModal.open({
        templateUrl: templateUrl,
        controller: controller,
        controllerAs: 'vm',
        size: size,
        windowClass: 'stick-up',
        resolve: {
          resolve: function () {
            return resolve;
          }
        }
      });

      modalInstance.result.then(
        function () {
          vm.getTerminals(vm.location.id);
        }
      );
    }

    function setOrder(order) {
      if (order === vm.order) {
        vm.order = '-' + order;
      } else {
        vm.order = order;
      }
    }
  }
})();
