; (function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('TerminalManagerModalController', TerminalManagerModalController);

  TerminalManagerModalController.$inject = [
    '$uibModalInstance',
    '$q',
    'TerminalsService',
    'ServicesManagerService',
    'resolve',
    'ResponseCatcherService'
  ];

  function TerminalManagerModalController($uibModalInstance, $q, TerminalsService, ServicesManagerService, resolve, ResponseCatcherService) {

    var vm = this;

    var locationId = {};

    vm.terminal = { services: [] };
    vm.services = [];

    vm.cancel = cancel;
    vm.isEditing = isEditing;
    vm.save = save;
    vm.serviceAlreadySelected = serviceAlreadySelected;

    // Initialization
    function init() {
      locationId = resolve.locationId;

      getServices(locationId).then(
        function (services) {
          vm.services = services;
          if (isEditing()) {
            vm.terminal.id = resolve.terminalId;
            getTerminal(locationId, vm.terminal.id).then(
              function (terminal) {
                vm.terminal = terminal;
              }
            )
          }
        }
      );
    }

    init();
    // /Initialization

    function getTerminal(locationId, terminalId) {
      vm.isLoading = true;
      var deferred = $q.defer();

      TerminalsService.getTerminal(locationId, terminalId).then(
        function (terminal) {
          for (var i = 0; i < terminal.services.length; i++) {
            terminal.services[i] = terminal.services[i].id;
          }
          deferred.resolve(terminal);
        },
        function (error) {
          ResponseCatcherService.catchError(error);
          $uibModalInstance.dismiss();
          deferred.reject(error);
        }).finally(
        function () {
          vm.isLoading = false;
        });

      return deferred.promise;
    }

    function getServices(locationId) {
      var deferred = $q.defer();

      ServicesManagerService.getServices(locationId).then(
        function (services) {
          deferred.resolve(services);
        },
        function (error) {
          ResponseCatcherService.catchError(error);
          $uibModalInstance.dismiss();
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function save(form) {
      vm.isLoading = true;
      form.$setSubmitted();
      var terminalObject = {
        locationId: locationId,
        terminal: vm.terminal
      };
      if (isEditing()) {
        TerminalsService.updateTerminal(terminalObject).then(
          function (response) {
            ResponseCatcherService.catchSuccess(response).then(
              function (messages) {
                $uibModalInstance.close();
              }
            );
          },
          function (error) {
            ResponseCatcherService.catchError(error);
          }).finally(
          function () {
            vm.isLoading = false;
          });
      } else {
        TerminalsService.addTerminal(terminalObject).then(
          function (response) {
            ResponseCatcherService.catchSuccess(response).then(
              function (messages) {
                $uibModalInstance.close();
              }
            );
          },
          function (error) {
            ResponseCatcherService.catchError(error);
          }).finally(
          function () {
            vm.isLoading = false;
          });
      }
    }

    function isEditing() {
      return resolve.terminalId;
    }

    function isValid() {
      return vm.terminal.name && vm.terminal.name.length >= 3 && vm.terminal.services.length > 0;
    }

    function serviceAlreadySelected(service) {
      for (var i = 0; i < vm.terminal.services.length; i++) {
        if (vm.terminal.services[i].id === service.id) {
          return true;
        }
      }
      return false;
    }

    function cancel() {
      $uibModalInstance.dismiss();
    }

  }
})();
