;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('TerminalRemoveModalController', TerminalRemoveModalController);

  TerminalRemoveModalController.$inject = [
    '$uibModalInstance',
    'TerminalsService',
    'resolve',
    'ResponseCatcherService'
  ];

  function TerminalRemoveModalController($uibModalInstance, TerminalsService, resolve, ResponseCatcherService) {
    var vm = this;
    vm.terminal = {};

    vm.cancel = cancel;
    vm.confirm = confirm;

    init();

    function init() {
      vm.terminal = resolve.terminal;
    }

    function cancel() {
      $uibModalInstance.dismiss();
    }

    function confirm() {
      vm.isLoading = true;
      TerminalsService.deleteTerminal({
        id: resolve.terminal.id,
        locationId: resolve.locationId
      }).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response).then(
            function(messages) {
              $uibModalInstance.close();
            }
          );
        },
        function () {
          ResponseCatcherService.catchError(error);
        }
      ).finally(function(){
        vm.isLoading = false;
      });
    }
  }
})();
