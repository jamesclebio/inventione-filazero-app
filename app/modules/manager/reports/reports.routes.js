;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider.state('app.provider.reports', {
      url: '/reports',
      data:{
        requireAuthentication: true
      },
      templateUrl: 'modules/manager/reports/reports.html',
      controller: 'ReportsController',
      controllerAs: 'vm',
      resolve: {
        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            'datepicker',
            'services/reports.service.js',
            'services/manager/locations-manager.service.js'
          ], {
            insertBefore: '#lazyload_placeholder'
          }).then(function () {
            return $ocLazyLoad.load([
              'modules/manager/reports/reports.controller.js',
            ]);
          });
        }]
      }
    });
  }
})();
