; (function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('ReportsController', ReportsController)

  ReportsController.$inject = [
    '$scope',
    '$stateParams',
    'currentProvider',
    'ReportsService',
    'LocationsManagerService',
    'ResponseCatcherService'
  ];

  function ReportsController($scope, $stateParams, currentProvider, ReportsService, LocationsManagerService, ResponseCatcherService) {
    var vm = this;

    vm.isLoading = true;
    vm.order = 'service.name';

    vm.getAttendanceReport = getAttendanceReport;
    vm.setOrder = setOrder;
    vm.openStartDatepicker = openStartDatepicker;
    vm.openEndDatepicker = openEndDatepicker;

    // Initialization
    $scope.$on('appLocationChanged', function () {
      init();
    });

    function init() {
      vm.location = LocationsManagerService.getCurrentLocation();

      initDates();

      if (vm.location) {
        vm.getAttendanceReport(vm.location.id);
      }
    }

    function initDates() {
      vm.endDate = new Date();
      vm.startDate = new Date();
      vm.startDate.setDate(vm.startDate.getDate() - 30);
    }

    init();
    // /Initialization

    // Reports
    function getAttendanceReport(locationId) {
      vm.isLoading = true;
      ReportsService.getAttendanceReport(locationId, vm.startDate, vm.endDate).then(
        function (attendanceReport) {
          vm.attendanceReport = attendanceReport;
          vm.isLoading = false;
        },
        function (error) {
          ResponseCatcherService.catchError(error, true);
        });
    }
    // /Reports

    // Table
    function setOrder(order) {
      if (order === vm.order) {
        vm.order = '-' + order;
      } else {
        vm.order = order;
      }
    }
    // /Table

    // Datepickers
    function openStartDatepicker() {
      vm.startDateOpened = true;
    }

    function openEndDatepicker() {
      vm.endDateOpened = true;
    }
    // /Datepickers
  }
})();
