; (function() {
  'use strict';

  angular
    .module('filazero-app')
    .controller('DashboardController', DashboardController);

  DashboardController.$inject = [
    '$scope',
    'LocationsManagerService',
    'DashboardService',
    '$q',
    '$stateParams',
    'ServicesManagerService',
    'currentProvider',
    'ResponseCatcherService'
  ];

  function DashboardController($scope, LocationsManagerService, DashboardService, $q, $stateParams, ServicesManagerService, currentProvider, ResponseCatcherService) {
    var vm = this;
    var location;
    var provider = currentProvider;

    vm.isLoadingTicketsPerDayChartData = true;
    vm.isLoadingTicketsPerServiceChartData = true;
    vm.isLoadingServicesAverageTimePerMonthChartData = true;

    vm.charts = {};

    vm.serviceChanged = serviceChanged;

    function init() {
      vm.location = LocationsManagerService.getCurrentLocation();
      if (vm.location) {
        initCharts();
        getTicketsPerDayChartData(vm.location.id);
        getTicketsPerServiceChartData(vm.location.id);
        //getServices(vm.location.id, getServicesAverageTimePerMonthChartData);
      }
    }

    init();

    function initCharts() {
      vm.charts = {
        line: {
          options: {
            chart: {
              type: 'lineChart',
              x: function(d) {
                return d[0];
              },
              height: 300,
              y: function(d) {
                return d[1];
              },
              color: [
                $.Pages.getColor('primary'),
                $.Pages.getColor('success'),
                $.Pages.getColor('danger'),
                $.Pages.getColor('complete')
              ],
              xAxis: {
                tickFormat: function(d) {
                  return d3.time.format('%d/%m/%y')(new Date(d));
                },
                axisLabel: 'Data (dia/mês/ano)'
              },
              yAxis: {
                tickFormat: d3.format('d'),
                axisLabel: 'Tickets'
              },
              useInteractiveGuideline: false,
              showLegend: true
            }
          }
        },
        donut: {
          options: {
            chart: {
              type: 'pieChart',
              height: 300,
              donut: true,
              x: function(d) {
                return d.serviceName;
              },
              y: function(d) {
                return d.tickets;
              },
              showLabels: true,

              duration: 2000,
              legend: {
                margin: {
                  top: 15,
                  right: 70,
                  bottom: 5,
                  left: 0
                }
              }
            }
          }
        },
        bar: {
          options: {
            chart: {
              type: 'historicalBarChart',
              height: 300,
              margin: {
                top: 20,
                right: 20,
                bottom: 65,
                left: 50
              },
              x: function(d) { return d[0]; },
              y: function(d) { return d[1]; },
              showValues: true,
              valueFormat: function(d) {
                return d.toFixed(1) + " min";
              },
              duration: 100,
              xAxis: {
                tickFormat: function(d) {
                  return d3.time.format('%b/%Y')(new Date(d))
                },
                rotateLabels: -15,
                showMaxMin: true
              },
              yAxis: {
                tickFormat: function(d) {
                  return d.toFixed(1) + " min";
                }
              },
              tooltip: {
                keyFormatter: function(d) {
                  return d3.time.format('%b/%Y')(new Date(d));
                }
              },
              zoom: {
                enabled: true,
                scaleExtent: [1, 10],
                useFixedDomain: false,
                useNiceScale: false,
                horizontalOff: false,
                verticalOff: true,
                unzoomEventType: 'dblclick.zoom'
              }
            }
          }
        }
      };
    }

    function getServices(locationId, functionGet) {
      ServicesManagerService.getServices(locationId).then(
        function(services) {
          vm.services = services;
          vm.selectedService = services[0];
          if (functionGet) {
            functionGet(locationId, services[0].id);
          }
        },
        function(error) {
          ResponseCatcherService.catchError(error, true);
        }
      )
    }

    function getServicesAverageTimePerMonthChartData(locationId, serviceId) {
      vm.isLoadingServicesAverageTimePerMonthChartData = true;
      DashboardService.getServicesAverageTimePerMonthChartData(locationId, serviceId).then(
        function(response) {
          $scope.barData = [
            {
              "key": "Tempo em minutos",
              "bar": true,
              "values": []
            }];
          angular.forEach(response, function(item) {
            $scope.barData[0].values.push([item[0], item[1]]);
          });
          vm.isLoadingServicesAverageTimePerMonthChartData = false;
        },
        function(error) {
          ResponseCatcherService.catchError(error, true);
        }
      );
    }

    function getTicketsPerDayChartData(locationId) {
      vm.isLoadingTicketsPerDayChartData = true;
      DashboardService.getTicketsPerDayChartData(locationId).then(
        function(response) {
          $scope.lineData = [{ key: '', values: [] }, { key: '', values: [] }, { key: '', values: [] }];
          $scope.lineData[0].key = 'Total';
          $scope.lineData[1].key = 'Atendidos';
          $scope.lineData[2].key = 'Cancelados';

          for (var i = 0; i < response.length; i++) {
            angular.forEach(response[i], function(item) {
              $scope.lineData[i].values.push(item);
            });
          }
          vm.isLoadingTicketsPerDayChartData = false;
        },
        function(error) {
          ResponseCatcherService.catchError(error, true);
        }
      );
    }

    function getTicketsPerServiceChartData(locationId) {
      vm.isLoadingTicketsPerServiceChartData = true;
      DashboardService.getTicketsPerServiceChartData(locationId).then(
        function(response) {
          $scope.donutData = response;
          vm.isLoadingTicketsPerServiceChartData = false;
        },
        function(error) {
          ResponseCatcherService.catchError(error, true);
        }
      );
    }

    function serviceChanged(serviceId) {
      getServicesAverageTimePerMonthChartData(vm.location.id, serviceId);
    }

    $scope.$on('appLocationChanged', function() {
      init();
    });
  }
})();
