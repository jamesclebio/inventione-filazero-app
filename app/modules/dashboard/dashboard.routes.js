;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider.state('app.provider.dashboard', {
      url: '/dashboard',
      data:{
        requireAuthentication: true
      },
      templateUrl: 'modules/dashboard/dashboard.html',
      controller: 'DashboardController',
      controllerAs: 'vm',
      resolve: {
        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            'nvd3',
            'mapplic',
            'rickshaw',
            'metrojs',
            'sparkline',
            'skycons',
            'switchery',
            'services/manager/services-manager.service.js',
            'services/manager/locations-manager.service.js',
            'services/dashboard.service.js'
          ], {
            insertBefore: '#lazyload_placeholder'
          }).then(function () {
            return $ocLazyLoad.load([
              'modules/dashboard/dashboard.controller.js'
            ]);
          });
        }]
      }
    });
  }
}());
