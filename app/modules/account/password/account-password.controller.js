;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('AccountPasswordController', AccountPasswordController);

  AccountPasswordController.$inject = ['AccountService', 'ResponseCatcherService'];

  function AccountPasswordController(AccountService, ResponseCatcherService) {

    var vm = this;

    vm.passwordObject = {};

    vm.editPassword = editPassword;

    function editPassword(form) {
      AccountService.changePassword(vm.passwordObject).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response).then(
            function () {
              vm.passwordObject = {};
              form.$setPristine();
            }
          )
        },
        function (error) {
          ResponseCatcherService.catchError(error);
        }
      )
    }
    }
  }
  )();
