;
(function () {
  'use strict';

  angular
    .module('filazero-app')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider
      .state('app.account', {
        abstract: true,
        url: '/account',
        data: {
          requireAuthentication: true
        },
        templateUrl: 'modules/account/account.html',
        controller: 'AccountController',
        controllerAs: 'vm',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([], {
              insertBefore: '#lazyload_placeholder'
            }).then(function () {
              return $ocLazyLoad.load([
                'modules/account/account.controller.js'
              ]);
            });
          }]
        }
      })

      .state('app.account.providers', {
        url: '/providers',
        templateUrl: 'modules/account/providers/account-providers.html',
        controller: 'AccountProvidersController',
        controllerAs: 'vm',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([], {
              insertBefore: '#lazyload_placeholder'
            }).then(function () {
              return $ocLazyLoad.load([
                'modules/account/providers/account-providers.controller.js',
                'modules/account/providers/account-provider-add-modal.controller.js',
                'modules/account/providers/account-provider-edit-modal.controller.js',
                'modules/account/providers/account-provider-remove-modal.controller.js'
              ]);
            });
          }]
        }
      })

      .state('app.account.profile', {
        url: '/profile',
        templateUrl: 'modules/account/profile/account-profile.html',
        controller: 'AccountProfileController',
        controllerAs: 'vm',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              'datepicker',
              'services/account.service.js'
            ], {
              insertBefore: '#lazyload_placeholder'
            }).then(function () {
              return $ocLazyLoad.load([
                'modules/account/profile/account-profile.controller.js'
              ]);
            });
          }]
        }
      })

      .state('app.account.password', {
        url: '/password',
        templateUrl: 'modules/account/password/account-password.html',
        controller: 'AccountPasswordController',
        controllerAs: 'vm',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              'services/account.service.js'
            ], {
              insertBefore: '#lazyload_placeholder'
            }).then(function () {
              return $ocLazyLoad.load([
                'modules/account/password/account-password.controller.js'
              ]);
            });
          }]
        }
      })
  }
}());
