;
(function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('AccountProviderEditModalController', AccountProviderEditModalController);

  AccountProviderEditModalController.$inject = [
    'ProvidersService',
    '$uibModalInstance',
    'resolve',
    'ResponseCatcherService'
  ];

  function AccountProviderEditModalController(ProvidersService, $uibModalInstance, resolve, ResponseCatcherService) {
    var vm = this;

    vm.isAvailable = true;
    vm.isLoading = true;
    vm.provider = {};

    vm.categories = [];

    vm.cancel = cancel;
    vm.checkAvailability = checkAvailability;
    vm.isProviderValid = isProviderValid;
    vm.save = save;

    function init() {
      getProvider(resolve.providerId);
      getCategories();
    }

    init();

    function getCategories() {
      vm.isLoading = true;
      ProvidersService.getCategories().then(
        function (categories) {
          vm.categories = categories;
        },
        function (error) {
          ResponseCatcherService.catchError(error, true);
        }).finally(
        function () {
          vm.isLoading = false;
        });
    }

    function getProvider(providerId) {
      vm.isLoading = true;
      ProvidersService.getProvider(providerId).then(
        function (provider) {
          vm.provider = provider;
          vm.focus = !vm.focus;
        },
        function (error) {
          ResponseCatcherService.catchError(error, true);
        }).finally(
        function () {
          vm.isLoading = false;
        });
    }

    function save() {
      vm.isLoading = true;

      var updateProviderObject = {
        provider: vm.provider
      };

      ProvidersService.updateProvider(updateProviderObject).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response).then(
            function (messages) {
              $uibModalInstance.close();
            }
          );
        },
        function (error) {
          ResponseCatcherService.catchError(error);
        }).finally(
        function () {
          vm.isLoading = false;
        });
    }

    function checkAvailability() {
      if (vm.provider.slug && vm.provider.slug.length > 3) {
        ProvidersService.checkAvailability(vm.provider.slug).then(
          function (response) {
            vm.isAvailable = response.isAvailable;
          }
        )
      }
    }

    function isProviderValid() {
      return $scope.provider.$invalid || !vm.isAvailable || !vm.provider.category;
    }

    function cancel() {
      $uibModalInstance.dismiss();
    }
  }
})();
