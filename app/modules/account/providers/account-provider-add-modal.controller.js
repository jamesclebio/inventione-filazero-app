;
(function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('AccountProviderAddModalController', AccountProviderAddModalController);

  AccountProviderAddModalController.$inject = [
    '$scope',
    'ProvidersService',
    '$uibModalInstance',
    'ResponseCatcherService'
  ];

  function AccountProviderAddModalController($scope, ProvidersService, $uibModalInstance, ResponseCatcherService) {

    var vm = this;

    vm.isAvailable = null;
    vm.location = {};
    vm.provider = {};
    vm.showProvider = null;

    vm.categories = [];
    vm.states = [];

    vm.cancel = cancel;
    vm.checkAvailability = checkAvailability;
    vm.isMobile = isMobile;
    vm.isProviderValid = isProviderValid;
    vm.save = save;

    function init() {
      getCategories();
      vm.isAvailable = false;
      vm.location = {
        name: "",
        phone: "",
        type: "Fixed",
        zipCode: "",
        address: "",
        city: "",
        state: "",
        expedients: [
          {
            "name": "Sunday",
            "isOpen": false,
            "opens": null,
            "closes": null
          },
          {
            "name": "Monday",
            "isOpen": true,
            opens: new Date(0, 0, 0, 7),
            closes: new Date(0, 0, 0, 18)
          },
          {
            "name": "Tuesday",
            "isOpen": true,
            "opens": new Date(0, 0, 0, 7),
            "closes": new Date(0, 0, 0, 18)
          },
          {
            "name": "Wednesday",
            "isOpen": true,
            "opens": new Date(0, 0, 0, 7),
            "closes": new Date(0, 0, 0, 18)
          },
          {
            "name": "Thursday",
            "isOpen": true,
            "opens": new Date(0, 0, 0, 7),
            "closes": new Date(0, 0, 0, 18)
          },
          {
            "name": "Friday",
            "isOpen": true,
            "opens": new Date(0, 0, 0, 7),
            "closes": new Date(0, 0, 0, 18)
          },
          {
            "name": "Saturday",
            "isOpen": false,
            "opens": null,
            "closes": null
          }
        ]
      };
      vm.states = [
        "Acre", "Alagoas", "Amapá", "Amazonas", "Bahia", "Ceará", "Distrito Federal", "Espírito Santo", "Goias", "Maranhão", "Mato Grosso", "Mato Grosso do Sul", "Minas Gerais", "Pará", "Paraíba", "Paraná", "Pernambuco", "Piauí", "Rio de Janeiro", "Rio Grande do Norte", "Rio Grande do Sul", "Rondônia", "Roraima", "Santa Catarina", "São Paulo", "Sergipe", "Tocantins"
      ];
      vm.showProvider = true;
      vm.focus = !vm.focus;
    }

    init();

    function getCategories() {
      vm.isLoading = true;
      ProvidersService.getCategories().then(
        function (categories) {
          vm.categories = categories;
        },
        function (error) {
          ResponseCatcherService.catchError(error, true);
        }).finally(
        function () {
          vm.isLoading = false;
        });
    }

    function save() {
      vm.isLoading = true;
      vm.provider.location = vm.location;

      ProvidersService.addProvider(vm.provider).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response).then(
            function (messages) {
              $uibModalInstance.close();
            }
          );
        },
        function (error) {
          ResponseCatcherService.catchError(error);
        }).finally(
        function () {
          vm.isLoading = false;
        });
    }

    function checkAvailability() {
      if (vm.provider.slug && vm.provider.slug.length >= 3) {
        ProvidersService.checkAvailability(vm.provider.slug).then(
          function (response) {
            vm.isAvailable = response.isAvailable;
          }
        )
      }
    }

    function isProviderValid() {
      return $scope.provider.$invalid || !vm.isAvailable || !vm.provider.category;
    }

    function isMobile() {
      return vm.location.type === "Mobile";
    }

    function cancel() {
      $uibModalInstance.dismiss();
    }
  }
})();
