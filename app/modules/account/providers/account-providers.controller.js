; (function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('AccountProvidersController', AccountProvidersController);

  AccountProvidersController.$inject = [
    'ProvidersService',
    'ngAuthSettings',
    '$uibModal',
    'AuthService',
    'ResponseCatcherService',
    '$state'
  ];

  function AccountProvidersController(ProvidersService, ngAuthSettings, $uibModal, AuthService, ResponseCatcherService, $state) {
    var vm = this;

    vm.isLoading = true;
    vm.currentPage = 0;
    vm.itensLimit = 0;
    vm.order = '';
    vm.searchProviders = '';
    vm.serviceBase = {};

    vm.filteredProviders = [];
    vm.providersList = [];

    vm.addProvider = addProvider;
    vm.editProvider = editProvider;
    vm.setOrder = setOrder;
    vm.deleteProvider = deleteProvider;

    // Initialization
    function init() {
      vm.currentPage = 1;
      vm.itensLimit = 5;
      vm.order = 'name';
      vm.serviceBase = ngAuthSettings.urlOrigin;

      getProviders();
    }

    init();
    // /Initialization

    // Providers
    function getProviders() {
      vm.isLoading = true;
      ProvidersService.getProviders().then(
        function (response) {
          vm.providersList = response.providers;
        },
        function (error) {
          ResponseCatcherService.catchError(error);
        }).finally(
        function () {
          vm.isLoading = false;
        });
    }

    function addProvider() {
      var templateUrl = 'modules/account/providers/account-provider-add-modal.html';
      var controller = 'AccountProviderAddModalController';
      var resolve = {};

      openModal(templateUrl, controller, resolve, 'lg').then(
        function () {
          getProviders();
        }
      );
    }

    function editProvider(providerId) {
      var templateUrl = 'modules/account/providers/account-provider-edit-modal.html';
      var controller = 'AccountProviderEditModalController';
      var resolve = {
        providerId: providerId
      };

      openModal(templateUrl, controller, resolve, 'md').then(
        function () {
          getProviders();
        }
      );
    }

    function deleteProvider(provider) {
      var templateUrl = 'modules/account/providers/account-provider-remove-modal.html';
      var controller = 'AccountProviderRemoveModalController';
      var resolve = {
        provider: provider
      };

      openModal(templateUrl, controller, resolve, 'md').then(
        function () {
          getProviders();
        }
      );
    }
    // /Providers

    function openModal(templateUrl, controller, resolve, size) {
      var modalInstance = $uibModal.open({
        templateUrl: templateUrl,
        controller: controller,
        controllerAs: 'vm',
        size: size,
        windowClass: 'stick-up',
        resolve: {
          resolve: function () {
            return resolve;
          }
        }
      });

      return modalInstance.result;
    }

    function setOrder(order) {
      if (order === vm.order) {
        vm.order = '-' + order;
      } else {
        vm.order = order;
      }
    }

  }
})();
