; (function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('AccountProviderRemoveModalController', AccountProviderRemoveModalController);

  AccountProviderRemoveModalController.$inject = [
    'ProvidersService',
    '$uibModalInstance',
    'resolve',
    'ResponseCatcherService'
  ];

  function AccountProviderRemoveModalController(ProvidersService, $uibModalInstance, resolve, ResponseCatcherService) {
    var vm = this;

    vm.provider = {};

    vm.cancel = cancel;
    vm.confirm = confirm;

    function init() {
      vm.provider = resolve.provider;
    }

    init();

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }

    function confirm() {
      var removeLocationObject = {
        provider: vm.provider
      };
      ProvidersService.deleteProvider(removeLocationObject).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response).then(
            function(messages) {
              $uibModalInstance.close();
            }
          );
        },
        function (error) {
          ResponseCatcherService.catchError(error);
        }
      );
    }


  }
} ());
