; (function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('InvitationsController', InvitationsController);

  InvitationsController.$inject = [
    '$stateParams',
    'AuthService',
    'InvitationService',
    '$uibModal',
    '$state',
    '$rootScope',
    'ngAuthSettings',
    'ResponseCatcherService'
  ];

  function InvitationsController($stateParams, AuthService, InvitationService, $uibModal, $state, $rootScope, ngAuthSettings, ResponseCatcherService) {
    var vm = this;

    vm.invitation = {};
    vm.isLoadingInvitation = false;
    vm.isLoadingRegistration = false;
    vm.siteURL = ngAuthSettings.site;

    vm.accept = accept;
    vm.checkLogin = checkLogin;
    vm.registerAndAccept = registerAndAccept;

    function init() {
      getInvitation();
    }

    init();

    function getInvitation() {
      vm.isLoadingInvitation = true;
      InvitationService.getInvitationDetails($stateParams.t).then(
        function (invitation) {
          vm.invitation = invitation;
          vm.isLoadingInvitation = false;
        },
        function (error) {
          ResponseCatcherService.catchError(error, true);
        }
      )
    }

    function accept() {
      vm.isLoadingInvitation = true;
      InvitationService.acceptInvitation($stateParams.t).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response).then(
            function (messages) {
              $state.go('app.account.providers');
            }
          );
        },
        function (error) {
          ResponseCatcherService.catchError(error);
        }).finally(
        function () {
          vm.isLoadingInvitation = false;
        })
    }

    function checkLogin() {
      if (AuthService.isAuth()) {
        accept();
      } else {
        var modalScope = $rootScope.$new();
        modalScope.modalInstance = $uibModal.open({
          templateUrl: 'modules/auth/login/login-modal.html',
          size: 'md',
          windowClass: 'stick-up',
          controller: 'LoginController',
          controllerAs: 'vm',
          scope: modalScope
        });
        modalScope.modalInstance.result.then(
          function (resolve) {
            if (resolve) {
              accept();
            }
          });
      }
    }

    function autoLogin() {
      var loginForm = {
        userName: vm.invitation.email,
        password: vm.user.password
      };
      AuthService.login(loginForm).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response);
          $state.go('app.account.providers');
        },
        function (error) {
          ResponseCatcherService.catchSuccess(error);
        }
      )
    }

    function registerAndAccept() {
      vm.isLoadingRegistration = true;
      var registerAndAcceptObject = {
        data: {
          firstName: vm.user.firstName,
          lastName: vm.user.lastName,
          password: vm.user.password,
          confirmPassword: vm.user.confirmPassword,
          checkTerms: vm.user.checkTerms,
          email: vm.invitation.email,
          providerId: vm.invitation.providerId
        },
        token: $stateParams.t
      };
      InvitationService.registerAndAccept(registerAndAcceptObject).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response).then(
            function (messages) {
              autoLogin();
            }
          );
        },
        function (error) {
          ResponseCatcherService.catchSuccess(error);
        }).finally(
        function () {
          vm.isLoadingRegistration = false;
        })
    }
  }
})();
