;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider.state('invitation', {
      url: '/invitations/:t',
      data: {
        requireAuthentication: false
      },
      templateUrl: 'modules/account/invitations/invitations.html',
      controller: 'InvitationsController',
      controllerAs: 'vm',
      resolve: {
        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            'services/providers.service.js',
            'services/invitation.service.js'
          ], {
            insertBefore: '#lazyload_placeholder'
          }).then(function () {
            return $ocLazyLoad.load([
              'modules/account/invitations/invitations.controller.js',
              'modules/auth/login/login.controller.js'
            ]);
          });
        }]
      }
    });
  }
}());
