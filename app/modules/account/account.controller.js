(function() {
  'use strict';

  angular
    .module('filazero-app')
    .controller('AccountController', AccountController);

  AccountController.$inject = ['ProviderService'];
  function AccountController(ProviderService) {
    var vm = this;

    function init() {
      ProviderService.removeCurrentProvider();
    }

    init();
  }
})();
