; (function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('InviteUserModalController', InviteUserModalController);

  InviteUserModalController.$inject = [
    'InvitationService',
    'resolve',
    '$uibModalInstance',
    'ResponseCatcherService',
    '$scope'
  ];

  function InviteUserModalController(InvitationService, resolve, $uibModalInstance, ResponseCatcherService, $scope) {

    var vm = this;

    vm.isLoading = false;
    vm.user = {
      email: '',
      roles: [],
      message: ''
    };
    vm.providerId = resolve;

    vm.cancel = cancel;
    vm.changeRole = changeRole;
    vm.hasRole = hasRole;
    vm.invite = invite;

    function cancel() {
      $uibModalInstance.dismiss();
    }

    function changeRole(user, role) {
      var index = user.roles.indexOf(role);
      if (index === -1) {
        vm.user.roles.push(role);
      } else {
        vm.user.roles.splice(index, 1);
      }
    }

    function hasRole(user, role) {
      return user.roles.indexOf(role) !== -1;
    }

    function invite(user) {
      $scope.user.$setSubmitted();
      if (!vm.user.roles.length || !vm.user.email){
        return;
      }

      vm.isLoading = true;
      InvitationService.invite(vm.user).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response).then(
            function () {
              $uibModalInstance.close();
            }
          )
        },
        function (error) {
          ResponseCatcherService.catchError(error);
        }).finally(
        function () {
          vm.isLoading = false;
        });
    }
  }
})();
