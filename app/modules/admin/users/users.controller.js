; (function () {
  'use strict';
  angular
    .module('filazero-app')
    .controller('UsersController', UsersController);

  UsersController.$inject = [
    'UserProviderService',
    '$scope',
    '$stateParams',
    '$uibModal',
    'InvitationService',
    'currentProvider',
    'ResponseCatcherService'
  ];

  function UsersController(UserProviderService, $scope, $stateParams, $uibModal, InvitationService, currentProvider, ResponseCatcherService) {
    var vm = this;

    vm.isLoadingUsers = true;
    vm.isLoadingInvitations = true;
    vm.currentPage = 1;
    vm.itensLimit = 5;
    vm.searchUsers = '';
    vm.order = 'name';
    vm.provider = currentProvider;

    vm.filteredUsers = [];
    vm.invites = [];
    vm.usersList = [];

    vm.cancelInvite = cancelInvite;
    vm.changeRole = changeRole;
    vm.getInvitations = getInvitations;
    vm.getUsers = getUsers;
    vm.hasRole = hasRole;
    vm.init = init;
    vm.inviteUser = inviteUser;
    vm.isOwner = isOwner;
    vm.removeUser = removeUser;
    vm.resendInvite = resendInvite;
    vm.setOrder = setOrder;

    // Initialization
    function init() {
      vm.getUsers(vm.provider.id);
      vm.getInvitations(vm.provider.id);
    }

    init();
    // /Initialization

    // Users
    function getUsers(providerId) {
      vm.isLoadingUsers = true;
      UserProviderService.getUsers(providerId).then(
        function (users) {
          vm.usersList = users;
        },
        function (error) {
          ResponseCatcherService.catchError(error, true);
        }).finally(function () {
          vm.isLoadingUsers = false;
        });
    }

    function removeUser(userId) {
      UserProviderService.removeUser(vm.provider.id, userId).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response).then(
            function (messages) {
              vm.getUsers(vm.provider.id);
              vm.getInvitations(vm.provider.id);
            }
          );
        },
        function (error) {
          ResponseCatcherService.catchError(error);
        });
    }

    function changeRole(user, role) {
      if (user.roles.indexOf(role) !== -1) {
        UserProviderService.deleteRole(vm.provider.id, user.id, role).then(
          function (response) {
            ResponseCatcherService.catchSuccess(response).then(
              function (messages) {
                vm.getUsers(vm.provider.id);
              }
            );
          },
          function (error) {
            ResponseCatcherService.catchError(error);
          });
      } else {
        UserProviderService.addRole(vm.provider.id, user.id, role).then(
          function (response) {
            ResponseCatcherService.catchSuccess(response).then(
              function (messages) {
                vm.getUsers(vm.provider.id);
              }
            );
          },
          function (error) {
            ResponseCatcherService.catchError(error);
          });
      }
    }

    function hasRole(user, role) {
      return user.roles.indexOf(role) !== -1;
    }

    function isOwner(user) {
      return user.roles.indexOf("4") !== -1;
    }
    // /Users

    // Invites
    function getInvitations(providerId) {
      vm.isLoadingInvitations = true;
      InvitationService.getProviderInvitations().then(
        function (invites) {
          vm.invites = invites;
        },
        function (error) {
          ResponseCatcherService.catchError(error, true);
        }).finally(
        function () {
          vm.isLoadingInvitations = false;
        });
    }

    function inviteUser() {
      $uibModal.open({
        templateUrl: 'modules/admin/users/invite-user-modal.html',
        controller: 'InviteUserModalController',
        controllerAs: 'vm',
        windowClass: 'stick-up',
        resolve: {
          resolve: function () {
            return vm.provider.id;
          }
        }
      }).result.then(
        function () {
          vm.getInvitations(vm.provider.id);
        });
    }

    function resendInvite(invite) {
      InvitationService.resendInvitation(invite.token)
        .then(
          function (response) {
            ResponseCatcherService.catchSuccess(response);
          },
          function (error) {
            ResponseCatcherService.catchError(error);
          })
        .finally(function(){
          vm.getInvitations(vm.provider.id);
        });
    }

    function cancelInvite(invite) {
      vm.isLoadingInvitations = true;
      InvitationService.cancelInvitation(invite.token).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response).then(
            function (messages) {
              vm.getInvitations(vm.provider.id);
            }
          );
        },
        function (error) {
          ResponseCatcherService.catchError(error);
        }).finally(
        function () {
          vm.isLoadingInvitations = false;
        });
    }
    // /Invites

    function setOrder(order) {
      if (order === vm.order) {
        vm.order = '-' + order;
      } else {
        vm.order = order;
      }
    }
  }
})();
