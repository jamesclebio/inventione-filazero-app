;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider.state('app.provider.users', {
      url: '/users',
      data:{
        requireAuthentication: true
      },
      templateUrl: 'modules/admin/users/users.html',
      controller: 'UsersController',
      controllerAs: 'vm',
      resolve: {
        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            'services/user-provider.service.js',
            'services/invitation.service.js'
          ], {
            insertBefore: '#lazyload_placeholder'
          }).then(function () {
            return $ocLazyLoad.load([
              'modules/admin/users/users.controller.js',
              'modules/admin/users/invite-user-modal.controller.js'
            ]);
          });
        }]
      }
    });
  }
}());
