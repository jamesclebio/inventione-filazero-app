; (function() {
  'use strict';

  angular
    .module('filazero-app')
    .controller('LocationRemoveModalController', LocationRemoveAdminModalController);

  LocationRemoveAdminModalController.$inject = [
    '$uibModalInstance',
    'LocationsAdminService',
    'resolve',
    'ResponseCatcherService'
  ];

  function LocationRemoveAdminModalController($uibModalInstance, LocationsAdminService, resolve, ResponseCatcherService) {
    var vm = this;

    vm.location = resolve.location;

    vm.cancel = cancel;
    vm.confirm = confirm;

    function confirm() {
      vm.isLoading = true;
      var removeLocationObject = {
        providerId: resolve.providerId,
        locationId: resolve.location.id
      };
      LocationsAdminService.deleteLocation(removeLocationObject).then(
        function(response) {
          ResponseCatcherService.catchSuccess(response).then(
            function(){
              $uibModalInstance.close();
            }
          );
          vm.isLoading = false;
        },
        function(error) {
          vm.isLoading = false;
          ResponseCatcherService.catchError(error);
        }
      );
    }

    function cancel() {
      $uibModalInstance.dismiss();
    }
  }
})();


