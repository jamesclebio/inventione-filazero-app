; (function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('LocationsController', LocationsAdminController);

  LocationsAdminController.$inject = [
    '$uibModal',
    '$rootScope',
    'currentProvider',
    'ResponseCatcherService',
    '$q',
    'LocationsAdminService'
  ];

  function LocationsAdminController($uibModal, $rootScope, currentProvider, ResponseCatcherService, $q, LocationsAdminService) {
    var vm = this;

    vm.currentPage = 1;
    vm.filteredLocations = [];
    vm.locationsList = [];
    vm.isLoading = true;
    vm.itensLimit = 10;
    vm.filter = {
      Mobile: false,
      Fixed: false
    };
    vm.provider = {};
    vm.searchLocations = "";
    vm.order = 'name';
    vm.provider = currentProvider;

    vm.addLocation = addLocation;
    vm.editLocation = editLocation;
    vm.filterByType = filterByType;
    vm.locations = init;
    vm.isMobile = isMobile;
    vm.removeLocation = removeLocation;
    vm.setOrder = setOrder;

    init();

    function init() {
      getLocations();
    }

    function getLocations() {
      vm.isLoading = true;
      LocationsAdminService.getLocations(vm.provider.id).then(
        function (locations) {
          vm.locationsList = locations;
          vm.isLoading = false;
        },
        function (error) {
          ResponseCatcherService.catchError(error, true);
        });
    }

    function getLocation(locationId) {
      var deferred = $q.defer();

      LocationsAdminService.getLocation(vm.provider.id, locationId).then(
        function (location) {
          deferred.resolve(location);
        },
        function (error) {
          deferred.reject(error);
          ResponseCatcherService.catchError(error);
        });

      return deferred.promise;
    }

    function addLocation() {
      var templateUrl = 'modules/admin/locations/location-manager-modal.html';
      var controller = 'LocationManagerModalController';
      var addLocationObject = {
        providerId: vm.provider.id
      };
      var size = 'lg';

      openModal(templateUrl, controller, addLocationObject, size);
    }

    function editLocation(locationId) {
      var templateUrl = 'modules/admin/locations/location-manager-modal.html';
      var controller = 'LocationManagerModalController';
      var editLocationObject = {
        locationId: locationId
      };
      var size = 'lg';

      openModal(templateUrl, controller, editLocationObject, size);
    }

    function removeLocation(location) {
      var templateUrl = 'modules/admin/locations/location-remove-modal.html';
      var controller = 'LocationRemoveModalController';
      var removeLocationObject = {
        providerId: vm.provider.id,
        location: location
      };
      var size = 'md';

      openModal(templateUrl, controller, removeLocationObject, size);
    }

    function openModal(templateUrl, controller, resolve, size) {
      var modalInstance = $uibModal.open({
        templateUrl: templateUrl,
        controller: controller,
        controllerAs: 'vm',
        size: size,
        windowClass: 'stick-up',
        resolve: {
          resolve: function () {
            return resolve;
          }
        }
      });

      modalInstance.result.then(
        function () {
          getLocations(vm.provider.id);
          $rootScope.$broadcast('locationsListChanged');
        });
    }

    // Filter and order
    function filterByType() {
      return function (location) {
        if (!vm.filter.Fixed && !vm.filter.Mobile) {
          return true;
        }
        if (location.type === "Fixed" && vm.filter.Fixed) {
          return true;
        } else {
          return location.type === "Mobile" && vm.filter.Mobile;
        }
      };
    }

    function setOrder(order) {
      if (order === vm.order) {
        vm.order = '-' + order;
      } else {
        vm.order = order;
      }
    }

    function isMobile(type) {
      return type === 'Mobile';
    }
  }
})();
