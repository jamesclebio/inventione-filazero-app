; (function() {
  'use strict';

  angular
    .module('filazero-app')
    .controller('LocationManagerModalController', LocationManagerModalController);

  LocationManagerModalController.$inject = [
    '$scope',
    '$uibModalInstance',
    'LocationsAdminService',
    'resolve',
    'ResponseCatcherService'
  ];

  function LocationManagerModalController($scope, $uibModalInstance, LocationsAdminService, resolve, ResponseCatcherService) {
    var vm = this;

    vm.isLoading = false;
    vm.location = {
      name: "",
      phone: "",
      type: "Fixed",
      zipCode: "",
      address: "",
      city: "",
      state: "",
      expedients: [
        {
          "name": "Sunday",
          "isOpen": false,
          "opens": null,
          "closes": null
        },
        {
          "name": "Monday",
          "isOpen": true,
          opens: new Date(0, 0, 0, 7),
          closes: new Date(0, 0, 0, 18)
        },
        {
          "name": "Tuesday",
          "isOpen": true,
          "opens": new Date(0, 0, 0, 7),
          "closes": new Date(0, 0, 0, 18)
        },
        {
          "name": "Wednesday",
          "isOpen": true,
          "opens": new Date(0, 0, 0, 7),
          "closes": new Date(0, 0, 0, 18)
        },
        {
          "name": "Thursday",
          "isOpen": true,
          "opens": new Date(0, 0, 0, 7),
          "closes": new Date(0, 0, 0, 18)
        },
        {
          "name": "Friday",
          "isOpen": true,
          "opens": new Date(0, 0, 0, 7),
          "closes": new Date(0, 0, 0, 18)
        },
        {
          "name": "Saturday",
          "isOpen": false,
          "opens": null,
          "closes": null
        }
      ]
    };

    vm.states = [
      "Acre", "Alagoas", "Amapá", "Amazonas", "Bahia", "Ceará", "Distrito Federal", "Espírito Santo", "Goias", "Maranhão", "Mato Grosso", "Mato Grosso do Sul", "Minas Gerais", "Pará", "Paraíba", "Paraná", "Pernambuco", "Piauí", "Rio de Janeiro", "Rio Grande do Norte", "Rio Grande do Sul", "Rondônia", "Roraima", "Santa Catarina", "São Paulo", "Sergipe", "Tocantins"
    ];

    vm.cancel = cancel;
    vm.invalidTime = invalidTime;
    vm.isMobile = isMobile;
    vm.save = save;


    init();

    function init(){
      if(resolve && resolve.locationId){
        getLocation(resolve.locationId);
      }
    }

    function getLocation(locationId){
      vm.isLoading = true;
      LocationsAdminService.getLocation(locationId).then(
        function(location) {
          vm.isLoading = false;
          vm.location = location;
          for (var i = 0; i < vm.location.expedients.length; i++) {
            vm.location.expedients[i].opens = new Date(vm.location.expedients[i].opens);
            vm.location.expedients[i].closes = new Date(vm.location.expedients[i].closes);
          }
        },
        function(error) {
          ResponseCatcherService.catchError(error);
          vm.isLoading = false;
          $uibModalInstance.close();
        });
    }

    function save(location) {
      $scope.locationForm.$setSubmitted();

      vm.isLoading = true;

      if (resolve.locationId) {
        editLocation(location);
      } else {
        addLocation(location);
      }
    }

    function addLocation(location){
      LocationsAdminService.addLocation(location).then(
        function(response) {
          ResponseCatcherService.catchSuccess(response).then(
            function(){
              $uibModalInstance.close();
            }
          );
        },
        function(error) {
          ResponseCatcherService.catchError(error);
        }
      ).finally(function(){
        vm.isLoading = true;
      })
    }

    function editLocation(location){
      LocationsAdminService.updateLocation(location).then(
        function(response) {
          ResponseCatcherService.catchSuccess(response).then(
            function(){
              $uibModalInstance.close();
            }
          );
        },
        function(error) {
          ResponseCatcherService.catchError(error);
        }
      ).finally(function(){
        vm.isLoading = false;
      })
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }

    function invalidTime(time) {
      return time === undefined || time === null;
    }

    function isMobile() {
      return vm.location.type == "Mobile";
    }

  }
})();
