;(function () {
  'use strict';

  angular
    .module('filazero-app')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider.state('app.provider.locations', {
      url: '/locations',
      data:{
        requireAuthentication: true
      },
      templateUrl: 'modules/admin/locations/locations.html',
      controller: 'LocationsController',
      controllerAs: 'vm',
      resolve: {
        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            'services/locations.service.js'
          ], {
            insertBefore: '#lazyload_placeholder'
          }).then(function () {
            return $ocLazyLoad.load([
              'modules/admin/locations/locations.controller.js',
              'modules/admin/locations/location-remove-modal.controller.js',
              'modules/admin/locations/location-manager-modal.controller.js'
            ]);
          });
        }]
      }
    });
  }
}());
