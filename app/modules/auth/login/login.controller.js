; (function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('LoginController', LoginController);

  LoginController.$inject = [
    'AuthService',
    '$location',
    '$state',
    '$stateParams',
    'ngAuthSettings',
    '$scope',
    '$q',
    'ProvidersService',
    'ResponseCatcherService',
    'LoginErrorCatcherService'
  ];

  function LoginController(AuthService, $location, $state, $stateParams, ngAuthSettings, $scope, $q, ProvidersService,
                           ResponseCatcherService, LoginErrorCatcherService) {
    var vm = this;

    vm.loginForm = {};
    vm.siteURL = ngAuthSettings.site;

    vm.authExternalProvider = authExternalProvider;
    vm.login = login;
    vm.isRunningLogin = false;

    init();

    function authExternalProvider(provider) {
      var redirectUri = location.protocol + '//' + location.host + '/assets/plugins/auth-complete/authComplete.html';

      var externalProviderUrl = ngAuthSettings.apiServiceBaseUri + "api/Account/ExternalLogin?provider=" + provider
        + "&response_type=token&client_id=" + ngAuthSettings.clientId
        + "&redirect_uri=" + redirectUri;
      window.$windowScope = $scope;

      window.open(externalProviderUrl, "Authenticate Account", "location=0,status=0,width=600,height=750");
    }

    function init() {
      if (AuthService.isAuth()) {
        $state.go('app.account.providers');
      }

      vm.loginForm = {
        userName: '',
        password: ''
      };
    }

    function login() {
      if(vm.loginForm.userName && vm.loginForm.password) {
        vm.isRunningLogin = true;
        AuthService.login(vm.loginForm)
          .then(
            function (response) {
              redirect();
            },
            function (error) {
              LoginErrorCatcherService.catchError(error);
            }
          )
          .finally(function () {
            vm.isRunningLogin = false;
          });
      }
    }

    function redirect() {
      if ($scope.modalInstance) {
        $scope.modalInstance.close(true);
      } else {
        if ($stateParams.redirectTo) {
          $location.url($stateParams.redirectTo);
        } else {
          getProviders().then(
            function (providers) {
              switch (providers.providers.length) {
                case 0:
                  $state.go('quickstart');
                  break;
                case 1:
                  $state.go('app.provider.dashboard', { providerSlug: providers.providers[0].slug });
                  break;
                default:
                  $state.go('app.account.providers');
              }
            });
        }
      }
    }

    function getProviders() {
      var deferred = $q.defer();

      ProvidersService.getProviders().then(
        function (response) {
          deferred.resolve(response);
        },
        function (error) {
          ResponseCatcherService.catchError(error);
        });

      return deferred.promise;
    }

    $scope.authCompletedCB = function (fragment) {
      $scope.$apply(
        function () {
          var externalData = {
            provider: fragment.provider,
            externalAccessToken: fragment.external_access_token
          };

          AuthService.registerOrUpdateExternal(externalData).then(
            function (response) {
              redirect();
            }
          );
        }
      )
    };
  }

})();
