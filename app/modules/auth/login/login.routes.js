; (function() {
  'use strict';

  angular
    .module('filazero-app')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider.state('login', {
      url: '/login?redirectTo',
      data: {
        requireAuthentication: false
      },
      templateUrl: 'modules/auth/login/login.html',
      controller: 'LoginController',
      controllerAs: 'vm',
      resolve: {
        deps: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load([
            'services/providers.service.js'
          ], {
            insertBefore: '#lazyload_placeholder'
          }).then(function() {
            return $ocLazyLoad.load([
              'modules/auth/login/login.controller.js'
            ]);
          });
        }]
      }
    });
  }
})();
