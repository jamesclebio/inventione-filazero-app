(function () {
  'use strict';

  angular
    .module('filazero-app')
    .controller('QuickStartController', QuickStartController);

  QuickStartController.$inject = [
    '$state',
    'WizardHandler',
    '$q',
    'ProvidersService',
    'AuthService',
    'QuickStartService',
    'ResponseCatcherService',
    'ViaCepService',
    'ServicesManagerService'
  ];

  function QuickStartController($state, WizardHandler, $q, ProvidersService, AuthService, QuickStartService, ResponseCatcherService, ViaCepService, ServicesManagerService) {
    var vm = this;

    vm.register = register;

    vm.createProviderAndLocation = createProviderAndLocation;
    vm.checkSlugAvailability = checkSlugAvailability;
    vm.getAddress = getAddress;

    vm.getCurrentStep = getCurrentStep;

    vm.AddSession = AddSession;
    vm.RemoveSession = RemoveSession;
    vm.SearchService = SearchService;
    vm.ToggleSelectedDay = ToggleSelectedDay;
    vm.DoneServiceConfiguration = DoneServiceConfiguration;
    vm.HasError = HasError;

    vm.states = [
      "Acre", "Alagoas", "Amapá", "Amazonas", "Bahia", "Ceará", "Distrito Federal", "Espírito Santo", "Goias", "Maranhão", "Mato Grosso", "Mato Grosso do Sul", "Minas Gerais", "Pará", "Paraíba", "Paraná", "Pernambuco", "Piauí", "Rio de Janeiro", "Rio Grande do Norte", "Rio Grande do Sul", "Rondônia", "Roraima", "Santa Catarina", "São Paulo", "Sergipe", "Tocantins"
    ];

    vm.specialty = {
      name: '',
      averageTime: 30,
      allowsSelfService: true,
      daysOfWeek: [1, 2, 3, 4, 5],
      sessionModels: []
    };

    vm.daysOfWeek = [];
    vm.maskOptions = {
      maskDefinitions: { '5': /[0-5]/ },
      clearOnBlur: false
    };
    vm.sessionsWithError = [];

    // Initialization
    function init() {
      if (AuthService.isAuth()) {
        getProviders().then(
          function (providers) {
            switch (providers.providers.length) {
              case 0:
                WizardHandler.wizard().goTo(1);
                getCategories();
                break;
              default:
                $state.go('app.provider.attendance', { providerSlug: providers.providers[0].slug })
            }
          });
      }
    }

    init();

    function getProviders() {
      var deferred = $q.defer();

      ProvidersService.getProviders().then(
        function (response) {
          deferred.resolve(response);
        },
        function (error) {
          ResponseCatcherService.catchError(error, true);
        });

      return deferred.promise;
    }
    // /Initialization

    // Register
    function register(registration) {
      vm.isRunningRegister = true;
      AuthService.saveRegistration(registration).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response, false).then(
            function (messages) {
              AuthService.login({ userName: registration.email, password: registration.password }).then(
                function (response) {
                  getCategories();
                  WizardHandler.wizard().goTo(1);
                },
                function (error) {
                  ResponseCatcherService.catchError(error, true);
                  vm.isRunningRegister = false;
                }
              );
            }
          ).finally(function(){
            vm.isRunningRegister = false;
          });
        },
        function (error) {
          ResponseCatcherService.catchError(error);
          vm.isRunningRegister = false;
        });
    }
    // /Register

    // Provider
    function createProviderAndLocation(provider) {
      vm.isRunningCreateProviderAndLocation = true;

      if (provider && provider.location) {
        provider.location.name = "Matriz";
        provider.location.type = "Fixed";
        provider.location.expedients = [];
      }

      ProvidersService.addProvider(provider).then(
        function (response) {
          ResponseCatcherService.catchSuccess(response, false).then(
            function (messages) {
              vm.provider = response.responseData;
              vm.locationId = response.responseData.locationId;
              initDaysOfWeek();
              initSessions();
              WizardHandler.wizard().goTo(2);
            },
            function (messages) {
              vm.isRunningCreateProviderAndLocation = false;
            }
          );
        },
        function (error) {
          ResponseCatcherService.catchError(error);
          vm.isRunningCreateProviderAndLocation = false;
        }
      );
    }

    function checkSlugAvailability(slug) {
      if (slug && slug.length > 3) {
        ProvidersService.checkAvailability(slug).then(
          function (response) {
            vm.isSlugAvailable = response.isAvailable;
          }
        )
      }
    }

    function getCategories() {
      vm.isLoading = true;
      ProvidersService.getCategories().then(
        function (categories) {
          vm.isLoading = false;
          vm.categories = categories;
        },
        function (error) {
          ResponseCatcherService.catchError(error, true);
        })
    }

    function getAddress(zipCode) {
      if (zipCode) {
        vm.isRunningGetAddress = true;
        ViaCepService.getAddress(zipCode).then(
          function (success) {
            vm.provider.location.address = success.logradouro;
            vm.provider.location.district = success.bairro;
            vm.provider.location.city = success.localidade;
            vm.provider.location.state = success.uf;
            vm.isRunningGetAddress = false;
          },
          function (error) {
            vm.isRunningGetAddress = false;
          })
      }
    }
    // /Provider

    // Services
    function initDaysOfWeek() {
      vm.dayList = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];
    }

    function initSessions() {
      vm.specialty.sessionModels = [
        { start: new Date(99, 5, 24, 8, 0), duration: '0400', slots: 5 },
        { start: new Date(99, 5, 24, 14, 0), duration: '0400', slots: 5 }
      ];
    }

    function AddSession() {
      vm.specialty.sessionModels.push(
        { start: undefined, duration: '', slots: undefined }
      )
      CheckSessionModels(vm.specialty.sessionModels);
    }

    function RemoveSession(index) {
      vm.specialty.sessionModels.splice(index, 1);
      CheckSessionModels(vm.specialty.sessionModels);
    }

    function SearchService(queryString) {
      return ServicesManagerService.searchServices(queryString).then(
        function (response) {
          return response;
        },
        function (error) {
          ResponseCatcherService.catchError(error);
        });
    }

    function ToggleSelectedDay(day) {
      var idx = vm.specialty.daysOfWeek.indexOf(day);

      if (idx > -1) {
        vm.daysOfWeek.splice(idx, 1);
      } else {
        vm.daysOfWeek.push(day)
      }
    }

    function DoneServiceConfiguration(form) {
      form.$setSubmitted(true);
      if (form.$valid && vm.specialty.daysOfWeek.length > 0 &&
        CheckSessionModels(vm.specialty.sessionModels)) {
        var user = AuthService.getUserInfo();

        vm.isRunningDoneServiceConfiguration = true;
        QuickStartService.configureFirstSpecialty(vm.provider.id, vm.locationId, { name: user.firstName, email: user.id }, vm.specialty).then(
          function (resolve) {
            ResponseCatcherService.catchSuccess(resolve, false).then(
              function (messages) {
                WizardHandler.wizard().goTo(3);
              },
              function (messages) {
                vm.isRunningDoneServiceConfiguration = false;
              }
            );
          },
          function (error) {
            ResponseCatcherService.catchError(error);
            vm.isRunningDoneServiceConfiguration = false;
          }
        ).finally(function(){
          vm.isRunningDoneServiceConfiguration = false;
        });
      }
    }

    function CheckSessionModels(sessionModels) {
      vm.sessionsWithError = [];
      sessionModels.forEach(function (s, key) {
        if (s.start === null || s.duration === undefined || (s.slots === undefined || s.slots <= 0)) {
          vm.sessionsWithError.push(key);
        }
      });

      return vm.sessionsWithError.length === 0;
    }

    function HasError(index) {
      return _.contains(vm.sessionsWithError, index);
    }
    // /Services

    // Wizard
    function getCurrentStep() {
      return WizardHandler.wizard().currentStepNumber();
    }
    // /Wizard
  }
})();
