; (function () {
  'use strict';

  angular
    .module('filazero-app')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider
      .state('quickstart', {
        url: '/quick-start',
        data: {
          requireAuthentication: false
        },
        templateUrl: 'modules/quick-start/quick-start.html',
        controller: 'QuickStartController',
        controllerAs: 'vm',
        resolve: {
          deps: ['$ocLazyLoad', function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              'select',
              'services/manager/sessions-generator-manager.service.js',
              'services/manager/resources-manager.service.js',
              'services/quick-start.service.js',
              'services/service-config.service.js',
              'services/via-cep.service.js',
              'services/manager/services-manager.service.js',
              'services/manager/rooms-manager.service.js'
            ], {
                insertBefore: '#lazyload_placeholder'
              }).then(function () {
                return $ocLazyLoad.load([
                  'modules/quick-start/quick-start.controller.js'
                ]);
              });
          }]
        }
      });
  }
})();



