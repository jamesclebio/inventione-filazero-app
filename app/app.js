; (function() {
  'use strict';

  angular.module('filazero-app', [
    'ui.router',
    'ui.bootstrap',
    'ui.mask',
    'oc.lazyLoad',
    'ngResource',
    'doowb.angular-pusher',
    'LocalStorageModule',
    'NgSwitchery',
    'pascalprecht.translate',
    'tmh.dynamicLocale',
    'ngCookies',
    'ngSanitize',
    'ngMessages',
    'angular.morris',
    'ngAudio',
    'virtualPage',
    'ResponseCatcher',
    'angular-ladda',
    'checklist-model',
    'angulartics',
    'angulartics.google.analytics',
    'mgo-angular-wizard'
  ]);
})();
